#!/usr/bin/python
# coding: utf-8
'''
 Date: 21 February 2013
 Author: Raymond S. Usbal
'''

from DBconn import DBclass
from deHTML import dehtml
from Scrap_url import get_url
import codecs
import lxml.html
import sys

# Setting
this_run = 'LOCAL'

dbc = DBclass()

if this_run == 'SERVER':
    """
    Connect to a2hosting: dev.club4causes.com, if it does not connect, create a tunnel first:
       $ ssh -p7822 clubcaus@dev.club4causes.com -L 3307:localhost:3306
    """
    conn = dbc.connect(host='127.0.0.1', db='clubcaus_c4c', init_command='SET NAMES utf8', port=3307)

else:
    conn = dbc.connect(host='localhost', db='clubcaus_c4c', init_command='SET NAMES utf8')

ci = conn.cursor()

proxy = "http://anonymouse.org/cgi-bin/anon-www.cgi/"
proxy_len = len(proxy)

resource_src = "http://www.looktothestars.org/celebrity/"

if len(sys.argv) == 2:
    first_name = sys.argv[1].split('-')[0].capitalize() + '%'
    ci.execute('''SELECT id, first_name, last_name FROM celebrities 
        WHERE short_bio IS NULL AND looktothestars_bio IS NULL AND first_name >= %s ORDER BY 2, 3''', (first_name,))
else:
    ci.execute('''SELECT id, first_name, last_name FROM celebrities 
        WHERE short_bio IS NULL AND looktothestars_bio IS NULL ORDER BY 2, 3''')

dbc = ci.fetchall()

for pid, first_name, last_name in dbc:

    search_name = (first_name + " " + last_name).strip()
    search_name = search_name.replace('.', '').replace('!', '').replace('?', '')
    search_name = search_name.replace('/', '-').replace(' ', '-').lower()

    print ("%s: %s, %s : %s" % (pid, last_name, first_name, search_name)),

    try:
        root = get_url(resource_src + search_name, verbose=False)
    except:
        print " Not found."
    else:
        el = root.cssselect("div[itemprop=description]")

        if el:
            text = lxml.html.tostring(el[0], pretty_print=True)
            text = text.replace('&#8217;', '’')
            content = dehtml(text).replace(' . ', '. ').replace(' , ', ', ').rstrip(' .') + "."

            result = ci.execute('''UPDATE celebrities SET looktothestars_bio = %s WHERE `id` = %s''', (content, pid,))
            print result
            conn.commit()
        else:
            print " Not in DOM"

    print

# eof
