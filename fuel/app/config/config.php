<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.0
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2012 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * If you want to override the default configuration, add the keys you
 * want to change here, and assign new values to them.
 */

return array(

	'always_load'  => array(

		/**
		 * These packages are loaded on Fuel's startup.
		 * You can specify them in the following manner:
		 *
		 * array('auth'); // This will assume the packages are in PKGPATH
		 *
		 * // Use this format to specify the path to the package explicitly
		 * array(
		 *     array('auth'	=> PKGPATH.'auth/')
		 * );
		 */
		'packages'  => array(
			'parser',
			'auth',
			'orm',
			'email',
		),
	),

    /**
     * Added by Raymond on 22 March 2013
     * http://www.marcopace.it/blog/2012/12/fuelphp-i18n-internationalization-of-a-web-application
     */
    'language'           => 'en', // Default language
    'language_fallback'  => 'en', // Fallback language when file isn't available for default language
    'locale'             => 'en_US', // PHP set_locale() setting, null to not set
    'locales'            => array(
        'en' => 'en_US',
        'it' => 'it_IT'
    ),
);
