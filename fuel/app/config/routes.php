<?php

/**
 * Get uri request
 */

$system = "";

if (isset($_SERVER['REQUEST_URI']))
{
    $uri = explode("/", trim($_SERVER['REQUEST_URI'], "/"));

    if ($uri[0] == "admin")
        $system = "admin/";
}

return array(
	'_root_'  => 'home',
	'_404_'   => $system.'system/404',

    'admin' => 'admin/index',

    'signin' => 'auth/signin',
    'signout' => 'auth/signout',
    'login' => 'auth/signin',
    'logout' => 'auth/signout',

    'blog/(:num)' => 'blog/index/$1',

	'registration' => 'members/registration',

    // 'blog' => 'blog/index',
    'non_profit' => 'nonprofit/index',
    'worlds-top-givers' => 'donor',
    'challenges' => 'challenges/index',
    
    'privacy-policy' => 'static/privacy',
    'terms-of-service' => 'static/tos',

    'nonprofit' => 'nonprofit/index',
    'nonprofit/index/(:any)' => 'nonprofit/index',
);