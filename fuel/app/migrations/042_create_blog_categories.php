<?php

namespace Fuel\Migrations;

class Create_blog_categories
{
	public function up()
	{
		\DBUtil::create_table('blog_categories', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'category' => array('constraint' => 45, 'type' => 'varchar'),

		), array('id'));

		\DBUtil::create_table('blog_posts_categories', array(
			'post_id' => array('constraint' => 11, 'type' => 'int'),
			'category_id' => array('constraint' => 11, 'type' => 'int'),

		), array('post_id', 'category_id'));
	}

	public function down()
	{
		\DBUtil::drop_table('blog_categories');
		\DBUtil::drop_table('blog_posts_categories');
	}
}