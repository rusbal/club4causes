<?php

namespace Fuel\Migrations;

class Create_asset_interfaces
{
	public function up()
	{
		\DBUtil::create_table('asset_interfaces', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'asset_id' => array('constraint' => 11, 'type' => 'int'),
			'contributor_id' => array('constraint' => 11, 'type' => 'int'),

		), array('id'));

        \DB::query("ALTER TABLE `asset_interfaces` ADD INDEX ( `asset_id` )")->execute();
        \DB::query("ALTER TABLE `asset_interfaces` ADD INDEX ( `contributor_id` )")->execute();
	}

	public function down()
	{
		\DBUtil::drop_table('asset_interfaces');
	}
}