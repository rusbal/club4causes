<?php

namespace Fuel\Migrations;

class Create_coupons
{
	public function up()
	{
		\DBUtil::create_table('coupons', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'business_name' => array('constraint' => 45, 'type' => 'varchar'),
			'coupon' => array('constraint' => 100, 'type' => 'varchar'),
			'business_url' => array('constraint' => 100, 'type' => 'varchar'),
			'donation' => array('constraint' => 45, 'type' => 'varchar'),
			'details' => array('type' => 'text'),
			'image_filename' => array('constraint' => 100, 'type' => 'varchar'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('coupons');
	}
}