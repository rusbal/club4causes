<?php

namespace Fuel\Migrations;

class Create_user_no_id_requests
{
	public function up()
	{
		\DBUtil::create_table('user_no_id_requests', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'user_id' => array('constraint' => 11, 'type' => 'int'),
			'hash' => array('constraint' => 255, 'type' => 'varchar'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),

		), array('id'));

		\DB::query("ALTER TABLE `user_no_id_requests` ADD INDEX ( `user_id` )")->execute();
	}

	public function down()
	{
		\DBUtil::drop_table('user_no_id_requests');
	}
}