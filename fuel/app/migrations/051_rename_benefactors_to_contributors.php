<?php

namespace Fuel\Migrations;

class Rename_benefactors_to_contributors
{
	public function up()
	{
        \DB::query("RENAME TABLE `benefactors` TO `contributors`")->execute();

	}

	public function down()
	{
        \DB::query("RENAME TABLE `contributors` TO `benefactors`")->execute();

	}
}