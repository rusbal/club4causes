<?php

namespace Fuel\Migrations;

class Add_field_to_user_alerts
{
	public function up()
	{
		\DBUtil::add_fields('user_alerts', array(
			'is_read' => array('type' => 'boolean'),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('user_alerts', array(
			'is_read'

		));
	}
}