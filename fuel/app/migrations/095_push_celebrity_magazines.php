<?php

namespace Fuel\Migrations;

class Push_celebrity_magazines
{
	public function up()
	{
        \DB::query("ALTER TABLE `celebrity_magazines` DROP `celebrity_id`")->execute();
        \DB::query("ALTER TABLE `celebrity_magazines` DROP `date`")->execute();
        \DB::query("ALTER TABLE `celebrity_magazines` DROP `image_filename`")->execute();
	}

	public function down()
	{
        \DB::query("ALTER TABLE `celebrity_magazines` ADD  `celebrity_id` INT( 11 ) NOT NULL")->execute();
        \DB::query("ALTER TABLE `celebrity_magazines` ADD  `date` DATE NOT NULL")->execute();
        \DB::query("ALTER TABLE `celebrity_magazines` ADD  `image_filename` VARCHAR( 255 ) NOT NULL")->execute();
	}
}