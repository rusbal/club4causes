<?php

namespace Fuel\Migrations;

class Add_field_to_celebrity_magazines
{
	public function up()
	{
		\DBUtil::add_fields('celebrity_magazines', array(
			'name' => array('constraint' => 45, 'type' => 'varchar'),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('celebrity_magazines', array(
			'name'

		));
	}
}