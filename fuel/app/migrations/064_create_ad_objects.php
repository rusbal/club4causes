<?php

namespace Fuel\Migrations;

class Create_ad_objects
{
	public function up()
	{
		\DBUtil::create_table('ad_objects', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'owner_id' => array('constraint' => 11, 'type' => 'int'),
			'image_filename' => array('constraint' => 100, 'type' => 'varchar'),
			'description' => array('constraint' => 200, 'type' => 'varchar'),
			'target_url' => array('constraint' => 100, 'type' => 'varchar'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),

		), array('id'));

		\DB::query("ALTER TABLE `ad_objects` ADD INDEX ( `owner_id` )")->execute();
	}

	public function down()
	{
		\DBUtil::drop_table('ad_objects');
	}
}