<?php

namespace Fuel\Migrations;

class Add_id_to_nonprofit_users
{
	public function up()
	{
		\DB::query("ALTER TABLE `nonprofit_users` DROP PRIMARY KEY")->execute();
		
		\DB::query("ALTER TABLE  `nonprofit_users` ADD  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY")->execute();
		\DB::query("ALTER TABLE `nonprofit_users` ADD INDEX ( `nonprofit_id` )")->execute();
		\DB::query("ALTER TABLE `nonprofit_users` ADD INDEX ( `user_id` )")->execute();
	}

	public function down()
	{
		\DBUtil::drop_fields('nonprofit_users', array(
			'id'
		));

		\DB::query("ALTER TABLE `nonprofit_users` DROP INDEX `nonprofit_id`")->execute();
		\DB::query("ALTER TABLE `nonprofit_users` DROP INDEX `user_id`")->execute();
		\DB::query("ALTER TABLE `nonprofit_users` ADD PRIMARY KEY ( `nonprofit_id`, `user_id` )")->execute();
	}
}