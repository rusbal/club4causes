<?php

namespace Fuel\Migrations;

class Create_nonprofits_users
{
	public function up()
	{
		\DBUtil::create_table('nonprofits_users', array(
			'nonprofit_id' => array('constraint' => 11, 'type' => 'int'),
			'user_id' => array('constraint' => 11, 'type' => 'int'),

		), array('nonprofit_id', 'user_id'));
	}

	public function down()
	{
		\DBUtil::drop_table('nonprofits_users');
	}
}