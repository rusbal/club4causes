<?php

namespace Fuel\Migrations;

class Rename_table_nonprofits_users_to_nonprofit_users
{
	public function up()
	{
		\DBUtil::rename_table('nonprofits_users', 'nonprofit_users');
	}

	public function down()
	{
		\DBUtil::rename_table('nonprofit_users', 'nonprofits_users');
	}
}