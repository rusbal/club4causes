<?php

namespace Fuel\Migrations;

class Increase_donor_donation
{
	public function up()
	{
        \DB::query("ALTER TABLE  `donor_info` CHANGE  `donation_billion`  `donation_billion` DECIMAL( 4, 2 ) NULL DEFAULT NULL")->execute();
	}

	public function down()
	{
	}
}