<?php

namespace Fuel\Migrations;

class Create_celebrity_news
{
	public function up()
	{
		\DBUtil::create_table('celebrity_news', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'celebrity_id' => array('constraint' => 11, 'type' => 'int'),
			'product_image_id' => array('constraint' => 11, 'type' => 'int'),
			'title' => array('constraint' => 100, 'type' => 'varchar'),
			'news_details' => array('type' => 'TEXT'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),

		), array('id'));

		\DB::query("ALTER TABLE `celebrity_news` ADD INDEX ( `celebrity_id` )")->execute();
		\DB::query("ALTER TABLE `celebrity_news` ADD INDEX ( `product_image_id` )")->execute();
	}

	public function down()
	{
		\DBUtil::drop_table('celebrity_news');
	}
}