<?php

namespace Fuel\Migrations;

class Unique_blog_categories
{
	public function up()
	{
        \DB::query("ALTER TABLE  `blog_categories` ADD UNIQUE (`category` )")->execute();
	}

	public function down()
	{
        \DB::query("ALTER TABLE `blog_categories` DROP INDEX category")->execute();
	}
}