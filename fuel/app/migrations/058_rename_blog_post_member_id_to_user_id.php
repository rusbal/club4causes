<?php

namespace Fuel\Migrations;

class Rename_blog_post_member_id_to_user_id
{
	public function up()
	{
        \DB::query("ALTER TABLE  `blog_posts` CHANGE  `member_id`  `user_id` INT( 11 ) NOT NULL")->execute();
	}

	public function down()
	{
        \DB::query("ALTER TABLE  `blog_posts` CHANGE  `user_id`  `member_id` INT( 11 ) NOT NULL")->execute();
	}
}