<?php

namespace Fuel\Migrations;

class Push_profession
{
	public function up()
	{
        \DB::query("INSERT INTO `professions` SET field = 'Other'")->execute();
	}

	public function down()
	{
        \DB::query("DELETE FROM `professions` WHERE field = 'Other'")->execute();
	}
}