<?php

namespace Fuel\Migrations;

class Add_selected_to_celebrities
{
	public function up()
	{
		\DBUtil::add_fields('celebrities', array(
			'selected' => array('constraint' => 11, 'type' => 'int'),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('celebrities', array(
			'selected'

		));
	}
}