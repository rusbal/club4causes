<?php

namespace Fuel\Migrations;

class Add_created_at_to_nonprofit_users
{
	public function up()
	{
		\DBUtil::add_fields('nonprofit_users', array(
			'created_at' => array('constraint' => 11, 'type' => 'int'),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('nonprofit_users', array(
			'created_at'

		));
	}
}