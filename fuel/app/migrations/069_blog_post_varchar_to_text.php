<?php

namespace Fuel\Migrations;

class Blog_post_varchar_to_text
{
	public function up()
	{
        \DB::query("ALTER TABLE  `blog_posts` CHANGE  `post`  `post` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL")->execute();
	}

	public function down()
	{
        \DB::query("ALTER TABLE  `blog_posts` CHANGE  `post`  `post` VARCHAR(20000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL")->execute();
	}
}