<?php

namespace Fuel\Migrations;

class Create_blog_comments
{
	public function up()
	{
		\DBUtil::create_table('blog_comments', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'post_id' => array('constraint' => 11, 'type' => 'int'),
			'comment' => array('constraint' => 1500, 'type' => 'varchar'),
			'user_id' => array('constraint' => 11, 'type' => 'int'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),

		), array('id'));

		\DB::query("ALTER TABLE `blog_comments` ADD INDEX ( `post_id` )")->execute();
	}

	public function down()
	{
		\DBUtil::drop_table('blog_comments');
	}
}