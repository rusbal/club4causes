<?php

namespace Fuel\Migrations;

class Add_priority_to_pets
{
	public function up()
	{
		\DBUtil::add_fields('pets', array(
			'priority' => array('constraint' => 11, 'type' => 'int'),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('pets', array(
			'priority'

		));
	}
}