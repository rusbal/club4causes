<?php

namespace Fuel\Migrations;

class Rename_ad_pages_page_to_page_path
{
	public function up()
	{
        \DB::query("ALTER TABLE  `ad_pages` CHANGE  `page`  `page_path` VARCHAR( 45 ) NOT NULL")->execute();    
	}

	public function down()
	{
        \DB::query("ALTER TABLE  `ad_pages` CHANGE  `page_path`  `page` VARCHAR( 45 ) NOT NULL")->execute();
	}
}