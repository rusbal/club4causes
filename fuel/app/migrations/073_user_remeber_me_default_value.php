<?php

namespace Fuel\Migrations;

class User_remeber_me_default_value
{
	public function up()
	{
        \DB::query("ALTER TABLE  `users` CHANGE  `remember_me`  `remember_me` VARCHAR( 225 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL")->execute();
	}

	public function down()
	{
        \DB::query("ALTER TABLE  `users` CHANGE  `remember_me`  `remember_me` VARCHAR( 225 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL")->execute();
	}
}