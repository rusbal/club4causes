<?php

namespace Fuel\Migrations;

class Create_missings
{
	public function up()
	{
		\DBUtil::create_table('missings', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'name' => array('constraint' => 35, 'type' => 'varchar'),
			'image_filename' => array('constraint' => 200, 'type' => 'varchar'),
			'merchant_url' => array('constraint' => 500, 'type' => 'varchar'),
			'priority' => array('constraint' => 11, 'type' => 'int'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('missings');
	}
}