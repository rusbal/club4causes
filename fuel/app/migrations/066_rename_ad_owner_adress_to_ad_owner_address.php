<?php

namespace Fuel\Migrations;

class Rename_ad_owner_adress_to_ad_owner_address
{
	public function up()
	{
        \DB::query("RENAME TABLE `ad_owner_adresses` TO `ad_owner_addresses`")->execute();
	}

	public function down()
	{
        \DB::query("RENAME TABLE `ad_owner_addresses` TO `ad_owner_adresses`")->execute();
	}
}