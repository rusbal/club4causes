<?php

namespace Fuel\Migrations;

class Create_celebrity_magazines
{
	public function up()
	{
		\DBUtil::create_table('celebrity_magazines', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'celebrity_id' => array('constraint' => 11, 'type' => 'int'),
			'date' => array('type' => 'DATE'),
			'image_filename' => array('constraint' => 255, 'type' => 'varchar'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),

		), array('id'));

		\DB::query("ALTER TABLE `celebrity_magazines` ADD INDEX ( `celebrity_id` )")->execute();
	}

	public function down()
	{
		\DBUtil::drop_table('celebrity_magazines');
	}
}