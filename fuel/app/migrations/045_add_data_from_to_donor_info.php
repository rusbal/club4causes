<?php

namespace Fuel\Migrations;

class Add_data_from_to_donor_info
{
	public function up()
	{
		\DBUtil::add_fields('donor_info', array(
			'data_from' => array('constraint' => 25, 'type' => 'varchar', 'null' => true),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('donor_info', array(
			'data_from'

		));
	}
}