<?php

namespace Fuel\Migrations;

class Create_shopslides
{
	public function up()
	{
		\DBUtil::create_table('shopslides', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'string' => array('type' => 'text'),
			'image_filename' => array('constraint' => 100, 'type' => 'varchar'),
			'merchant_url' => array('constraint' => 500, 'type' => 'varchar'),
			'is_active' => array('type' => 'boolean'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('shopslides');
	}
}