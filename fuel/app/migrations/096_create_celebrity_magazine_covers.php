<?php

namespace Fuel\Migrations;

class Create_celebrity_magazine_covers
{
	public function up()
	{
		\DBUtil::create_table('celebrity_magazine_covers', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'celebrity_id' => array('constraint' => 11, 'type' => 'int'),
			'magazine_id' => array('constraint' => 11, 'type' => 'int'),
			'image_filename' => array('constraint' => 255, 'type' => 'varchar'),
			'date' => array('type' => 'DATE'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),

		), array('id'));

		\DB::query("ALTER TABLE `celebrity_magazine_covers` ADD INDEX ( `celebrity_id` )")->execute();
		\DB::query("ALTER TABLE `celebrity_magazine_covers` ADD INDEX ( `magazine_id` )")->execute();
	}

	public function down()
	{
		\DBUtil::drop_table('celebrity_magazine_covers');
	}
}