<?php

namespace Fuel\Migrations;

class Add_field_to_blog_comments
{
	public function up()
	{
		\DBUtil::add_fields('blog_comments', array(
			'user_id' => array('constraint' => 11, 'type' => 'int'),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('blog_comments', array(
			'user_id'

		));
	}
}