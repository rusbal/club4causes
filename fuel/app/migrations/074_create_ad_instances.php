<?php

namespace Fuel\Migrations;

class Create_ad_instances
{
	public function up()
	{
		\DBUtil::create_table('ad_instances', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'space_id' => array('constraint' => 11, 'type' => 'int'),
			'object_id' => array('constraint' => 11, 'type' => 'int'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('ad_instances');
	}
}