<?php

namespace Fuel\Migrations;

class Create_ad_spaces_histories
{
	public function up()
	{
		\DBUtil::create_table('ad_spaces_histories', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'owner_id' => array('constraint' => 11, 'type' => 'int'),
			'space_id' => array('constraint' => 11, 'type' => 'int'),
			'activity' => array('constraint' => 45, 'type' => 'varchar'),
			'date_time' => array('constraint' => 45, 'type' => 'char'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('ad_spaces_histories');
	}
}