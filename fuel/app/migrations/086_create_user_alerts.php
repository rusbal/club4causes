<?php

namespace Fuel\Migrations;

class Create_user_alerts
{
	public function up()
	{
		\DBUtil::create_table('user_alerts', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'from_id' => array('constraint' => 11, 'type' => 'int'),
			'to_id' => array('constraint' => 11, 'type' => 'int'),
			'alert_id' => array('constraint' => 11, 'type' => 'int'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),

		), array('id'));

		\DB::query("ALTER TABLE `user_alerts` ADD INDEX ( `from_id` )")->execute();
		\DB::query("ALTER TABLE `user_alerts` ADD INDEX ( `to_id` )")->execute();
		\DB::query("ALTER TABLE `user_alerts` ADD INDEX ( `alert_id` )")->execute();

	}

	public function down()
	{
		\DBUtil::drop_table('user_alerts');
	}
}