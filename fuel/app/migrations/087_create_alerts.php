<?php

namespace Fuel\Migrations;

class Create_alerts
{
	public function up()
	{
		\DBUtil::create_table('alerts', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'message' => array('constraint' => 250, 'type' => 'varchar'),
			'permalink' => array('constraint' => 250, 'type' => 'varchar'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('alerts');
	}
}