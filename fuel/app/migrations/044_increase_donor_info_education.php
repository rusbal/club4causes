<?php

namespace Fuel\Migrations;

class Increase_donor_info_education
{
    /**
     * Increase donor_info.education from 75 to 120
     */
	public function up()
	{
        \DB::query("ALTER TABLE  `donor_info` CHANGE  `education`  `education` VARCHAR( 120 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL")->execute();
	}

	public function down()
	{
        \DB::query("ALTER TABLE  `donor_info` CHANGE  `education`  `education` VARCHAR( 75 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL")->execute();
	}
}