<?php

namespace Fuel\Migrations;

class Add_looktothestars_bio_to_celebrities
{
	public function up()
	{
		\DBUtil::add_fields('celebrities', array(
			'looktothestars_bio' => array('type' => 'text', 'null' => true),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('celebrities', array(
			'looktothestars_bio'

		));
	}
}