<?php

namespace Fuel\Migrations;

class Add_status_to_blog_comments
{
	public function up()
	{
		\DBUtil::add_fields('blog_comments', array(
			'name' => array('constraint' => 45, 'type' => 'varchar'),
			'email' => array('constraint' => 100, 'type' => 'varchar'),
			'website' => array('constraint' => 45, 'type' => 'varchar'),
			'status' => array('type' => 'smallint'),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('blog_comments', array(
			'name'
,			'email'
,			'website'
,			'status'

		));
	}
}