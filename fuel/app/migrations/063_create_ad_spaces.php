<?php

namespace Fuel\Migrations;

class Create_ad_spaces
{
	public function up()
	{
		\DBUtil::create_table('ad_spaces', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'page_id' => array('constraint' => 11, 'type' => 'int'),
			'location_code' => array('constraint' => 45, 'type' => 'varchar'),
			'description' => array('constraint' => 200, 'type' => 'varchar'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),

		), array('id'));

		\DB::query("ALTER TABLE `ad_spaces` ADD INDEX ( `page_id` )")->execute();
	}

	public function down()
	{
		\DBUtil::drop_table('ad_spaces');
	}
}