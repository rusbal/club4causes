<?php

namespace Fuel\Migrations;

class Create_faqs
{
	public function up()
	{
		\DBUtil::create_table('faqs', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'question' => array('constraint' => 200, 'type' => 'varchar'),
			'answer' => array('type' => 'text'),
			'priority' => array('constraint' => 3, 'type' => 'int'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('faqs');
	}
}