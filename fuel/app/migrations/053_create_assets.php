<?php

namespace Fuel\Migrations;

class Create_assets
{
	public function up()
	{
		\DBUtil::create_table('assets', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'type' => array('constraint' => '"link","image","video"', 'type' => 'enum'),
			'location' => array('constraint' => 200, 'type' => 'varchar'),
			'description' => array('constraint' => 200, 'type' => 'varchar'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('assets');
	}
}