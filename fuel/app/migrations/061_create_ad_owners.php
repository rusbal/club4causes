<?php

namespace Fuel\Migrations;

class Create_ad_owners
{
	public function up()
	{
		\DBUtil::create_table('ad_owners', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'user_id' => array('constraint' => 11, 'type' => 'int'),
			'business_name' => array('constraint' => 100, 'type' => 'varchar'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),

		), array('id'));

		\DB::query("ALTER TABLE `ad_owners` ADD INDEX ( `user_id` )")->execute();
	}

	public function down()
	{
		\DBUtil::drop_table('ad_owners');
	}
}