<?php

namespace Fuel\Migrations;

class Create_blog_posts
{
	public function up()
	{
		\DBUtil::create_table('blog_posts', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'member_id' => array('constraint' => 11, 'type' => 'int'),
			'post' => array('constraint' => 20000, 'type' => 'varchar'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),

		), array('id'));

		\DB::query("ALTER TABLE `blog_posts` ADD INDEX ( `member_id` )")->execute();
	}

	public function down()
	{
		\DBUtil::drop_table('blog_posts');
	}
}