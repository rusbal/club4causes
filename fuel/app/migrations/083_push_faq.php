<?php

namespace Fuel\Migrations;

class Push_faq
{
    private $data = array(
        array(
        "What is a member?",
        "A member is a person who joins (our logo) to support their favorite, charity, congregation, school or fraternal organization.",
        ),
        array(
        "What does it cost to be a member?",
        "There is no initial or ongoing cost to be a member.",
        ),
        array(
        "Is a member asked to make any donations?",
        "never ask's it's member's to make donations of any kind.",
        ),
        array(
        "What are the obligations of the member?",
        "The members are under no obligation of any kind.",
        ),
        array(
        "Why should someone become a member?",
        "The club is geared for people who recognize they can support their favorite cause at no cost to themselves.",
        ),
        array(
        "What is a partner ?",
        "A partner is the charity, congregation, school or fraternal organization that has been designated by a member to recieve their points, which  becomes a money contribtion to the partner.",
        ),
        array(
        "Can a Partner have more than one member designate them?",
        "yes, there is no limit to the number of members a partner may have supporting them.",
        ),
        array(
        "What are a partner’s obligations to the club?",
        "absolutley none,although the club would appreciate it if the partner promoted the use of the club.",
        ),
        array(
        "Why should an organization want to be a partner?",
        "Because at no cost,or effort, to them they automaticaly will recieve contributions.",
        ),
        array(
        "When does a partner begin recieving points?",
        "The first day they are designated by a member to be their partner.",
        ),
        array(
        "How are points generated?",
        "The member gets points for their designated partner, every time they participate in the club's challenges, shopping or recruiting.",
        ),
        array(
        "How often is the distribution to the partners calculated ?",
        "INCOME DISTRIBUTION: This Income Distribution is for example only and is not a contract or guarantee of what the partners will earn which is shown in this example.",
        ),
        array(
        "How often are the points calculated.",
        "Points are calculated in real time.",
        ),
        array(
        "What are the challenges?",
        "<strong>Surveys,Quizzes ,Polls<br> on  a wide variety of subjects</strong><br> Headline news,  Politics,  Business news  , Celebrities , Books,  Trivia, Science,  Technology,  Art,  Cars,  Outdoors,  Recipies,  Movies , Music, History,  Geography,  T.V., Pets, Fashion, Nascar, College sports,  International Soccer, Baseball, Football,, Basketball, Hockey, Golf, Tennis,  Fantasy Sports PLUS  Daily Crosswords, Sudok,  Assorted Puzzles, Chess, Bridge, Poker, Slots, Scrabble, Assorted card games, Internet games.",
        ),
    );

    public function up()
    {

        $x = 0;
        foreach ($this->data as $datum)
        {
            $x += 1;
            $faq = \Model_Faq::forge(array(
                'id' => $x,
                'question' => $datum[0],
                'answer' => $datum[1],
                'priority' => $x,
            ));
            $faq->save();
        }

        \DB::query("UPDATE `faqs` SET `created_at` = " . time() . ", `updated_at` = " . time() . " WHERE `id` <= " . count($this->data))->execute();
    }

    public function down()
    {
        \DB::query("DELETE FROM `faqs` WHERE `id` <= " . count($this->data))->execute();
    }
}