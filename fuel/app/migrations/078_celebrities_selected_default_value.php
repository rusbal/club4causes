<?php

namespace Fuel\Migrations;

class Celebrities_selected_default_value
{
	public function up()
	{
        \DB::query("ALTER TABLE `celebrities` CHANGE `selected` `selected` INT( 11 ) NOT NULL DEFAULT '0'")->execute();
	}

	public function down()
	{
        \DB::query("ALTER TABLE `celebrities` CHANGE `selected` `selected` INT( 11 ) NOT NULL")->execute();
	}
}