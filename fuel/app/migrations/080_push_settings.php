<?php

namespace Fuel\Migrations;

class Push_settings
{
    public function up()
    {
        \DB::query("INSERT INTO `settings` SET `id` = 1, `info` = 'easy_points', `key` = 'referring_a_new_member', `value` = '500'")->execute();
        \DB::query("INSERT INTO `settings` SET `id` = 2, `info` = 'easy_points', `key` = 'each_login', `value` = '200'")->execute();
        \DB::query("INSERT INTO `settings` SET `id` = 3, `info` = 'easy_points', `key` = 'participating_in_a_chat_room', `value` = '200'")->execute();
        \DB::query("INSERT INTO `settings` SET `id` = 4, `info` = 'easy_points', `key` = 'each_shopping_purchase', `value` = '2000'")->execute();
        \DB::query("INSERT INTO `settings` SET `id` = 5, `info` = 'easy_points', `key` = 'horoscope', `value` = '100'")->execute();

        \DB::query("INSERT INTO `settings` SET `id` = 6, `info` = 'sponsorship', `key` = 'members_page', `value` = '5000'")->execute();
        \DB::query("INSERT INTO `settings` SET `id` = 7, `info` = 'sponsorship', `key` = 'non_profits_page', `value` = '10000'")->execute();
        \DB::query("INSERT INTO `settings` SET `id` = 8, `info` = 'sponsorship', `key` = 'others', `value` = '10000'")->execute();

        \DB::query("INSERT INTO `settings` SET `id` = 9, `info` = 'challenges', `key` = 'contests.participation', `value` = '500'")->execute();
        \DB::query("INSERT INTO `settings` SET `id` = 10, `info` = 'challenges', `key` = 'contests.first', `value` = '3000'")->execute();
        \DB::query("INSERT INTO `settings` SET `id` = 11, `info` = 'challenges', `key` = 'contests.second', `value` = '2000'")->execute();
        \DB::query("INSERT INTO `settings` SET `id` = 12, `info` = 'challenges', `key` = 'contests.third', `value` = '1000'")->execute();

        \DB::query("INSERT INTO `settings` SET `id` = 13, `info` = 'challenges', `key` = 'games.participation', `value` = '500'")->execute();
        \DB::query("INSERT INTO `settings` SET `id` = 14, `info` = 'challenges', `key` = 'games.first', `value` = '3000'")->execute();
        \DB::query("INSERT INTO `settings` SET `id` = 15, `info` = 'challenges', `key` = 'games.second', `value` = '2000'")->execute();
        \DB::query("INSERT INTO `settings` SET `id` = 16, `info` = 'challenges', `key` = 'games.third', `value` = '1000'")->execute();

        \DB::query("INSERT INTO `settings` SET `id` = 17, `info` = 'challenges', `key` = 'quizzes.participation', `value` = '500'")->execute();
        \DB::query("INSERT INTO `settings` SET `id` = 18, `info` = 'challenges', `key` = 'quizzes.first', `value` = '3000'")->execute();
        \DB::query("INSERT INTO `settings` SET `id` = 19, `info` = 'challenges', `key` = 'quizzes.second', `value` = '- - -'")->execute();
        \DB::query("INSERT INTO `settings` SET `id` = 20, `info` = 'challenges', `key` = 'quizzes.third', `value` = '- - -'")->execute();

        \DB::query("INSERT INTO `settings` SET `id` = 21, `info` = 'challenges', `key` = 'survey_and_polls.participation', `value` = '500'")->execute();
        \DB::query("INSERT INTO `settings` SET `id` = 22, `info` = 'challenges', `key` = 'survey_and_polls.first', `value` = '- - -'")->execute();
        \DB::query("INSERT INTO `settings` SET `id` = 23, `info` = 'challenges', `key` = 'survey_and_polls.second', `value` = '- - -'")->execute();
        \DB::query("INSERT INTO `settings` SET `id` = 24, `info` = 'challenges', `key` = 'survey_and_polls.third', `value` = '- - -'")->execute();

        \DB::query("INSERT INTO `settings` SET `id` = 25, `info` = 'challenges', `key` = 'fantasy_games.participation', `value` = '1000'")->execute();
        \DB::query("INSERT INTO `settings` SET `id` = 26, `info` = 'challenges', `key` = 'fantasy_games.first', `value` = '5000'")->execute();
        \DB::query("INSERT INTO `settings` SET `id` = 27, `info` = 'challenges', `key` = 'fantasy_games.second', `value` = '3000'")->execute();
        \DB::query("INSERT INTO `settings` SET `id` = 28, `info` = 'challenges', `key` = 'fantasy_games.third', `value` = '2000'")->execute();

        \DB::query("INSERT INTO `settings` SET `id` = 29, `info` = 'challenges', `key` = 'slot_games.participation', `value` = 'TBD'")->execute();
        \DB::query("INSERT INTO `settings` SET `id` = 30, `info` = 'challenges', `key` = 'slot_games.first', `value` = 'TBD'")->execute();
        \DB::query("INSERT INTO `settings` SET `id` = 31, `info` = 'challenges', `key` = 'slot_games.second', `value` = 'TBD'")->execute();
        \DB::query("INSERT INTO `settings` SET `id` = 32, `info` = 'challenges', `key` = 'slot_games.third', `value` = 'TBD'")->execute();

        \DB::query("INSERT INTO `settings` SET `id` = 33, `info` = 'challenges', `key` = 'puzzles.participation', `value` = '500'")->execute();
        \DB::query("INSERT INTO `settings` SET `id` = 34, `info` = 'challenges', `key` = 'puzzles.first', `value` = '1000'")->execute();
        \DB::query("INSERT INTO `settings` SET `id` = 35, `info` = 'challenges', `key` = 'puzzles.second', `value` = '- - -'")->execute();
        \DB::query("INSERT INTO `settings` SET `id` = 36, `info` = 'challenges', `key` = 'puzzles.third', `value` = '- - -'")->execute();

        \DB::query("UPDATE `settings` SET `created_at` = " . time() . ", `updated_at` = " . time() . " WHERE `id` <= 36")->execute();
    }

    public function down()
    {
        \DB::query("DELETE FROM `settings` WHERE `id` <= 36")->execute();
    }
}