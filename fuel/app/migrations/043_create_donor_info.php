<?php

namespace Fuel\Migrations;

class Create_donor_info
{
	public function up()
	{
		\DBUtil::create_table('donor_info', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'donor_id' => array('constraint' => 11, 'type' => 'int'),
			'position' => array('constraint' => 45, 'type' => 'varchar', 'null' => true),
			'age' => array('constraint' => 3, 'type' => 'int', 'null' => true),
			'source_wealth' => array('constraint' => 45, 'type' => 'varchar', 'null' => true),
			'residence' => array('constraint' => 35, 'type' => 'varchar', 'null' => true),
			'country' => array('constraint' => 35, 'type' => 'varchar', 'null' => true),
			'education' => array('constraint' => 75, 'type' => 'varchar', 'null' => true),
			'marital_status' => array('constraint' => 20, 'type' => 'varchar', 'null' => true),
			'child_count' => array('constraint' => 2, 'type' => 'int', 'null' => true),
			'about' => array('constraint' => 1000, 'type' => 'varchar', 'null' => true),
			'donation_billion' => array('constraint' => '3,2', 'type' => 'decimal', 'null' => true),
			'networth_billion' => array('constraint' => '4,2', 'type' => 'decimal', 'null' => true),

		), array('id'));

		\DB::query("ALTER TABLE `donor_info` ADD INDEX ( `donor_id` )")->execute();
	}

	public function down()
	{
		\DBUtil::drop_table('donor_info');
	}
}