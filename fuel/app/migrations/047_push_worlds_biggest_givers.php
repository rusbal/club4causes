<?php

namespace Fuel\Migrations;

class Push_worlds_biggest_givers
{
    private function _raw_to_array($raw_str)
    {
        $out_arr = array();

        $rows = explode("\n", $raw_str);

        foreach ($rows as $row)
        {
            $row = trim($row);
            if ( ! $row)
                continue;

            list($key, $value) = explode(":", $row, 2);

            $out_arr[$key] = trim($value);
        }

        return $out_arr;
    }

    private function _refine_data($row)
    {
        $out_arr = array();

        foreach ($row as $key => $value)
        {
            $key = str_replace(' ', '_', strtolower($key));

            if ('donations' == $key)
            {
                preg_match_all('!\d+!', $value, $matches);

                $amt = implode('.', $matches[0]);
                if (strpos($value, 'million'))
                    $amt = $amt / 1000;
               
                $key = 'donation_billion';
                $value = $amt;
            }

            else if ('net_worth' == $key)
            {
                preg_match_all('!\d+!', $value, $matches);

                $amt = implode('.', $matches[0]);
                if (strpos($value, 'million'))
                    $amt = $amt / 1000;

                $key = 'networth_billion';
                $value = $amt;
            }

            else if ('name' == $key)
                list($value,) = explode(',', $value, 2);

            else if ('children' == $key) $key = 'child_count';
            else if ('source_of_wealth' == $key) $key = 'source_wealth';

            $out_arr[$key] = trim($value);
        }

        $n = str_replace(array(' ', '&'), array('-', 'and'), $out_arr['name']) . ".jpg";
        $out_arr['image_filename'] = \Helper::remove_non_ascii(strtolower($n));

        return $out_arr;
    }

    private function _get_data()
    {
        $data_arr = array();
        array_push($data_arr, "Name:Bill Gates, U.S.
            Donations: $28 billion 
            Net worth: $56 billion
            Position: Co-Chair, Bill & Melinda Gates Foundation
            Age: 57
            Source of Wealth: Microsoft, self-made 
            Residence: Medina, WA
            Country: United States 
            Education: Drop Out, Harvard University
            Marital Status: Married
            Children: 3
            About:￼Early on Gates dabbled in different areas, giving money to Harvard's computer science department, libraries, pilot high schools and local Seattle charities. His giving took off in 1999, g really when he funded his family foundation with $16 billion in Microsoft stock. Since then, with further contributions from Gates and pal Warren Buffett, the foundation has become the preeminent philanthropic institution in the world. Among its main initiatives: It will spend $10 billion over the next 10 years on vaccines. In the U.S., education--teacher training in particular--is its main project.
        ");
        array_push($data_arr, "Name:Warren Buffett, U.S.
            Donations: $8.3 billion 
            Net worth: $50 billion
            Position: CEO, Berkshire Hathaway
            Age: 82
            Source of Wealth: Berkshire Hathaway, self-made
            Residence: Omaha, NE
            Country: United States 
            Education: Master of Science, Columbia University; Bachelor of Arts / Science, University of Nebraska Lincoln
            Marital Status: Widowed, Remarried 
            Children: 3
            About:￼For many years Buffett insisted he would give all his money away at his death, but not before. He had a change of heart, and in 2006 made a pledge to give more than $30 billion over 20 years to the Gates Foundation. In 2010 he turned over $1.9 billion. Perhaps even more notably, he teamed with Gates last year to create the Giving Pledge, which has signed up 69 wealthy individuals and families to commit to giving the majority of their wealth to philanthropy.
        ");
        array_push($data_arr, "Name:George Soros, U.S.
            Donations: $8 billion
            Net worth: $14.5 billion
            Age: 82
            Source of Wealth: hedge funds, self- made
            Residence: Katonah, NY
            Country: United States 
            Education: Bachelor of Arts / Science, London School of Economics
            Marital Status: Divorced
            Children: 5
            About:￼An eclectic giver has doled out $8 billion since 1979, backing causes as diverse as clean-needle clinics in California and scientific research in Russia to helping the Roma, or Gypsies, in Eastern Europe. Through his Open Society, he has given $1.7 billion to human rights causes and to help promote democracy. Another $1.6 billion has gone to education.
        ");
        array_push($data_arr, "Name:Gordon Moore, U.S.
            Donations: $6.8 billion 
            Net worth: $4 billion
            Position: Cofounder and Chairman Emeritus, Intel 
            Age: 84
            Source of Wealth: Intel, self-made 
            Residence: Woodside, CA
            Country: United States 
            Education: Doctorate, California Institute of Technology; Bachelor of Arts / Science, University of California Berkeley
            Marital Status: Married
            Children: 2
            About:￼The Intel cofounder and former CEO handed over $6 billion in stock to the Gordon and Betty Moore Foundation in 2000. The foundation focuses on science, environmental conservation and nursing education. The last category was the brainchild of Moore's wife's Betty, who once received the wrong injection from a nurse in the hospital. Moore is also partially funding the construction of the world's biggest telescope; it's in Hawaii, where he lives part-time.
        ");
        array_push($data_arr, "Name:Carlos Slim Helú, Mexico
            Donations: $4 billion 
            Net worth: $74 billion
            Position: Honorary Chairman, América Móvil 
            Age: 73
            Source of Wealth: telecom, self-made 
            Residence: Mexico City, Mexico 
            Country: Mexico 
            Education: Bachelor of Arts / Science, Universidad Nacional Autonoma de Mexico
            Marital Status: Widowed 
            Children: 6
            About: World's richest man has publicly stated that he feels more good can be done from creating jobs than from band-aid charitable giving. Yet it turns out he gave $2 billion, mostly from dividends, to his Carlos Slim Foundation in 2006, and $2 billion in 2010. Most of its programs are focused on digital education and health. A $100 million gift to the Clinton Initiative project is helping pay for 50,000 cataract surgeries in Peru. With the Gates Foundation and the government of Spain, it's spending $150 million on nutrition and disease prevention in Central America.
        ");
        array_push($data_arr, "Name:George Kaiser, U.S.
            Donations: $4 billion 
            Net worth: $9.8 billion
            Position: Chairman, BOK Financial
            Age: 70
            Source of Wealth: oil & gas, banking 
            Residence: Tulsa, OK
            Country: United States 
            Education: Bachelor of Arts / Science, Harvard University; Master of Business Administration, Harvard University
            Marital Status: Married
            Children: 3
            About:￼\"It is the government's responsibility to ensure at birth that every child has the same opportunity. But that is a hoax at this point,\" says Kaiser. \"With what we know, it is morally offensive not to act.\" Tulsa's wealthiest man has acted by funding his George Kaiser Family Foundation with $4 billion over the past decade. It spends millions a year on medical clinics across Tulsa, helping women get off drugs, improving Tulsa's public schools, and developing early childhood education centers. His National Energy Policy Institute investigates ways to wean the U.S. off foreign oil.
        ");
        array_push($data_arr, "Name:Eli Broad, U.S.
            Donations: $2.6 billion
            Net worth: $5.8 billion
            Age: 79
            Source of Wealth: investments, self- made
            Residence: Los Angeles, CA
            Country: United States 
            Education: Bachelor of Arts / Science, Michigan State University
            Marital Status: Married
            Children: 2
            About: Broad, who made two fortunes, first as a homebuilder and later in the insurance-annuities industry, has lately been focused on philanthropy. He has tried to reform public education by giving awards to individual educators. Last year he got approval to build a museum in Los Angeles. His foundation also helps fund medical research; it has given more than half a billion dollars for a stem cell research institute at Harvard and M.I.T.
        ");
        array_push($data_arr, "Name:Azim Premji, India
            Donations: $2.1 billion 
            Net worth: $16.8 billion
            Position: Chairman, Wipro
            Age: 67
            Source of Wealth: software 
            Residence: Bangalore, India 
            Country: India 
            Education: Bachelor of Arts / Science, Stanford University 
            Marital Status: Married 
            Children: 2
            About: Head of tech titan Wipro started the Azim Premji Foundation in 2001, with an initial endowment of Wipro shares worth $125 million. Last year, in keeping with his avowed intent of giving away a chunk of his fortune, he donated shares worth $2 billion, to a trust that will go to the foundation to help public schools in India's heartland by training teachers and improving curriculum. His new Azim Premji University to train teachers is opening this July.
        ");
        array_push($data_arr, "Name:James Stowers, U.S.
            Donations: $2 billion 
            Net worth: $100 million
            About: The mutual fund tycoon has not been a member of the Forbes 400 since 2000, when he gave $1.2 billion to endow the Stowers Institute for Medical Research in Kansas City, Mo. Since then he and his wife Virginia have given millions more to the institute, which performs genetic research targeted at advancing the understanding of cancer, diabetes, heart disease and other conditions.
        ");
        array_push($data_arr, "Name:Michael Bloomberg, U.S.
            Donations: $1.8 billion 
            Net worth: $18.1 billion
            Position: Mayor, New York City, United States 
            Age: 70
            Source of Wealth: Bloomberg LP, self- made
            Residence: New York, NY
            Country: United States 
            Education: Master of Business Administration, Harvard Business School; Bachelor of Arts / Science, Johns Hopkins University
            Marital Status: Divorced
            Children: 2
            About: Bloomberg has apparently given to more than 850 charities. He's supported smoking cessation campaigns, tougher national gun control laws and New York's arts institutions. Over the years he's also given $200 million to his alma mater, Johns Hopkins University, where he sits on the board; plus $50 million to Princeton and Carnegie.
        ");
        array_push($data_arr, "Name:Li Ka-shing, Hong Kong
            Donations: $1.6 billion 
            Net worth: $26 billion
            Position: Chairman, Hutchison Whampoa 
            Age: 84
            Source of Wealth: diversified, self- made
            Residence: Hong Kong, Hong Kong 
            Country: Hong Kong 
            Education: Drop Out, High School 
            Marital Status: Widowed
            Children: 2
            About: Li started a foundation in 1980; one of his first gifts was to Shantou University. In January 2005 he sold his stake in CIBC and donated $1 billion in proceeds to his foundation. In 2006 Li announced he would eventually give away one-third of his fortune, which he refers to as his \"third son.\" Over the years he's sponsored children's centers, churches; provided money to fight hepatitis and avian flu. He's a major donor in Canada; in the U.S., he helped fund a new medical education building, which is named after him.
        ");
        array_push($data_arr, "Name:Herbert & Marion Sandler, U.S.
            Donations: $1.5 billion 
            Net worth: $800 million
            About: The Sandlers sold Golden West, the Bay Area bank they founded, for $25 billion to Wachovia in 2006 before the credit crisis began. They have since been mosly focused on philanthropy. The couple's most high-profile charity is investigative journalism group ProPublica, which won its first Pulitzer in 2010 and another this year. The Sandlers also are avid funders of civil liberties groups like the ACLU, and of scientific research, especially for diseases that affect low-income people, like asthma.
        ");
        array_push($data_arr, "Name:Dietmar Hopp, Germany
            Donations: $1.25 billion 
            Net worth: $850 million
            Age: 72
            Source of Wealth: SAP, self- made
            Residence: Waldorf, Germany 
            Country: Germany
            Education: Master of Science, University of Karlsruhe 
            Marital Status: Married 
            Children: 2
            About: With four former colleagues from IBM, Hopp cofounded giant German software company SAP. In 1995 he donated 70% of his SAP holdings to fund the nonprofit Dietmar Hopp Foundation, now one of Germany's and Europe's largest private foundations. Its focus is regional, in keeping with Hopp's desire to return a share of his wealth to the Rhein-Neckar area where he grew up, supporting youth sports, cancer research and healing diseases in children in particular. To date, the foundation has disbursed more than $380 million.
        ");
        array_push($data_arr, "Name:Michael Dell, U.S.
            Donations: $1.2 billion 
            Net worth: $14.6 billion
            Position: Chairman and CEO, Dell
            Age: 47
            Source of Wealth: Dell, self-made 
            Residence: Austin, TX
            Country: United States
            Education: Drop Out, The University of Texas at Austin 
            Marital Status: Married
            Children: 4
            About: In 1999 Michael and his wife Susan Dell started their eponymous foundation, which focuses on improving lives of poor urban children. Their money provides scholarships for high achievers and backs innovative charter schools. It also has a program to combat childhood obesity in the U.S., and is now working to improve lives of children living in India's urban slums.
        ");
        array_push($data_arr, "Name:Jon Huntsman, U.S.
            Donations: $1.2 billion 
            Net worth: $900 million
            Data from: March 2010
            Age: 75
            Source of Wealth: chemicals
            Residence: Salt Lake City, UT
            Country: United States 
            Education: Bachelor of Arts / Science, U of Penn Wharton; Master of Business Administration, U of Southern Cal
            Marital Status: Married 
            Children: 9
            About: In 1992 Jon Huntsman Sr. was diagnosed with prostate cancer; on the way to the hospital for treatment, he made three stops: First the chemicals mogul dropped by a homeless shelter and left a $1 million check. Then he stopped at a soup kitchen and handed over another $1 million check. Finally he dropped off $500,000 at the clinic that had found his malignancy. Since then Huntsman, who says he plans to give it all away before he dies, has donated the majority of his fortune to his cancer foundation.
        ");
        array_push($data_arr, "Name:Ted Turner, U.S.
            Donations: $1.2 billion 
            Net worth: $2.1 billion
            Age: 74
            Source of Wealth: cable television, self-made
            Residence: Atlanta, GA
            Country: United States 
            Education: Bachelor of Arts / Science, Brown University
            Marital Status: Divorced
            Children: 5
            About: Turner was perhaps the first celebrity $1 billion giver. While building his cable television empire the outspoken mogul famously criticized other billionaires for not giving money away while they were young and still had energy and ideas. In 1998 he announced a $1 billion pledge to United Nations-related causes; to date, he has given $841 million of that sum.
        ");
        array_push($data_arr, "Name:Klaus Tschira, Germany
            Donations: $1.1 billion 
            Net worth: $2 billion
            Age: 72
            Source of Wealth: SAP, self-made 
            Residence: Heidelberg, Germany 
            Country: Germany 
            Education: Master of Science, University of Karlsruhe
            Marital Status: Married
            Children: 2
            About:￼SAP cofounder donated 7 million of his shares in 1995 to found the Klaus Tschira Foundation, a nonprofit established to foster public understanding of mathematics, informatics and natural sciences (an amateur astronomer, Tschira has a small asteroid named after him). Retired from SAP in 1998, devotes his time to his foundation, promoting such projects as grants for single parents studying information science and economics and computer camp for vision-impaired youth.
        ");
        array_push($data_arr, "Name:Paul Allen, U.S.
            Donations: $1 billion
            Net worth: $13 billion
            Age: 60
            Source of Wealth: Microsoft, investments, self-made
            Residence: Mercer Island, WA
            Country: United States 
            Education: Drop Out, Washington State University
            Marital Status: Single
            About: Recent gifts including $26 million to Washington State University to complete its School of Global Animal Health has pushed him into the ranks of billion-dollar donors for the first time. In a note to Forbes, the Microsoft co-founder said that science had become a priority for his personal giving. His foundation gives grants to those exploring “edgy neuroscience.” His Brain Institute just released a $55 million digital atlas of the brain. Supports Pacific Northwest charities – “I think it's important to give at home” – but starting to look abroad.
        ");
        array_push($data_arr, "Name:Stephan Schmidheiny, Switzerland
            Donations: $1 billion 
            Net worth: $2.9 billion
            Age: 65
            Source of Wealth: investments 
            Residence: Zurich, Switzerland 
            Country:Switzerland 
            Education: Doctorate, University of Zurich
            Marital Status: Divorced
            Children: 2
            About: Fourth-generation member of a Swiss-German industrial fortune, his boyhood dream was to become a missionary. Retired in 2003 at age 47 to concentrate on philanthropy. That year he founded the Viva Trust, to which he donated pipe and forestry company Grupo Nueva, then worth $1 billion. Funds have primarily been distributed to the not-for-profit Avina, to help build networks of entrepreneurs and leaders in Latin America.
        ");
        return $data_arr;
    }

    public function up()
    {
        $f_data = array();
        $g_data = array();

        $data_arr = $this->_get_data();

        foreach ($data_arr as $datum) $f_data[] = $this->_raw_to_array($datum);
        foreach ($f_data as $row) $g_data[] = $this->_refine_data($row);

        foreach ($g_data as $row)
        {
            $donor = \Model_Donor::forge(array(
                'name' => $row['name'],
                'image_filename' => $row['image_filename'],
            ));

            $name = $row['name'];
            unset($row['name']);
            unset($row['image_filename']);

            if (ord(substr($row['about'], 0, 1)) == 239)
                $row['about'] = substr($row['about'], 3);

            $donor->info = new \Model_Donor_Info($row);

            if ($donor->save())
                \Cli::write("Inserted - " . $name);
            else
                \Cli::write("Error - " . $name);
        }
    }

    public function down()
    {
        $f_data = array();
        $g_data = array();

        $data_arr = $this->_get_data();

        foreach ($data_arr as $datum) $f_data[] = $this->_raw_to_array($datum);
        foreach ($f_data as $row) $g_data[] = $this->_refine_data($row);

        foreach ($g_data as $row)
        {
            if ($donor = \Model_Donor::find_by_name($row['name']))
            {
                if ($donor->delete())
                    \Cli::write("Deleted - " . $row['name']);
                else
                    \Cli::write("Error - " . $row['name']);
            }
        }
    }
}