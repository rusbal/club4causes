<?php

namespace Fuel\Migrations;

class Add_status_to_blog_posts
{
	public function up()
	{
		\DBUtil::add_fields('blog_posts', array(
			'title' => array('constraint' => 100, 'type' => 'varchar'),
			'status' => array('type' => 'smallint'),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('blog_posts', array(
			'title'
,			'status'

		));
	}
}