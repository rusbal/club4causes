<?php

namespace Fuel\Migrations;

class Create_ad_owner_adresses
{
	public function up()
	{
		\DBUtil::create_table('ad_owner_adresses', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'owner_id' => array('constraint' => 11, 'type' => 'int'),
			'city' => array('constraint' => 35, 'type' => 'varchar'),
			'state' => array('constraint' => 35, 'type' => 'varchar'),
			'country' => array('constraint' => 35, 'type' => 'varchar'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),

		), array('id'));

		\DB::query("ALTER TABLE `ad_owner_adresses` ADD INDEX ( `owner_id` )")->execute();
	}

	public function down()
	{
		\DBUtil::drop_table('ad_owner_adresses');
	}
}