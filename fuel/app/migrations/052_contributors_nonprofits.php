<?php

namespace Fuel\Migrations;

class Contributors_nonprofits
{
	public function up()
	{
        \DBUtil::create_table('contributors_nonprofits', array(
            'contributor_id' => array('constraint' => 11, 'type' => 'int'),
            'nonprofit_id' => array('constraint' => 11, 'type' => 'int'),

        ));
        
        \DB::query("ALTER TABLE `contributors_nonprofits` ADD INDEX ( `contributor_id` )")->execute();
        \DB::query("ALTER TABLE `contributors_nonprofits` ADD INDEX ( `nonprofit_id` )")->execute();
	}

	public function down()
	{
        \DBUtil::drop_table('contributors_nonprofits');
	}
}