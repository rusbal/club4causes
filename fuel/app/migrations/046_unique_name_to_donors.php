<?php

namespace Fuel\Migrations;

class Unique_name_to_donors
{
	public function up()
	{
        \DB::query("ALTER TABLE  `donors` ADD UNIQUE (`name` )")->execute();

	}

	public function down()
	{
        \DB::query("ALTER TABLE `donors` DROP INDEX `name`")->execute();

	}
}