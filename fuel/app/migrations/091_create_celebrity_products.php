<?php

namespace Fuel\Migrations;

class Create_celebrity_products
{
	public function up()
	{
		\DBUtil::create_table('celebrity_products', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'name' => array('constraint' => 100, 'type' => 'varchar'),
			'details' => array('type' => 'TEXT'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('celebrity_products');
	}
}