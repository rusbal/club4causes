<?php

namespace Fuel\Migrations;

class Create_celebrity_product_images
{
	public function up()
	{
		\DBUtil::create_table('celebrity_product_images', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'product_id' => array('constraint' => 11, 'type' => 'int'),
			'image_filename' => array('constraint' => 255, 'type' => 'varchar'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),

		), array('id'));

		\DB::query("ALTER TABLE `celebrity_product_images` ADD INDEX ( `product_id` )")->execute();
	}

	public function down()
	{
		\DBUtil::drop_table('celebrity_product_images');
	}
}