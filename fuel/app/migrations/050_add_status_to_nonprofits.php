<?php

namespace Fuel\Migrations;

class Add_status_to_nonprofits
{
	public function up()
	{
		\DBUtil::add_fields('nonprofits', array(
			'status' => array('constraint' => 32, 'type' => 'varchar', 'null' => true),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('nonprofits', array(
			'status'

		));
	}
}