<?php
/**
 * c4c.php
 */

class C4c
{
    const THEME = 'abaca';
    
    const RECAPTCHA_PRIVATE_KEY = "6LeV0NoSAAAAABHkIng-biUPhOJ0jKh-o77xDl_5";
    const RECAPTCHA_PUBLIC_KEY = "6LeV0NoSAAAAANdqZY7Ayqf5_7Co2YbBz3VrZSEP";

    const COMMONLY_SELECTED_COUNTRIES = "Canada, United States, United Kingdom";

    const NO_IMAGE = "/assets/img/no_image.jpg";

    public static function javascript()
    {
        return array(
            'constants.js',

            'cufon-yui.js',
            'calibri.font.js',
            'cufon_elements.js',
            'modernizr.custom.28468.js',
            'jquery.cslider.js?a=15feb2013',
            'jquery.colorbox.js',
            'spin.min.js',
            'jquery.spin.js',
        );
    }

    public static function admin_menu()
    {
        $menu = array();

        $menu[] = array(array("admin", "home", "Dashboard"));

        $menu[] = array(
            array("", "window", "Advertisement"),
            array("admin/ads/owner/view", "View Business Owners"),
            array("admin/ads/owner", "Add Owner"),
            array("admin/ads/page", "Add Pages"),
        );

        $menu[] = array(
            array("", "window", "Celebrities"),
            array("admin/celebrities", "Celebrities"),
            array("admin/celebrities/create", "Add A Celebrity"),
            array("admin/celebrities/select", "Select Celebrity"),
        );

        $menu[] = array(array("admin/comment", "window", "What's New"));

        $menu[] = array(
            array("", "window", "About Us"),
            array("admin/contributor", "View Contributors"),
            array("admin/contributor/create", "Add a Contributor"),
        );

        $menu[] = array(
            array("", "window", "FAQ"),
            array("admin/faq", "View FAQs"),
            array("admin/faq/create", "Add a FAQ"),
        );

        $menu[] = array(
            array("", "window", "Daily Mail"),
            array("admin/iemail/create", "Add Mail"),
            array("admin/iemail", "Set Active Mail"),
        );

        $menu[] = array(
            array("", "window", "Missing Children"),
            array("admin/missings", "View missing children"),
            array("admin/missings/create", "Add a missing child"),
        );

        $menu[] = array(
            array("", "window", "Pets"),
            array("admin/pets", "View Pets"),
            array("admin/pets/create", "Add a Pet"),
        );

        $menu[] = array(
            array("admin/settings", "window", "Point Allocations"),
        );

        $menu[] = array(
            array("", "window", "World's Top Givers"),
            array("admin/topgiver", "View Top Givers"),
            array("admin/topgiver/create", "Add a Top Giver"),
        );

        $menu[] = array(
            array("admin/tos", "window", "Terms of Service"),
        );
        
        $menu[] = array(
            array("admin/privacypolicy", "window", "Privacy Policy"),
        );
        
        $menu[] = array(
            array("admin/mediakit", "window", "Media Kit"),
        );

        $menu[] = array(
            array("", "window", "Shopping Mall"),
            array("admin/shopslide", "View Shop slides"),
            array("admin/shopslide/create", "Add a Shop slide"),
            array("admin/shopslide/coupon", "Add a Coupon"),
        );

        $set_selected = false;
        for ($x = count($menu) - 1; $x >= 0; $x--)
        {
            foreach ($menu[$x] as $item)
            {
                if ('/' . $item[0] == $_SERVER['REQUEST_URI'])
                    $set_selected = true;
            }

            if ($set_selected)
            {
                $menu[$x][0][] = true;
                break;
            }
        }

        if ( ! $set_selected)
            $menu[0][0][] = true;

        return $menu;
    }
}

// eof
