<?php

class Model_Faq extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'question',
		'answer',
		'priority' => array('default' => 0),
		'created_at',
		'updated_at',
	);

	protected static $_observers = array('\Orm\Observer_CreatedAt', '\Orm\Observer_UpdatedAt');

    public static function validate($factory = null)
    {
        $val = Validation::forge($factory);

        $val->add_field('question', "Question", 'required|min_length[10]|max_length[200]');
        $val->add_field('answer', "Answer", 'required|min_length[10]');

        return $val;
    }
}
