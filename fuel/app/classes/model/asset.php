<?php

class Model_Asset extends \Orm\Model
{
    protected static $_properties = array(
        'id',
        'type',
        'location',
        'description' => array('default' => ""),
        'created_at',
        'updated_at',
    );

    protected static $_observers = array('\Orm\Observer_CreatedAt', '\Orm\Observer_UpdatedAt');

    protected static $_has_one = array(
        'interface' => array(
            'key_from' => 'id',
            'model_to' => 'Model_Asset_Interface',
            'key_to' => 'asset_id',
            'cascade_save' => true,
            'cascade_delete' => true,
        ),
    );

    public static function multiple_asset_autosave($type, $input_arr, $description_arr)
    {
        $out_ids = array();
        $len = count($input_arr);

        for ($x = 0; $x < $len; $x++)
        {
            if ($input_arr[$x])
            {
                $asset = new Model_Asset();
                $asset->type = $type;
                $asset->location = $input_arr[$x];

                if ($description_arr[$x] && $description_arr[$x] != 'description...')
                    $asset->description = $description_arr[$x];

                if ($asset->save())
                    $out_ids[] = $asset->id;
            }
        }

        return $out_ids;
    }
}
