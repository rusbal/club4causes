<?php

class Model_Missing extends \Orm\Model
{
    protected static $_properties = array(
        'id',
        'name',
        'image_filename',
        'merchant_url',
        'priority' => array('default' => 0),
        'created_at',
        'updated_at',
    );

    protected static $_observers = array('\Orm\Observer_CreatedAt', '\Orm\Observer_UpdatedAt');

    protected static $_has_many = array(
        'assets' => array(
            'key_from' => 'id',
            'model_to' => 'Model_Asset_Interface',
            'key_to' => 'missing_id',
            'cascade_save' => true,
            'cascade_delete' => true,
        ),
    );

    public static function validate($factory = null)
    {
        $val = Validation::forge($factory);

        $val->add_field('name', "Name", 'required|min_length[2]|max_length[35]');
        $val->add_field('merchant_url', "Merchant URL", 'required|valid_url');

        return $val;
    }

    public static function get_selected()
    {
        $ar = DB::query("SELECT id, name, image_filename, merchant_url, priority FROM missings ORDER BY priority DESC LIMIT 1")->execute();
        foreach ($ar as $row) {
            if ($row['priority'] > 0)
                return $row;
        }

        $ar = DB::query("SELECT id, name, image_filename, merchant_url FROM missings ORDER BY updated_at DESC LIMIT 1")->execute();
        foreach ($ar as $row) {
            return $row;
        }
    }

    public static function get_selected_id()
    {
        $ar = DB::query("SELECT id, priority FROM missings ORDER BY priority DESC LIMIT 1")->execute();
        foreach ($ar as $row) {
            if ($row['priority'] > 0)
                return $row['id'];
        }

        $ar = DB::query("SELECT id, priority FROM missings ORDER BY updated_at DESC LIMIT 1")->execute();
        foreach ($ar as $row) {
            return $row['id'];
        }
    }
}
