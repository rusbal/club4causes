<?php

class Model_Nonprofit_Users extends \Orm\Model
{
	protected static $_table_name = 'nonprofit_users';

	protected static $_properties = array(
		'id',
		'nonprofit_id',
        'user_id',
		'created_at',
	);

    protected static $_has_one = array(
        'user' => array(
            'key_from' => 'user_id',
            'model_to' => 'Model_User',
            'key_to' => 'id',
            'cascade_save' => true,
        ),
    );

	protected static $_observers = array('\Orm\Observer_CreatedAt');
}
