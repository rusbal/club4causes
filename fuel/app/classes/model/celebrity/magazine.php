<?php

class Model_Celebrity_Magazine extends \Orm\Model
{
	protected static $_properties = array(
		'id',
        'name',
		'created_at',
        'updated_at',
    );

    protected static $_observers = array('\Orm\Observer_CreatedAt', '\Orm\Observer_UpdatedAt');

   protected static $_many_many = array(
        'celebrities' => array(
            'key_through_from' => 'magazine_id',
            'table_through' => 'Model_Celebrity_Magazine_Cover',
            'key_through_to' => 'celebrity_id',
            'model_to' => 'Model_Celebrity',
            'cascade_save' => true,
        ),
    ); 
}
