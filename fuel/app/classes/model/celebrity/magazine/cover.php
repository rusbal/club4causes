<?php

class Model_Celebrity_Magazine_Cover extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'celebrity_id',
		'magazine_id',
		'image_filename',
		'date',
		'created_at',
        'updated_at',
    );

    protected static $_observers = array('\Orm\Observer_CreatedAt', '\Orm\Observer_UpdatedAt');

    

}
