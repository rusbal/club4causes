<?php

class Model_Celebrity_Product extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'name',
		'details',
		'created_at',
        'updated_at',
    );

    protected static $_observers = array('\Orm\Observer_CreatedAt', '\Orm\Observer_UpdatedAt');

    protected static $_many_many = array(
        'celebrities' => array(
            'key_through_from' => 'product_id',
            'table_through' => 'celebrity_products',
            'key_through_to' => 'celebrity_id',
            'model_to' => 'Model_Celebrity',
            'cascade_save' => true,
        ),
    );

    protected static $_has_many = array(
        'images' => array(
            'key_from' => 'id',
            'model_to' => 'Model_Celebrity_Magazine',
            'key_to' => 'product_id',
            'cascade_save' => true,
            'cascade_delete' => true,
        ),
    );
}
