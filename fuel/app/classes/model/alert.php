<?php

class Model_Alert extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'message',
		'permalink',
		'created_at',
        'updated_at',
    );

    protected static $_observers = array('\Orm\Observer_CreatedAt', '\Orm\Observer_UpdatedAt');

    protected static $_has_many = array(
        'user_alerts' => array(
            'key_from' => 'id',
            'model_to' => 'Model_User_Alert',
            'key_to' => 'alert_id',
            'cascade_save' => true,
            'cascade_delete' => true,
        ),
    );
}
