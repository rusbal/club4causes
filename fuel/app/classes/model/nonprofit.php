<?php

class Model_Nonprofit extends \Orm\Model
{
    protected static $_table_name = 'nonprofits';

    protected static $_properties = array(
        'id',
        'name',
        'running_points' => array('default' => 0),
        'info' => array('default' => ''),
        'website_url' => array('default' => ''),
        'is_contacted' => array('default' => 0),
        'status' => array('default', null),
        'created_at',
        'updated_at',
    );

    protected static $_observers = array('\Orm\Observer_CreatedAt', '\Orm\Observer_UpdatedAt');

    protected static $_has_one = array(
        'address' => array(
            'key_from' => 'id',
            'model_to' => 'Model_Nonprofit_Address',
            'key_to' => 'nonprofit_id',
            'cascade_save' => true,
            'cascade_delete' => true,
        )
    );

    protected static $_has_many = array(
        'points' => array(
            'key_from' => 'id',
            'model_to' => 'Model_Nonprofit_Points',
            'key_to' => 'nonprofit_id',
            'cascade_save' => true,
            'cascade_delete' => true,
        ),
        'np_users' => array(
            'key_from' => 'id',
            'model_to' => 'Model_Nonprofit_Users',
            'key_to' => 'nonprofit_id',
            'cascade_save' => true,
            'cascade_delete' => true,
        ),
    );

    protected static $_many_many = array(
        'celebrities' => array(
            'key_through_from' => 'nonprofit_id',
            'table_through' => 'celebrities_nonprofits',
            'key_through_to' => 'celebrity_id',
            'model_to' => 'Model_Celebrity',
            'cascade_save' => true,
        ),
        'contributors' => array(
            'key_through_from' => 'nonprofit_id',
            'table_through' => 'contributors_nonprofits',
            'key_through_to' => 'contributor_id',
            'model_to' => 'Model_Contributor',
            'cascade_save' => true,
        ),
        'members' => array(
            'key_from' => 'id',
            'key_through_from' => 'nonprofit_id',
            'table_through' => 'members_nonprofits',
            'key_through_to' => 'member_id',
            'model_to' => 'Model_Member',
            'key_to' => 'id',
            'cascade_save' => true,
        ),
    );

    public static function validate($factory)
    {
        $val = Validation::forge('nonprofit'.$factory);

        if (is_numeric($factory))
            $val->add_field('nprofit_name_'.$factory, 'Name', 'required');
        else
            $val->add_field('nprofit_name', 'Name', 'required');

        return $val;
    }

    public static function validate_name($factory)
    {
        $val = Validation::forge($factory);

        $val->add_field('first_name', 'First Name', 'required|max_length[25]');
        $val->add_field('last_name', 'Last Name', 'required|max_length[25]');
        $val->add_field('username', 'Username', 'required|max_length[25]');

        return $val;
    }

    public static function get_id_autosave($nonprofit)
    {
        $row = self::find()->where('name', $nonprofit)->get_one();

        if ($row)
            return $row->id;
        else
        {
            $ar = self::forge(array('name' => $nonprofit));
            if ($ar->save())
                return $ar->id;
            else
                trigger_error("Model_Nonprofit::get_id_autosave('$nonprofit') failed.", E_USER_ERROR);
        }
    }

    public static function get_model_autosave($nonprofit, $hash = null)
    {
        if (is_null($hash))
            $hash = md5(Controller_Admin_Base::APP_SALT . rand(0, 10000));

        if ($ar = self::find()->where('name', $nonprofit)->get_one())
        {
            if ('1' != $ar->status)
            {
                /**
                 * Nonprofit.status used to temporarily store hash to validate nonprofit first visit
                 */
                $ar->status = $hash;
                $ar->save();
            }
        }
        else
        {
            /**
             * Nonprofit.status used to temporarily store hash to validate nonprofit first visit
             */
            $ar = self::forge(array('name' => $nonprofit, 'status' => $hash));
            $ar->save();
        }

        return $ar;
    }

    public static function get_nonprofit_arr()
    {
        $arr = array();
        $rows = DB::query('SELECT name FROM nonprofits ORDER BY name')->execute();
        foreach ($rows as $row)
            $arr[] = $row['name'];
        return $arr;
    }

    public static function get_selecting_member($id = null)
    {
        if (is_null($id))
            return false;

        $sql = "SELECT u.id, u.first_name, u.last_name
            FROM nonprofits AS np
                INNER JOIN member_nonprofit_selections AS s ON s.nonprofit_id = np.id
                INNER JOIN members AS m ON m.id = s.member_id
                INNER JOIN users AS u ON u.id = m.user_id
            WHERE np.id = $id
            ORDER BY s.created_at DESC
            LIMIT 1";

        if ($row = DB::query($sql)->execute())
            return array(
                'id' => $row[0]['id'],
                'first_name' => $row[0]['first_name'],
                'last_name' => $row[0]['last_name'],
            );
        else
            return false;
    }
}
