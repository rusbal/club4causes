<?php

class Model_Shopslide extends \Orm\Model
{
    protected static $_properties = array(
        'id',
        'string',
        'image_filename',
        'merchant_url',
        'is_active' => array('default' => true),
        'created_at',
        'updated_at',
    );

    protected static $_observers = array('\Orm\Observer_CreatedAt', '\Orm\Observer_UpdatedAt');

    public static function validate($factory = null)
    {
        $val = Validation::forge($factory);

        $val->add_field('string', "String", 'required|min_length[10]');
        // $val->add_field('merchant_url', "Merchant URL", 'required|valid_url');

        return $val;
    }
}
