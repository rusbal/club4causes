<?php

class Model_User extends \Orm\Model
{
    protected static $_table_name = 'users';

    protected static $_properties = array(
        'id',
        'username',
        'password',
        'group',
        'email',
        'last_login' => array('default' => '0'),
        'login_hash' => array('default' => '0'),
        'remember_me' => array('default' => null),
        'profile_fields' => array('default' => '0'),
        'first_name',
        'last_name',
        'alias',
        'birth_date',
        'birth_mm_dd',
        'phone_number' => array('default' => ''),
        'image_filename' => array('default' => ''),
        'gender',
        'created_at',
        'updated_at',
    );

    protected static $_observers = array('\Orm\Observer_CreatedAt', '\Orm\Observer_UpdatedAt');

    protected static $_has_one = array(
        'member' => array(
            'key_from' => 'id',
            'model_to' => 'Model_Member',
            'key_to' => 'user_id',
            'cascade_save' => true,
            'cascade_delete' => true,
        ),
        'no_id_requests' => array(
            'key_from' => 'id',
            'model_to' => 'Model_User_No_Id_Request',
            'key_to' => 'user_id',
            'cascade_save' => true,
            'cascade_delete' => true,
        ),
    );

    protected static $_has_many = array(
        'blog_posts' => array(
            'key_from' => 'id',
            'model_to' => 'Model_Blog_Post',
            'key_to' => 'user_id',
            'cascade_save' => true,
            'cascade_delete' => true,
        ),
        'sent_alerts' => array(
            'key_from' => 'id',
            'model_to' => 'Model_User_Alert',
            'key_to' => 'from_id',
            'cascade_save' => true,
            'cascade_delete' => true,
        ),
        'received_alerts' => array(
            'key_from' => 'id',
            'model_to' => 'Model_User_Alert',
            'key_to' => 'to_id',
            'cascade_save' => true,
            'cascade_delete' => true,
        ),
    );

    protected static $_many_many = array(
        'users' => array(
            'key_through_from' => 'user_id',
            'table_through' => 'nonprofits_users',
            'key_through_to' => 'nonprofit_id',
            'model_to' => 'Model_Nonprofit',
            'cascade_save' => true,
        ),
    );

    public static function validate()
    {
        $val = Validation::forge('user');

        $val->add_field('password', 'Password', 'required|max_length[255]');
        $val->add_field('email', 'Email', 'required|valid_email');
        $val->add_field('first_name', 'First Name', 'required|max_length[25]');
        $val->add_field('last_name', 'Last Name', 'required|max_length[25]');
        $val->add_field('alias', 'Alias', 'required|max_length[25]');

        // $val->add_field('birth_date', 'Birth Date', 'required|min_length[5]|max_length[10]');
        $val->add_field('birth_date_day', 'Birth Day', 'required');
        $val->add_field('birth_date_month', 'Birth Month', 'required');

        return $val;
    }

    public static function validate_password()
    {
        $val = Validation::forge('password');

        $val->add_callable(new MyRules());

        $val->add_field('password', 'Password', 'required|min_length[6]|max_length[255]')
            ->add_rule('should_be_the_same');
        $val->add_field('confirm_password', 'Confirm Password', 'required|min_length[6]|max_length[255]')
            ->add_rule('should_be_the_same');

        return $val;
    }

    public static function generate_unique_username($username)
    {
        $username = str_replace(' ', '', $username);
        
        $matches = self::query()->where(array('username', 'like', "$username%"))->get();    

        if (count($matches) == 0)
            return $username;

        $int = 0;

        foreach ($matches as $match)
        {
            $this_int = (int) Helper::extract_integer($match->username);
            if ($this_int > $int)
                $int = $this_int;
        }
        return $username . ($int + 1);
    }

    public static function generate_birthday_celebrants()
    {
        /**
         * SQL below is the correct one
         */
        // $sql = "SELECT u.first_name, u.last_name, u.email, m.id
        //     FROM users AS u
        //     INNER JOIN members AS m ON (m.user_id = u.id)
        //     WHERE EXTRACT(MONTH FROM birth_date) = " . date('m');

        /**
         * This SQL is used temporarily to display more birthday celebrants
         */
        $sql = "SELECT u.first_name, u.last_name, u.email, m.id
            FROM users AS u
            INNER JOIN members AS m ON (m.user_id = u.id)";

        $query = DB::query($sql)->execute();

        $birthday_users = array();
        foreach ($query as $row)
        {
            $row['avatar'] = self::get_avatar($row['id'], 74);
            $birthday_users[] = $row;
        }

        return $birthday_users;
    }

    public static function get_avatar($id, $size = 122)
    {
        $user = self::find($id);
        if ($user) // && property_exists($user, 'email'))
            return Helper::get_avatar($id, $user->email, $size);

        return 'http://' . $_SERVER['HTTP_HOST'] . '/assets/img/no_image_2.jpg';
    }

    public static function get_superuser_emails()
    {
        $emails = array();

        $query = self::find()->where('group', 999)->get();
        foreach ($query as $row)
        {
            $emails[] = array(
                'first_name' => $row['first_name'],
                'last_name' => $row['last_name'],
                'email' => $row['email'],
            );
        }

        return $emails;
    }

    public static function save_first_nonprofit_user($nonprofit_id = null, $name = null, $email = null, $password = null)
    {
        if ( ! ($nonprofit_id && $name && $email && $password))
            return array(false, 'Incomplete parameter');

        $user = Model_User::find_by_email($email);
        if ($user)
        {
            if ($user->last_login > 0)
            {
                /**
                 * This email belongs to an active user
                 */
                return array(false, 'Email belongs to existing user');
            }

            /**
             * Email exists but it was not used to sign-in
             */
            $user->delete();
        }

        try {
            $new_user = Model_User::forge(array(
                'username' => $email,
                'alias' => $email,
                'password' => Auth::instance()->hash_password($password),
                'group' => Controller_Base::GROUP_NONPROFIT,
                'email' => $email,
                'first_name' => "",
                'last_name' => "",
                'gender' => "",
            ));
            $new_user->save();
        }
        catch (\Database_Exception $e)
        {
            if ($e->getCode() == 23000)
                return array(false, 'Duplicate email');
            else
                return array(false, 'Unknown error');
        } 

        /**
         * When successful, return user (Active Record) as second parameter
         */
        return array(true, $new_user);
    }

    public static function forum_data($username, $pw, $email)
    {
        $sql = "INSERT INTO `zf_users` SET 
            `username` = '". $username ."' ,
            `group_id` = 4,
            `password` = '". sha1($pw) ."',
            `email` = '". $email ."',
            `email_setting` = 1,
            `timezone` = 0 ,
            `dst` = 0,
            `language` = 'English',
            `style` = 'Air',
            `registered` = '". time() ."',
            `registration_ip` = '". $_SERVER['REMOTE_ADDR'] ."',
            `last_visit` = '". time() . "'
        ";

        $query = DB::query($sql)->execute();
    }
}

// eof
