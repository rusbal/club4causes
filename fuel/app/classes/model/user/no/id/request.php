<?php

class Model_User_No_Id_Request extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'user_id',
		'hash',
        'created_at',
	);

	protected static $_observers = array('\Orm\Observer_CreatedAt');

    protected static $_belongs_to = array(
        'user' => array(
            'key_from' => 'user_id',
            'model_to' => 'Model_User',
            'key_to' => 'id',
            'cascade_save' => true,
        ),
    );

    public static function delete_for_user($user_id)
    {
        return DB::query('DELETE FROM user_no_id_requests WHERE user_id = ' . $user_id)->execute();
    }
}
