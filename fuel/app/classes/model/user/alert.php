<?php

class Model_User_Alert extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'from_id',
		'to_id',
		'alert_id',
        'is_read'=> array('default' => 0),
		'created_at',
        'updated_at',
    );

    protected static $_observers = array('\Orm\Observer_CreatedAt', '\Orm\Observer_UpdatedAt');

    protected static $_belongs_to = array(
        'user' => array(
            'key_from' => 'to_id',
            'model_to' => 'Model_User',
            'key_to' => 'id',
            'cascade_save' => true,
        ),
        'alert' => array(
            'key_from' => 'alert_id',
            'model_to' => 'Model_Alert',
            'key_to' => 'id',
            'cascade_save' => true,
        ),
    );
}
