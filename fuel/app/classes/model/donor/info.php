<?php

class Model_Donor_Info extends \Orm\Model
{
    protected static $_table_name = 'donor_info';

    protected static $_properties = array(
        'id',
        'donor_id',
        'position' => array('default' => null),
        'age' => array('default' => null),
        'source_wealth' => array('default' => null),
        'residence' => array('default' => null),
        'country' => array('default' => null),
        'education' => array('default' => null),
        'marital_status' => array('default' => null),
        'child_count' => array('default' => null),
        'about' => array('default' => null),
        'donation_billion' => array('default' => null),
        'networth_billion' => array('default' => null),
        'data_from' => array('default' => null),
    );

    public static function get_properties()
    {
        return static::$_properties;
    }

    protected static $_belongs_to = array(
        'donor' => array(
            'key_from' => 'donor_id',
            'model_to' => 'Model_Donor',
            'key_to' => 'id',
            'cascade_save' => true,
        )
    );

    public static function label($field)
    {
        $labels = array(
            'position' => '',
            'age' => 'Age',
            'source_wealth' => 'Source of Wealth',
            'residence' => 'Residence',
            'country' => 'Country of Citizenship',
            'education' => 'Education',
            'marital_status' => 'Marital Status',
            'child_count' => 'Children',
            'about' => '',
            'donation_billion' => 'Donations',
            'networth_billion' => 'Net worth',
            'data_from' => 'Data from',
        );
        return isset($labels[$field]) ? $labels[$field] : $field;
    }
}
