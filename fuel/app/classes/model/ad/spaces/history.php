<?php

class Model_Ad_Spaces_History extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'owner_id',
		'space_id',
		'activity',
		'date_time',
		'created_at',
        'updated_at',
	);

	protected static $_observers = array('\Orm\Observer_CreatedAt', '\Orm\Observer_UpdatedAt');

}
