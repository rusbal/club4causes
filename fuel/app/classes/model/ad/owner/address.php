<?php

class Model_Ad_Owner_Address extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'owner_id',
		'city',
		'state',
		'country',
        'created_at',
        'updated_at',
	);

    protected static $_observers = array('\Orm\Observer_CreatedAt', '\Orm\Observer_UpdatedAt');

    public static function validate($factory = null)
    {
        $val = Validation::forge($factory);

        $val->add_field('city', "City", 'required|min_length[6]');
        $val->add_field('state', "State", 'required|min_length[6]');
        $val->add_field('country', "Country", 'required|min_length[6]');

        return $val;
    }
}
