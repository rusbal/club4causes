<?php

class Model_Ad_Object extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'owner_id',
		'image_filename',
		'description',
		'target_url',
        'created_at',
        'updated_at',
	);

    protected static $_observers = array('\Orm\Observer_CreatedAt', '\Orm\Observer_UpdatedAt');

	protected static $_belongs_to = array(
        'owner' => array(
            'key_from' => 'owner_id',
            'model_to' => 'Model_Ad_Owner',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => true
        ),
    );

    protected static $_many_many = array(
        'spaces' => array(
            'key_through_from' => 'object_id',
            'table_through' => 'Model_Ad_Instance',
            'key_through_to' => 'space_id',
            'model_to' => 'Model_Ad_Space',
            'cascade_save' => true,
        ),
    );

    public static function validate($factory = null)
    {
        $val = Validation::forge($factory);

        $val->add_field('target_url[]', "Target_Url[]", 'required|min_length[6]');
        $val->add_field('target_description[]', "Target_Description[]", 'required|min_length[6]');

        return $val;
    }

    public static function multiple_object_autosave($url, $id, $image, $desc)
    {
        $out_ids = array();
        $len = count($url);

        for ($x = 0; $x < $len; $x++)
        {
            if ($url[$x])
            {
                $obj = new Model_Ad_Object();
                $obj->target_url = $url[$x];
                $obj->owner_id = $id;
                $obj->description = $desc[$x];
                $obj->image_filename = $image;

                if ($obj->save())
                    $out_ids[] = $obj->id;
            }
        }
        return $out_ids;
    }

}
