<?php

class Model_Ad_Page extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'page_path',
		'description',
        'created_at',
        'updated_at',
	);

    protected static $_observers = array('\Orm\Observer_CreatedAt', '\Orm\Observer_UpdatedAt');

    protected static $_has_many = array(
        'spaces' => array(
            'key_from' => 'id',
            'model_to' => 'Model_Ad_Space',
            'key_to' => 'page_id',
            'cascade_save' => true,
            'cascade_delete' => true,
        ),
    );

    public static function validate($factory = null)
    {
        $val = Validation::forge($factory);

        $val->add_field('page[]', "Page[]", 'required|min_length[6]');
        $val->add_field('page_description[]', "Page_Description[]", 'required|min_length[6]');

        return $val;
    }

    public static function multiple_page_autosave($page, $desc, $code, $code_desc)
    {
        $out_ids = array();
        $len = count($page);

        for ($x = 0; $x < $len; $x++)
        {
            if ($page[$x])
            {
                $ad_page = new Model_Ad_Page();
                $ad_page->page_path = $page[$x];
                $ad_page->description = $desc[$x];

                $ad_page->spaces[] = new Model_Ad_Space (array(
                   'location_code' => $code[$x],
                   'description' => $code_desc[$x],
                ));

                if ($ad_page->save())
                {
                     $out_ids[] = $ad_page->id;
                    Session::set_flash('okhide', 'Page was successfully saved!');
                    Response::redirect('admin/ads/page');
                }
            }
        }

        return $out_ids;
    }

}
