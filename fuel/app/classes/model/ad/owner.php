<?php

class Model_Ad_Owner extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'user_id',
		'business_name',
        'created_at',
        'updated_at',
	);

	protected static $_observers = array('\Orm\Observer_CreatedAt', '\Orm\Observer_UpdatedAt');

    protected static $_has_many = array(
        'objects' => array(
            'key_from' => 'id',
            'model_to' => 'Model_Ad_Object',
            'key_to' => 'owner_id',
            'cascade_save' => true,
            'cascade_delete' => true,
        ),
    );

    protected static $_has_one = array(
         'user' => array(
            'key_from' => 'user_id',
            'model_to' => 'Model_User',
            'key_to' => 'id',
            'cascade_save' => true,
        ),       
         'address' => array(
            'key_from' => 'id',
            'model_to' => 'Model_Ad_Owner_Address',
            'key_to' => 'owner_id',
            'cascade_save' => true,
        ),
    );

    public static function validate($factory = null)
    {
        $val = Validation::forge($factory);

        $val->add_field('business_name', "Business_Name", 'required|min_length[6]');

        return $val;
    }
}
