<?php

class Model_Ad_Space extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'page_id',
		'location_code',
		'description',
        'created_at',
        'updated_at',
	);

    protected static $_observers = array('\Orm\Observer_CreatedAt', '\Orm\Observer_UpdatedAt');

	protected static $_belongs_to = array(
        'page' => array(
            'key_from' => 'page_id',
            'model_to' => 'Model_Ad_Page',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => true
        ),
    );

    protected static $_many_many = array(
        'objects' => array(
            'key_through_from' => 'space_id',
            'table_through' => 'Ad_Instance',
            'key_through_to' => 'object_id',
            'model_to' => 'Model_Ad_Object',
            'cascade_save' => true,
        ),
    );

    public static function validate($factory = null)
    {
        $val = Validation::forge($factory);

        $val->add_field('location_code[]', "Location_Code[]", 'required|max_length[6]');

        return $val;
    }
    
}
