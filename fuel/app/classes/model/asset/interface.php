<?php

class Model_Asset_Interface extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'asset_id',
		'contributor_id',
	);

    protected static $_has_one = array(
        'asset' => array(
            'key_from' => 'asset_id',
            'model_to' => 'Model_Asset',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => true,
        ),
    );
}
