<?php

class Model_Coupon extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'business_name',
		'coupon',
		'business_url',
		'donation',
		'details',
		'image_filename',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array('\Orm\Observer_CreatedAt', '\Orm\Observer_UpdatedAt');

	public static function validate($factory = null)
    {
        $val = Validation::forge($factory);

        $val->add_field('business_name', "Business_name", 'required|min_length[3]');
        $val->add_field('coupon', "Coupon", 'required|min_length[3]');
        $val->add_field('business_url', "Business_url", 'required|min_length[5]');
        $val->add_field('details', "Details", 'required|min_length[15]');

        return $val;
    }
}
