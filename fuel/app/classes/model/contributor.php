<?php

class Model_Contributor extends \Orm\Model
{
	protected static $_table_name = 'contributors';

	protected static $_properties = array(
		'id',
		'name',
		'image_filename' => array('default' => ''),
		'organization_name',
		'organization_logo' => array('default' => ''),
		'organization_text',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array('\Orm\Observer_CreatedAt', '\Orm\Observer_UpdatedAt');

    protected static $_many_many = array(
        'nonprofits' => array(
            'key_through_from' => 'contributor_id',
            'table_through' => 'contributors_nonprofits',
            'key_through_to' => 'nonprofit_id',
            'model_to' => 'Model_Nonprofit',
            'cascade_save' => true,
        ),
    );

    protected static $_has_many = array(
        'assets' => array(
            'key_from' => 'id',
            'model_to' => 'Model_Asset_Interface',
            'key_to' => 'contributor_id',
            'cascade_save' => true,
            'cascade_delete' => true,
        ),
    );

	public static function validate($factory = null)
	{
		$val = Validation::forge($factory);

		$val->add_field('name', "Name", 'required|min_length[3]|max_length[45]');
		$val->add_field('organization_name', "Organization", 'required|min_length[3]|max_length[45]');
		$val->add_field('about', "About", 'required|min_length[45]');

		return $val;
	}
}
