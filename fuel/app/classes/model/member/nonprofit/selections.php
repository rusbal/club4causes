<?php

class Model_Member_Nonprofit_Selections extends \Orm\Model
{
    protected static $_table_name = 'member_nonprofit_selections';

    protected static $_properties = array(
        'id',
        'member_id',
        'nonprofit_id',
        'reason' => array('default' => ''),
        'created_at',
        'updated_at',
    );

    protected static $_observers = array('\Orm\Observer_CreatedAt', '\Orm\Observer_UpdatedAt');

    protected static $_belongs_to = array(
        'member' => array(
            'key_from' => 'member_id',
            'model_to' => 'Model_Member',
            'key_to' => 'id',
            'cascade_save' => true,
        ),
    );

    public static function get_nonprofit_selections($user_id)
    {   
        $arr = array();
        
        $result = self::query()->where(array('member_id', $user_id))->get();

        foreach ($result as $row)
            $arr[$row->nonprofit_id] = $row->reason;

        return $arr;
    }

    public static function get_supporter_of_nonprofit($nonprofit_id)
    {
        return self::find_by_nonprofit_id($nonprofit_id, array('order_by' => array('created_at' => 'desc')));
    }

    public static function get_nonprofit_name($user_id)
    {
        $sql = "SELECT nonprofits.name, nonprofits.id, users.username FROM users, members, nonprofits, members_nonprofits
                    WHERE nonprofits.id = members_nonprofits.nonprofit_id AND members.id = members_nonprofits.member_id
                    AND users.id = members.id AND users.id = $user_id";

        $query = DB::query($sql)->execute();

        $arr = array();

        foreach ($query as $row)
        {
            $arr[] = $row['name'];
            $username[] = $row['username'];

            foreach ($username as $user)
                $user = $user;
        }
            
        foreach ($arr as $result)
        {
            $sql2 = "SELECT forum_name FROM zf_forums where forum_name = '" . $result . "'";
            $query2 = DB::query($sql2)->execute();

            $num_rows = count($query2);
            if (!$num_rows)
            {
                $sql3 = "INSERT INTO zf_forums SET forum_name = '". $result ."',
                    forum_desc = '". $result ."',
                    redirect_url = NULL,
                    moderators = NULL,
                    num_topics = 0,
                    num_posts = 0,
                    last_post = '". time() ."',
                    last_post_id = NULL,
                    last_poster = '". $user ."',
                    sort_by = 0,
                    disp_position = 1,
                    cat_id = 1
                ";

                $query3 = DB::query($sql3)->execute();
            }
        }
    }
}
