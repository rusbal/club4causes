<?php

class Model_Blog_Comment extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'post_id',
		'comment',
        'user_id',
        'name',
        'email',
        'website',
        'status',
		'created_at',
	);

    protected static $_observers = array('\Orm\Observer_CreatedAt');

    protected static $_belongs_to = array(
        'post' => array(
            'key_from' => 'post_id',
            'model_to' => 'Model_Blog_Post',
            'key_to' => 'id',
            'cascade_save' => true,
        ),
        'user' => array(
            'key_from' => 'user_id',
            'model_to' => 'Model_User',
            'key_to' => 'id',
            'cascade_save' => true,
        ),
    );

    public static function validate($factory = null)
    {
        $val = Validation::forge($factory);

        $val->add_field('name', "Name", 'required|min_length[3]');
        $val->add_field('email', "Email", 'required|valid_email');
        $val->add_field('comment', "Comment", 'required|min_length[10]');

        return $val;
    }

    public static function get_gravatar($email, $size = 122)
    {
        return Helper::get_avatar(null, $email, $size);
    }

    public static function time_elapsed($timestamp)
    {
        return Helper::time_difference($timestamp);
    }
}
