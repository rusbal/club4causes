<?php

class Model_Blog_Post extends \Orm\Model
{
    protected static $_properties = array(
        'id',
        'user_id',
        'title',
        'post',
        'created_at',
        'updated_at',
    );

    protected static $_observers = array('\Orm\Observer_CreatedAt', '\Orm\Observer_UpdatedAt');

    protected static $_many_many = array(
        'categories' => array(
            'key_through_from' => 'post_id',
            'table_through' => 'blog_posts_categories',
            'key_through_to' => 'category_id',
            'model_to' => 'Model_Blog_Category',
            'cascade_save' => true,
        ),
    );
    
    protected static $_has_many = array(
        'comments' => array(
            'key_from' => 'id',
            'model_to' => 'Model_Blog_Comment',
            'key_to' => 'post_id',
            'cascade_save' => true,
            'cascade_delete' => true,
        ),
    );

    protected static $_belongs_to = array(
        'user' => array(
            'key_from' => 'user_id',
            'model_to' => 'Model_User',
            'key_to' => 'id',
            'cascade_save' => true,
        ),
    );

    public static function validate($factory = null)
    {
        $val = Validation::forge($factory);

        $val->add_field('title', "Title", 'required');
        $val->add_field('post', "Post", 'required|min_length[25]');

        return $val;
    }

    public static function row_count()
    {
        $q = DB::query("SELECT COUNT(*) AS `count` FROM blog_posts")->execute();
        return (int) $q[0]['count'];
    }
}
