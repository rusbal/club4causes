<?php

class Model_Blog_Category extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'category'
	);

    protected static $_many_many = array(
        'posts' => array(
            'key_through_from' => 'category_id',
            'table_through' => 'blog_posts_categories',
            'key_through_to' => 'post_id',
            'model_to' => 'Model_Blog_Post',
            'cascade_save' => true,
        ),
    );

    public static function get_id_autosave($categories)
    {
        $row = self::find()->where('category', $categories)->get_one();

        if ($row)
            return $row->id;
        else
        {
            $ar = self::forge(array('category' => $categories));
            if ($ar->save())
                return $ar->id;
            else
                trigger_error("Model_Blog_Category::get_id_autosave('$categories') failed.", E_USER_ERROR);
        }
    }
}
