<?php
/**
 * points.php
 */

class Points
{
    public static function add_points($key, $user_id = null)
    {
        if (is_null($user_id))
        {
            return false;
        }

        if ( ! ($value = Session::get("settings.$key")))
        {
            return false;
        }

        if ($user = Model_User::find($user_id))
        {
            if ($key === 'each_login')
            {
                foreach ($user->member->points as $instances)
                {
                    if ($instances->source === 'each_login')
                        if ( date('Y-m-d', $instances->created_at) === date('Y-m-d', time() ))
                            return false; // once each day only
                }
            }

            $user->member->points[] = new \Model_Member_Points(array(
                'points' => $value,
                'source' => $key,
            ));
            $user->member->running_points += $value;

            return $user->save();
        }

        return false;
    }
}
