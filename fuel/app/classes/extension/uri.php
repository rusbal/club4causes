<?php
/*
Using language file

We are at the end of the tutorial: now our application is able to understand the language selected by the user, so its time to create an application that can be multilanguage.
This can be done using the Lang class of the Fuel core, using two simple methods:
load() can get all the entries of a language file and put it in a variable. An example is:
    Lang::load('example', 'test');

This will load the fuel/app/lang/{lang}/example.php file in the "test" variable.
get() can get the translated value associated to the current key:   
    Lang::get('test.something');
    
It will take the "something" key from the "test" variable, the same we loaded with the load() method.
Now we are able to get the current language file, get the variable... What else?
 
Create language file

    We are at the end of the tutorial: the creation of a language file.
    Let's create a file called fuel/app/lang/{lang}/example.php : {lang} is the language, example.php is the file, but the name should be whatever you want. I suggest to create a file for every section of the site.

    Now its time to create it:

        return array(
            'hello' => 'Hello',
            'something' => 'brave new :name!',
            'test' => array('key1' => 'variable1')
        );

    And these are examples to how use it:

        echo Lang::get('test.hello');
        // This will print "Hello"
         
        echo Lang::get('test.something', array('name' => 'world'));
        // This will print "brave new world"
         
        echo Lang::get('test.test.key'));
        // This will print "variable1"
*/
 
class Uri extends Fuel\Core\Uri
{
    public function __construct($uri = NULL)
    {
        parent::__construct($uri);
        $this->detect_language();
    }
 
    public function detect_language()
    {
        if ( ! count($this->segments))
        {
            return false;
        }
 
        $first = $this->segments[0];
        $locales = Config::get('locales');
 
        if(array_key_exists($first, $locales))
        {
            array_shift($this->segments);
            $this->uri = implode('/', $this->segments);
 
            Config::set('language', $first);
            Config::set('locale', $locales[$first]);
        }
    }

    /**
     * Now we can create an url using the syntax Uri::generate('mylink')
     */
    public static function generate($uri = null, $variables = array(), $get_variables = array(), $secure = null) 
    {
        $language = Config::get('language');
     
        if ( !empty($uri))
        {
            $language .= '/';
        }
             
        return \Uri::create($language.$uri, $variables, $get_variables, $secure);
    }
}
