<?php

class Controller_Challenges_Polls extends Controller_Base
{
    public function action_index()
    {
        $this->template->title = "Programmer needs further direction";

        $this->reddo('content/message.twig', array(
            'story_one' => "Nothing's here yet, what to show here?",
            'story_two' => null,
            'link_uri' => '/',
            'link_text' => 'View home page',
        ));
    }
}
