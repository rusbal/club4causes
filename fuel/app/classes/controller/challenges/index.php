<?php

class Controller_Challenges_Index extends Controller_Base
{
    public function action_index()
    {
        $this->reddo('content/challenges/old.twig', array(
            'newest_members' => Model_Member::find()->order_by('id', 'desc')->limit(32)->get(),
            'show_home_btn_on_right' => true,
        ));
    }
}
