<?php
/**
 * Front-end index page
 */
class Controller_Home extends Controller_Base
{
	public function action_index()
	{
        $this->template->show_header_content = true;

        $top_nprofit = Model_Nonprofit::find()->order_by('running_points', 'desc')->limit(50)->get();
        $top_members = Model_Member::find()->order_by('running_points', 'desc')->limit(15)->get();

        $this->template->scripts[] = 'easytabs.js';

        View::set_global('selected_celebrities', Model_Celebrity::find()->where('selected', true)->get());

		$this->reddo('content/index.twig', array(
            'top_nprofit' => $top_nprofit,
            'top_members' => $top_members,
            'newest_members' => Model_Member::find()->order_by('id', 'desc')->limit(10)->get(),
            // 'birthday_users' => Model_User::generate_birthday_celebrants(),
            'birthday_users' => Model_User::find('all', array('where' => array('created_at' => array('>' => 0)))),
            'comments' => Model_Topic::get_latest_comments(),
            'pet' => Model_Pet::get_selected(),
            'missing' => Model_Missing::get_selected(),
            'random_us_state' => Model_Static::get_random_us_state(),
		));
	}
}
