<?php

class Controller_Nonprofit_Index extends Controller_Base
{
    /**
     * Preview all posts
     */
    public function action_index($offset = 1, $limit = 4)
    {
        $db_offset = ($offset - 1) * $limit;
        
        $posts = Model_Blog_Post::find('all', array(
            'where' => array('user_id' => $this->current_user->id), 
            'order_by' => array('id' => 'desc'),
            'offset' => $db_offset,
            'limit' => $limit));

        if ( ! $posts && is_null($this->current_user))
        {
            Session::set_flash('error', 'No blog entry, sign in to create one');
            Response::redirect('/');
        }

        if (($count = count($posts)) == 0) {
            Session::set_flash('okhide', 'No blog entry, create blog entry here');
            Response::redirect('/blog/create');
        }

        $this->template->styles[] = 'view/blog.css';

        $this->reddo('content/blog/preview.twig', array(
            'posts' => $posts,
            'pagination' => Helper::paginate(Model_Blog_Post::row_count(), $limit),
            'offset' => $offset,
        ), false);
    }
}
