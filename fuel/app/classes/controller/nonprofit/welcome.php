<?php

class Controller_Nonprofit_Welcome extends Controller_Base
{
    private function _send_activation_email($name, $nonprofit, $email, $username, $password)
    {
        $subject = $name . " account successfully activated on Club4Causes!";

        $message = "
        <p>Dear " . $name . ",</p>
        <p>" . $nonprofit . " will be receiving on going donations.</p>
        <p>Please see <a href='http://" . $_SERVER['HTTP_HOST'] . "'>\"HOW IT WORKS\"</a> and <a href='http://" . $_SERVER['HTTP_HOST'] . "/nonprofit/" . Input::post('nonprofit_id') . "'>YOUR PAGE</a>.
        <div style='margin-left:30px'>Username: " . $username . "</div>
        <div style='margin-left:30px'>Password: " . $password . "</div>
        <p>If you have any questions, please use the <a href='http://" . $_SERVER['HTTP_HOST'] . "/contact'>contact page</a> on our website.</p>
        <p>Thank you</p>
        <img src='http://dev.club4causes.com/assets/img/site_logo.png'>
        ";

        return Helper::send_email($subject, $message, $email, $name);
    }

    public function action_index($id = null, $hash = null)
    {
        if (is_null($id) || is_null($hash) || ! ($nonprofit = Model_Nonprofit::find($id)) ) return $this->action_404();

        if ($nonprofit->status != $hash)
        {
            $ok = true;
            $this->render_message("The link you use is no longer valid", "Please check latest email from ".self::APP_TITLE . ".");
        }
        else
        {
            $ok = false;

            if (Input::method() == "POST")
            {
                $val = Model_Nonprofit::validate_name('name');

                if ($ok = $val->run())
                {
                    $user = current($nonprofit->users);
                    $user_id = $user->id;

                    $nonprofit->users[$user_id]->alias = $val->validated('username');
                    $nonprofit->users[$user_id]->username = $val->validated('username');
                    $nonprofit->users[$user_id]->first_name = $val->validated('first_name');
                    $nonprofit->users[$user_id]->last_name = $val->validated('last_name');

                    $nonprofit->status = "1"; // Activated

                    if ($ok = $nonprofit->save())
                    {
                        $this->_send_activation_email(
                            Input::post('first_name').' '.Input::post('last_name'), $nonprofit->name, 
                            $user->email, Input::post('username'), Input::post('password')
                        );
                        $this->render_message($nonprofit->name." will now start to receive on going donations", "Watch how it works for you on the homepage.");
                    }
                    else
                        $this->template->flash_error = "Failed attempt to save nonprofit data".self::THIS_SHOULD_NOT_HAPPEN;
                }
                
                if ( ! $ok)
                {
                    $this->errmsg['first_name'] = $val->error('first_name');
                    $this->errmsg['last_name'] = $val->error('last_name');

                    $this->value['first_name'] = Input::post('first_name');
                    $this->value['last_name'] = Input::post('last_name');
                }
            }
        }

        if ( ! $ok)
            $this->reddo('content/nonprofit/welcome.twig', array(
                'nonprofit' => $nonprofit,
                'selecting' => Model_Member_Nonprofit_Selections::get_supporter_of_nonprofit($id),
            ));
    }
}
