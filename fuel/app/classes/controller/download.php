<?php

class Controller_Download extends Controller_Base
{
    public function action_index($img_code = null)
    {
        if ($img_code == 'SEE_ME_ON_SMALL')
        {
            $img_filename = 'see-me-on-small.jpg';
        }

        if ($img_filename)
        {
            header('Content-disposition: attachment; filename='.$img_filename);
            header('Content-type: image/jpg');
        }
    }
}
