<?php

class Controller_Blog extends Controller_Baseblog
{
    /**
     * Preview all posts
     */
    public function action_index($offset = 1, $limit = 4)
    {
        $db_offset = ($offset - 1) * $limit;
        $posts = Model_Blog_Post::find()->order_by('id', 'desc')->offset($db_offset)->limit($limit)->get();

        if ( ! $posts && is_null($this->current_user))
        {
            Session::set_flash('error', 'No blog entry, sign in to create one');
            Response::redirect('/');
        }

        if (($count = count($posts)) == 0) {
            Session::set_flash('okhide', 'No blog entry, create blog entry here');
            Response::redirect('/blog/create');
        }

        $this->reddo('content/blog/preview.twig', array(
            'posts' => $posts,
            'pagination' => Helper::paginate(Model_Blog_Post::row_count(), $limit),
            'offset' => $offset,
        ), false);
    }

    /**
     * Preview of posts with category
     */
    public function action_category($category_identifier = null, $offset = 1, $limit = 4)
    {
        if ( ! $category_identifier)
            return $this->action_404();

        $db_offset = ($offset - 1) * $limit;

        if (is_numeric($category_identifier))
            $category = Model_Blog_Category::find($category_identifier);
        else
            $category = Model_Blog_Category::find_by_category($category_identifier);

        if ( ! $category)
            return $this->action_404();

        $count = count($category->posts);
        $posts = array();
        $x = 0;
        
        foreach ($category->posts as $post)
        {
            if (($db_offset - 1) <= $x && $x < $limit)
                $posts[] = $post;
            $x += 1;
        }

        if ( ! $posts)
            return $this->action_404();

        $this->reddo('content/blog/preview.twig', array(
            'posts' => $posts,
            'pagination' => Helper::paginate($count, $limit),
            'url_path' => 'category/'.$category_identifier.'/',
            'offset' => $offset,
        ), false);
    }

    /**
     * View one post, comment entry
     */
    public function action_view($post_identifier)
    {
        if (is_numeric($post_identifier)) {
            $post = Model_Blog_Post::find($post_identifier);
        } 

        /**
         * Saving of comment
         */
        if (Input::method() == "POST")
        {
            Helper::exit_on_honeypot_captcha();
            
            $val_comment = Model_Blog_Comment::validate('comment');

            if ($val_comment->run())
            {
                $comment = new Model_Blog_Comment(array(
                    'website' => Input::post('website'),
                    'comment' => Input::post('comment'),
                    'status' => 1,
                ));

                if ($this->current_user)
                {
                    $comment['user_id'] = $this->current_user->id;
                    $comment['name'] = $this->current_user->first_name . ' ' . $this->current_user->last_name;
                    $comment['email'] = $this->current_user->email;
                }
                else
                {
                    $comment['user_id'] = 0;
                    $comment['name'] = Input::post('name');
                    $comment['email'] = Input::post('email');
                }

                $post->comments[] = $comment;

                if ($post->save()) {
                    $this->template->flash_success = "Comment entry acknowledged";
                } else {
                    $this->template->flash_error = "Failed attempt to save comment".self::THIS_SHOULD_NOT_HAPPEN;
                }
            }
            else
            {
                $this->value['name'] = Input::post('name');
                $this->value['email'] = Input::post('email');
                $this->value['website'] = Input::post('website');
                $this->value['comment'] = Input::post('comment');

                $this->errmsg['name'] = $val_comment->error('name');
                $this->errmsg['email'] = $val_comment->error('email');
                $this->errmsg['comment'] = $val_comment->error('comment');
            }
        }
        else
        {
            if ($this->current_user)
            {
                $this->value['name'] = $this->current_user->first_name . ' ' . $this->current_user->last_name;
                $this->value['email'] = $this->current_user->email;
            }
        }

        $this->reddo('content/blog/view.twig', array(
            'post' => $post,
        ), false);
    }

    /**
     * Create a post
     */
    public function action_create()
    {
        $val_post = Model_Blog_Post::validate('post');

        if (Input::method() == "POST")
        {
            if ($val_post->run())
            {
                $post = new Model_Blog_Post();
                $post->user_id = Session::get('user_id');
                $post->title = Input::post('title');
                $post->post = Helper::tidy_html(Input::post('post'));

                if (Input::post('category'))
                {
                    $categories = explode(',', Input::post('category'));

                    foreach ($categories as $category)
                    {
                        $category = trim($category);

                        if ($cat = Model_Blog_Category::find_by_category($category)) {
                            $post->categories[] = $cat;
                        } else {
                            $post->categories[] = Model_Blog_Category::forge(array('category' => $category));
                        }
                    }
                }

                if ($post->save())
                {
                    Session::set_flash('okhide', 'Blog post was successfully saved');
                    Response::redirect('/blog/view/'.$post->id);
                }

                $this->template->flash_error = "Failed attempt to save blog post".self::THIS_SHOULD_NOT_HAPPEN;
            }
            else
            {
                $this->errmsg['title'] = $val_post->error('title');
                $this->errmsg['post'] = $val_post->error('post');
            }
        }

        $this->template->cdn_scripts[] = '/assets/vendor/tiny_mce/jquery.tinymce.js';
        $this->template->title = "New Blog Entry";
        $this->reddo('content/blog/post-form.twig');
    }

    /**
     * Update a post
     */
    public function action_update($post_identifier)
    {
        if (is_numeric($post_identifier)) {
            $post = Model_Blog_Post::find($post_identifier);
        }

        if ( ! $post)
            return $this->action_404();

        $val_post = Model_Blog_Post::validate('post');

        if (Input::method() == "POST")
        {
            if ($val_post->run())
            {
                $post->title = Input::post('title');
                $post->post = Helper::tidy_html(Input::post('post'));

                /**
                 * Update categories
                 */
                $input_categories = Helper::str_to_array(Input::post('category'), ',');

                foreach ($post->categories as $category) {
                    if (in_array($category->category, $input_categories)) {
                        // Remove from input list, already on database
                        $input_categories = array_diff($input_categories, array($category->category));
                    } else {
                        // Deleted category, remove relationship from database
                        unset($post->categories[$category->id]);
                    }
                }

                /**
                 * Remove unused categories
                 */
                $categories = Model_Blog_Category::find('all');
                foreach ($categories as $category)
                {
                    if (count($category->posts) == 0)
                        $category->delete();
                }

                // Added categories
                foreach ($input_categories as $category) {
                    if ($cat = Model_Blog_Category::find_by_category($category)) {
                        $post->categories[] = $cat;
                    } else {
                        $post->categories[] = Model_Blog_Category::forge(array('category' => trim($category)));
                    }
                }

                if ($post->save())
                {
                    Session::set_flash('okhide', 'Blog post was successfully updated');
                    Response::redirect('/blog/view/'.$post->id);
                }

                $this->template->flash_error = "Failed attempt to update blog post".self::THIS_SHOULD_NOT_HAPPEN;
            }
            else
            {
                $this->value['title'] = Input::post('title');
                $this->value['post'] = Input::post('post');
                $this->value['category'] = Input::post('category');

                $this->errmsg['title'] = $val_post->error('title');
                $this->errmsg['post'] = $val_post->error('post');
            }
        }
        else
        {
            $this->value['title'] = $post->title;
            $this->value['post'] = $post->post;
            $categories_csv = "";

            foreach ($post->categories as $c)
                $categories_csv = $categories_csv . $c->category . ", ";

            $this->value['category'] = rtrim($categories_csv, ", ");
        }

        $this->template->cdn_scripts[] = '/assets/vendor/tiny_mce/jquery.tinymce.js';
        $this->template->title = "Update Blog Entry";
        $this->reddo('content/blog/post-form.twig');
    }
}
