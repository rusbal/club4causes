<?php

class Controller_Shopping extends Controller_Base
{
    public function action_index()
    {
        $this->template->styles[] = 'jquery.my-slider.css';
        $this->template->scripts[] = 'jquery.my-slider.js';

        $this->reddo('content/shopping.twig', array(
            'shopslides' => Model_Shopslide::find('all', array('where' => array('is_active' => 1), 'order_by' => array('updated_at' => 'desc'))),
            'coupons' => Model_Coupon::find()->get(),
        ), false);
    }
        
}
