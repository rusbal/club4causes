<?php

class Controller_Faq extends Controller_Base
{
    public function action_index()
    {
        $this->reddo('content/faq.twig', array(
            'faqs' => Model_Faq::find('all', array('order_by' => array('priority' => 'asc'), )),
        ), false);
    }
}
