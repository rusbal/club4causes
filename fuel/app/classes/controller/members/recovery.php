<?php

class Controller_Members_Recovery extends Controller_Base
{
    private function _send_change_password_email($user_name, $user_email, $hash)
    {
        $subject = "Password Reset Requested, " . self::APP_NAME;

        $link = "http://" . $_SERVER['HTTP_HOST'] . "/members/recovery/reset/" . base64_encode($hash);

        $message = "<em>Dear $user_name,</em>
        <p>You, or someone posing as you, requested to reset your password.  Follow the link below to change your password.</p>
        <p><a href='$link'>$link</a></p>
        <p>This URL is valid only for one day.</p>
        <p>Regards,</p>
        <p>" . self::APP_NAME . "</p>";

        Helper::send_email($subject, $message, $user_email, $user_name);
    }

    private function _send_new_password_email($user_name, $user_email, $password)
    {
        $subject = "Password Updated, " . self::APP_NAME;

        $message = "<em>Dear $user_name,</em>
        <p>You have successfully updated your password [ $password ].</p>
        <p>Regards,</p>
        <p>" . self::APP_NAME . "</p>";

        Helper::send_email($subject, $message, $user_email, $user_name);
    }

    public function action_reset($base64_hash = null)
    {
        if (is_null($base64_hash)) return $this->action_404();

        $hash = base64_decode($base64_hash);

        if ($request = Model_User_No_Id_Request::find_by_hash($hash))
        {
            /**
             * Check if the link is not more than a day old
             */
            if ($request->created_at + 86400 < $this->timestamp)
            {
                $this->render_message("The link you used is no longer valid",
                    "Please request password reset again."
                );
                return;
            }

            if (Input::method() == 'POST')
            {
                $request->user->password = Auth::instance()->hash_password(Input::post('password'));

                if ($request->save())
                {
                    Model_User_No_Id_Request::delete_for_user($request->user->id);
                    $this->_send_new_password_email($request->user->first_name . ' ' . $request->user->last_name, $request->user->email, Input::post('password'));

                    $this->render_message("Password Changed",
                        "You have successfully updated your password!"
                    );
                    return;
                }

                $this->template->flash_error = "Failed attempt to update your password".self::THIS_SHOULD_NOT_HAPPEN;
            }
        }
        else
        {
            $this->render_message("The link you used is no longer valid",
                "Please request password reset again."
            );
            return;
        }

        $this->reddo('content/recovery-new-password.twig');
    }

    public function action_index()
    {
        if (Input::method() == 'POST')
        {
            $user = false;

            if ($email = filter_var( Input::post('username'), FILTER_VALIDATE_EMAIL ))
            {
                if ( ! ($user = Model_User::find_by_email( Input::post('username') )))
                    $this->errmsg['username'] = "No account found with that email address.";
            }
            else
            {
                if ( ! ($user = Model_User::find_by_username( Input::post('username') )))
                    $this->errmsg['username'] = "No account found with that username.";
            }

            if ($user)
            {
                $hash = Crypt::encode($user->email . $this->timestamp, $this->timestamp);

                Model_User_No_Id_Request::delete_for_user($user->id);
                $user->no_id_requests = Model_User_No_Id_Request::forge(array('hash' => $hash));

                if ($user->save())
                {
                    $this->_send_change_password_email($user->first_name . ' ' . $user->last_name, $user->email, $hash);
                    $this->render_message("Password reset request has been processed...",
                        "Please check your email to continue resetting your password."
                    );
                    return;
                }
                else
                    $this->template->flash_error = "Failed attempt to process your request for password reset".self::THIS_SHOULD_NOT_HAPPEN;
            }
            else
                $this->value['username'] = Input::post('username');
        }

        $this->reddo('content/recovery.twig');
    }
}
