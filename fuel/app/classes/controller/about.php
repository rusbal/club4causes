<?php
/**
 * Shows Contributors
 */
class Controller_About extends Controller_Base
{
    public function action_index()
    {
        $this->reddo('content/about.twig', array(
            'contributors' => Model_Contributor::find()->order_by('name', 'asc')->get(),
        ));
    }
}
