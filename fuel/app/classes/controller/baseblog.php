<?php

class Controller_Baseblog extends Controller_Base
{
    public function reddo($file = null, $data = array(), $auto_filter = null)
    {
        $this->template->styles[] = 'view/blog.css';

        $data = array_merge($data, array(
            'comments' => Model_Blog_Comment::find()->order_by('id', 'desc')->limit(25)->get(),
            'categories' => Model_Blog_Category::find('all'),
        ));

        parent::reddo($file, $data, $auto_filter);
    }
}

// eof
