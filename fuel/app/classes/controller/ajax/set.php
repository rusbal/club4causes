<?php

class Controller_Ajax_Set extends Controller_Base
{

     public function action_choose()
    {
        $success = false;
        $message = null;

        $id = Input::post('id');
        $action = Input::post('action');

        if($id && $user = Model_Member::find(Session::get('user_id'))){
            if($action == 'delete'){
                unset($user->celebrities[$id]);
                if ($user->save()) {
                    $success = true;
                }   
            }
            else if ($action == 'add') {
                if ($celeb = Model_Celebrity::find($id)){
                    $user->celebrities[] = $celeb;
                    if($user->save()){
                        $success = true;
                    }
                }
            }
        }

        return json_encode(array('success' => $success, 'message' => $message));
    }
}

// eof
