<?php

class Controller_Ajax_Post extends Controller_Base
{
    public function action_alert()
    {
        $success = false;
        $message = null;

        if ( ! $_POST['nonprofit']) {
            return json_encode(array('success' => $success, 'message' => $message));
        }

        $alert_message = $_POST['nonprofit'] . ' fellow supporter, ' . $this->current_user->first_name . ' ' . $this->current_user->last_name . ', posted a message';

        /**
         * Save alert message
         */
        $alert = Model_Alert::forge(array(
            'message' => $alert_message,
            'permalink' => ($_POST['permalink'] ? $_POST['permalink'] : ''),
        ));
        if ( ! $alert->save()) {
            return json_encode(array('success' => $success, 'message' => 'Failure saving alert message'));
        }

        $np = Model_Nonprofit::find_by_name($_POST['nonprofit']);

        foreach ($np->members as $member) {
            /**
             * Save user alerts
             */
            $member->user->received_alerts[] = Model_User_Alert::forge(array(
                'from_id' => $this->current_user->id,
                'alert_id' => $alert->id,
            ));
            $member->user->save();
        }
        
        header('Content-Type: application/json');
        return json_encode(array('success' => $success, 'message' => $message));
    }

    public function action_forum()
    {
        $user_id = Session::get('user_id');

        $sql = "SELECT f.forum_name, f.last_poster, u.username, u.id FROM zf_forums as f, users as u where f.last_poster = u.username
                AND u.id = $user_id";

        $query = DB::query($sql)->execute();

        foreach ($query as $row)
            $arr[] = $row['forum_name'];

        return $arr;
    }
}

// eof
