<?php

class Controller_Ajax_Get extends Controller_Base
{

     public function action_choose()
    {
        $success = false;
        $message = null;

        $id = Input::post('id');
        $action = Input::post('action');

        if($id){
            if($action == 'delete'){
                if ($ar = Model_Member::find(Session::get('user_id'))->celebrities) {
                    if ($ar->delete()) {
                        $success = true;
                    }
                }   
            }
            else if ($action == 'add') {
                if ($ar = Model_Celebrity::find($id)){
                    $user = Model_User::forge();
                    $user->member->celebrities[] = $c;
                    if($user->save()){
                        $success = true;
                    }
                }
            }
        }

        return json_encode(array('success' => $success, 'message' => $message));
    }

    public function action_celebrity_id_picture()
    {
        $success = false;
        $data = null;

        if ($full_name = Input::get('full_name'))
        {
            $name_arr = explode(' ', $full_name, 2);

            $query = Model_Celebrity::query()->where('first_name', $name_arr[0]);

            if (isset($name_arr[1]))
                $query->where('last_name', $name_arr[1]);

            if ($ar = $query->get(1))
            {
                $success = true;

                $id = key($ar);
                $value = current($ar);
                $data = array('id' => $id, 'image_filename' => self::CELEBRITY_IMAGE_PATH.$value->image_filename);
            }
        }

        header('Content-Type: application/json');
        return json_encode(array('success' => $success, 'data' => $data));
    }

    public function action_celebrity_formatted_box($id = null)
    {
        if (is_null($id)) return false;

        $celebrity = Model_Celebrity::find($id);

        if ( ! $celebrity) return false;

        return View::forge('content/celebrity-featured.twig', array(
            'random_celebrities' => array($celebrity),
        ), false);
    }
}

// eof
