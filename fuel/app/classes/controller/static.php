<?php

class Controller_Static extends Controller_Base
{
    public function action_privacy()
    {
        $this->reddo('content/static.twig', array(
            'title' => "Privacy Policy",
            'string' => file_get_contents(DOCROOT.DS."web_data/static/privacy_policy.html"),
        ), false);
    }

    public function action_tos()
    {
        $this->reddo('content/static.twig', array(
            'title' => "Terms of Service",
            'string' => file_get_contents(DOCROOT.DS."web_data/static/terms_of_service.html"),
        ), false);
    }
}
