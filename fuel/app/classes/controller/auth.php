<?php

class Controller_Auth extends Controller_Base
{
    private static function _sign_in_user($user, $remember_me = false, $timestamp = null)
    {
        if (is_null($timestamp))
            $timestamp = time();
        
        if ($remember_me)
        {
            $user->remember_me = $user->id.'~'.sha1(Config::get('simpleauth.login_hash_salt').$timestamp.$user->login_hash);
            $user->save();
            Cookie::set('remember_me', $user->remember_me, 86400 * 14); // 14 days cookie life
        }

        Session::set('user_id', $user->id);
        Session::set('user_group', $user->group);

        if ($user->member) {
            Session::set('member_id', $user->member->id);

            /**
             * Adding of points if necessary
             */
            Points::add_points('each_login', $user->id);
        }

        return $user;
    }

    public static function check_with_remembering()
    {
        if (Auth::check())
            return true;

        if ( ! ($remember_me = Cookie::get('remember_me')))
            return false;
    
        list($user_id, $remember_hash) = explode('~', $remember_me, 2);

        if ($user = Model_User::find($user_id, array('where' => array('remember_me' => $remember_me))))
        {
            /**
             * Valid user, set to signed in
             */
            Auth::force_login($user->id);
            self::_sign_in_user($user, true);

            return 'Welcome back, '.$user->first_name.' '.$user->last_name.'!';
        }
        else
        {
            /**
             * Delete this outdated cookie
             */
            Cookie::delete('remember_me');
        }

        return false;
    }

    public function action_signin($user_type = "member")
    {
        if (Auth::check())
        {
            // Session::set_flash('success', 'Redirected page from login to homepage because you are already signed in!');
            // Response::redirect('/');
            Response::redirect('/my/homepage');
        }

        $this->require_user('_ANYBODY_');

        if (Input::method() == 'POST')
        {
            $result = Auth::login(Input::post('username'), Input::post('password'));

            if ($result === true)
            {
                list(, $user_id) = Auth::get_user_id();
                $user = Model_User::find($user_id);

                $this->_sign_in_user($user, Input::post('remember_me'), $this->timestamp);

                Session::set_flash('okhide', 'Greetings '.$user->first_name.' '.$user->last_name.', you have signed in successfully.');

                $page = '/';

                if ($request_uri = Session::get('request_uri'))
                {
                    Session::delete('request_uri');
                    $page = $request_uri;
                }
                
                /**
                 * Authentication successful, redirect user to to appropriate page
                 */
                $pra = Session::get('page_requesting_auth');

                if ($pra)
                {
                    Session::delete('page_requesting_auth');
                    Response::redirect($pra);
                }
                else
                {

                    if ("admin" == $user_type)
                    {
                        Response::redirect('admin/dashboard');
                    }
                    else if ($user->group == self::GROUP_NONPROFIT)
                    {
                        Response::redirect('nonprofit/index/' . $user->id);
                    }
                    else
                    {
                        Response::redirect('my/homepage');
                    }
                }
            }
            else if ($result === false)
            {
                Session::set_flash('error', 'Invalid login, please try again.');
            }
            else if ($result === 'USER_NOT_ACTIVATED')
            {
                Session::set_flash('error', 'You have yet to activate your account. We have just sent you an email with activation link in case you missed the first one.');
                Response::redirect('/');
            }
        }

        if ("admin" == $user_type)
            Response::redirect('admin/signin');
        else
            Response::redirect('members/signin');
    }

    public function action_signout()
    {
        if (Auth::check())
        {
            Auth::logout();
            Session::delete('user_id');
            Session::delete('member_id');
            Cookie::delete('remember_me');
            Session::set_flash('okhide', 'Bye '.$this->current_user->first_name.', you have signed out.');

            // unset cookies
            if (isset($_SERVER['HTTP_COOKIE'])) {
                $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
                foreach($cookies as $cookie) {
                    $parts = explode('=', $cookie);
                    $name = trim($parts[0]);
                    setcookie($name, '', time()-1000);
                    setcookie($name, '', time()-1000, '/');
                }
            }
        }
        
        Response::redirect('/');
    }
}
