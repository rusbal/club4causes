<?php

class Controller_Admin_Shopslide extends Controller_Admin_Base
{
    public function action_index()
    {
        $this->reddo('admin/shopslide_list.twig', array(
            'shopslides' => Model_Shopslide::find('all', array('order_by' => array('is_active' => 'desc', 'updated_at' => 'desc'))),
        ), false);
    }

    public function action_create()
    {
        if (Input::method() == "POST")
        {
            $val_shopslide = Model_Shopslide::validate('shopslide');

            if (true || $val_shopslide->run())
            {
                list($image_filename, $str_error) = Helper::upload_file('static/images', true);

                $shopslide = new Model_Shopslide();
                $shopslide->string = Input::post('string');
                $shopslide->merchant_url = Input::post('merchant_url');

                if (is_null($str_error))
                    $shopslide->image_filename = $image_filename;

                if ($shopslide->save())
                {
                    Session::set_flash('success', "Shopslide was successfully saved!");
                    Response::redirect('admin/shopslide');
                }

                $this->template->flash_error = "Failed attempt to save shopslide".self::THIS_SHOULD_NOT_HAPPEN;
            }
            else
            {
                /**
                 * Sticky
                 */
                $this->value['string'] = Input::post('string');
                // $this->value['merchant_url'] = Input::post('merchant_url');

                /**
                 * Validation error messages
                 */
                $this->errmsg['string'] = $val_shopslide->error('string');
                // $this->errmsg['merchant_url'] = $val_shopslide->error('merchant_url');
            }
        }

        $this->reddo('admin/shopslide_create.twig');
    }

    public function action_coupon()
    {
        if (Input::method() == "POST")
        {
            $val_coupon = Model_Coupon::validate('coupon');

            if (true || $val_coupon->run())
            {
                list($image_filename, $str_error) = Helper::upload_file('coupon', true);

                $coupon = new Model_Coupon();
                $coupon->business_name = Input::post('business_name');
                $coupon->coupon = Input::post('coupon');
                $coupon->business_url = Input::post('business_url');
                $coupon->details = Input::post('details');
                $coupon->donation = 2;
                $coupon->image_filename = $image_filename;

                if ($coupon->save())
                {
                    Session::set_flash('success', "Coupon was successfully saved!");
                    Response::redirect('admin/shopslide/coupon');
                }

                $this->template->flash_error = "Failed attempt to save shopslide".self::THIS_SHOULD_NOT_HAPPEN;
            }
            else
            {
                /**
                 * Sticky
                 */
                $this->value['business_name'] = Input::post('business_name');
                $this->value['coupon'] = Input::post('coupon');
                $this->value['business_url'] = Input::post('business_url');
                $this->value['details'] = Input::post('details');

                /**
                 * Validation error messages
                 */
                $this->errmsg['string'] = $val_coupon->error('string');
                $this->errmsg['coupon'] = $val_coupon->error('coupon');
                $this->errmsg['business_url'] = $val_coupon->error('business_url');
                $this->errmsg['details'] = $val_coupon->error('details');
                // $this->errmsg['merchant_url'] = $val_shopslide->error('merchant_url');
            }
        }

        $this->reddo('admin/coupon-create.twig');
    }
}
