<?php

class Controller_Admin_Missings extends Controller_Admin_Base
{
    public function action_index()
    {
        $this->reddo('admin/missings_list.twig', array(
            'missings' => Model_Missing::find()->get(),
            'selected_id' => Model_Missing::get_selected_id(),
        ));
    }

    public function action_create()
    {
        if (Input::method() == "POST")
        {
            $val_missing = Model_Missing::validate('missings');

            if ($val_missing->run())
            {
                list($image_filename, $str_error) = Helper::upload_file('missing', true);

                $missing = new Model_Missing();
                $missing->name = Input::post('name');
                $missing->merchant_url = Input::post('merchant_url');

                if (is_null($str_error))
                    $missing->image_filename = $image_filename;

                /**
                 * Save assets
                 */
                if (Input::post('asset_video_url'))
                {
                    $asset = new Model_Asset();
                    $asset->type = "video";
                    $asset->location = Input::post('asset_video_url');

                    if ($asset->save())
                        $missing->assets[] = Model_Asset_Interface::forge(array('asset_id' => $asset->id));
                }

                if ($missing->save())
                {
                    Session::set_flash('success', "Missing child was successfully saved!");
                    Response::redirect('admin/missings');
                }

                $this->template->flash_error = "Failed attempt to save missing child".self::THIS_SHOULD_NOT_HAPPEN;
            }
            else
            {
                /**
                 * Sticky
                 */
                $this->value['name'] = Input::post('name');
                $this->value['merchant_url'] = Input::post('merchant_url');

                /**
                 * Validation error messages
                 */
                $this->errmsg['name'] = $val_missing->error('name');
                $this->errmsg['merchant_url'] = $val_missing->error('merchant_url');
            }
        }

        $this->reddo('admin/missings_create.twig');
    }
}
