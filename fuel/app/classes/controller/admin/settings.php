<?php

class Controller_Admin_Settings extends Controller_Admin_Base
{
    public function action_index()
    {
        if (Input::method() == "POST")
        {
            $count = 0;

            foreach (Input::post() as $key => $value) {
                /**
                 * somehow fuelphp sets . to _
                 */
                $key = str_replace('_participation', '.participation', $key); 
                $key = str_replace('_first', '.first', $key);
                $key = str_replace('_second', '.second', $key);
                $key = str_replace('_third', '.third', $key);

                $setting = Model_Setting::find_by_key($key);
                if ($setting) {
                    $setting->value = $value;
                    if ($setting->save()) {
                        $count += 1;
                    }
                }
            }
            
            if ($count > 0)
            {
                Session::set_flash('success', "$count settings was successfully saved!");
                Response::redirect('admin/dashboard');
            }

            $this->template->flash_error = "Failed attempt to save settings".self::THIS_SHOULD_NOT_HAPPEN;
        }

        $this->reddo('admin/settings.twig', array(
            'settings' => Model_Setting::find('all'),
        ));
    }
}
