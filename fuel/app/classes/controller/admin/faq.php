<?php

class Controller_Admin_Faq extends Controller_Admin_Base
{
    public function action_index()
    {
        if (Input::method() === "POST")
        {
            $val_faq = Model_Faq::validate('faq');

            if ($val_faq->run())
            {
                $faq = Model_Faq::find(Input::post('faqs_id'));

                if ($faq) {

                    $faq->question = Input::post('question');
                    $faq->answer = Input::post('answer');
                    $faq->priority = Input::post('priority') ? Input::post('priority') : 0;

                    if ($faq->save()) {
                        $this->template->flash_success = "A FAQ update was successful saved!";
                    } else {
                        $this->template->flash_error = "Failed attempt to update an FAQ".self::THIS_SHOULD_NOT_HAPPEN;
                    }
                } else {
                    $this->template->flash_error = "The FAQ no longer exists on database";
                }

            } else {
                $this->template->flash_error = $val_faq->error('question') . "\n" . $val_faq->error('answer');
            }
        }

        $this->reddo('admin/faq_list.twig', array(
            'faqs' => Model_Faq::find()->get(),
        ));
    }

    public function action_create()
    {
        if (Input::method() == "POST")
        {
            $val_faq = Model_Faq::validate('faq');

            if ($val_faq->run())
            {
                $faq = new Model_Faq();
                $faq->question = Input::post('question');
                $faq->answer = Input::post('answer');
                $faq->priority = Input::post('priority');

                if ($faq->save())
                {
                    Session::set_flash('success', "FAQ was successfully saved!");
                    Response::redirect('admin/faq');
                }

                $this->template->flash_error = "Failed attempt to save FAQ".self::THIS_SHOULD_NOT_HAPPEN;
            }
            else
            {
                $this->value['question'] = Input::post('question');
                $this->value['answer'] = Input::post('answer');
                $this->value['priority'] = Input::post('priority');

                $this->errmsg['question'] = $val_faq->error('question');
                $this->errmsg['answer'] = $val_faq->error('answer');
                $this->errmsg['priority'] = $val_faq->error('priority');
            }
        }

        $this->reddo('admin/faq_create.twig');
    }
}
