<?php

class Controller_Admin_Privacypolicy extends Controller_Admin_Base
{
    public function action_index()
    {
        if (Input::method() == "POST")
        {
            if (strlen(Input::post('policy')) > 100)
            {
                if (Helper::save_text2file( Input::post('policy'), "privacy_policy.html" ))
                {
                    $this->template->flash_success = "Privacy Policy was successfully saved!";
                }
                else
                {
                    $this->template->flash_error = "Failed attempt to save Privacy Policy".self::THIS_SHOULD_NOT_HAPPEN;
                }
            }
            else
            {
                $this->value['policy'] = Input::post('policy');
                $this->errmsg['policy'] = "The field Privacy Policy has to contain at least 100 characters.";
            }
        } else {
            if (file_exists(DOCROOT.DS."web_data/static/privacy_policy.html")) {
                $this->value['policy'] = file_get_contents(DOCROOT.DS."web_data/static/privacy_policy.html");
            }
        }

        $this->reddo('admin/privacy_policy.twig');
    }
}
