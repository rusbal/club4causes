<?php

class Controller_Admin_Ajax_Set extends Controller_Admin_Base
{
    public function action_pet()
    {
        $success = false;
        $message = null;

        $id = Input::post('id');

        if ($id)
        {
            if ($ar = Model_Pet::find($id))
            {
                if (is_null(Input::post('priority'))) {
                    $ar->priority = Input::post('priority');
                } else {
                    $ar->priority = time();
                }

                if ($ar->save())
                {
                     $success = true;
                     $message = 'ok';
                }
                   
            } else {
                $message = 'Not found';
            }
        }

        return json_encode(array('success' => $success, 'message' => $message));
    }

    public function action_shopslide()
    {
        $success = false;
        $message = null;

        $id = Input::post('id');

        if ($id)
        {
            $success = (DB::update('shopslides')->value('is_active', Input::post('is_active', 0))->where('id', $id)->execute() > 0);
            if ($success)
                $message = 'ok';
            else
                $message = 'Not found';
        }

        return json_encode(array('success' => $success, 'message' => $message));
    }
}

// eof
