<?php

class Controller_Admin_Ajax_Touch extends Controller_Admin_Base
{
    public function action_shopslide()
    {
        $success = false;
        $message = null;

        $id = Input::post('id');

        if ($id)
        {
            $success = (DB::update('shopslides')->value('updated_at', time())->where('id', $id)->execute() > 0);
        }

        return json_encode(array('success' => $success, 'message' => $message));
    }
}

// eof
