<?php

class Controller_Admin_Ajax_Delete extends Controller_Admin_Base
{
    public function action_celebrity()
    {
        $success = false;
        $message = null;

        $id = Input::post('id');

        if ($id && $ar = Model_Celebrity::find($id)) {
            if ($ar->delete()) {
                $success = true;
            }
        }

        return json_encode(array('success' => $success, 'message' => $message));
    }

    public function action_shopslide()
    {
        $success = false;
        $message = null;

        $id = Input::post('id');

        if ($id && $ar = Model_Shopslide::find($id)) {
            if ($ar->delete()) {
                $success = true;
            }
        }

        return json_encode(array('success' => $success, 'message' => $message));
    }

    public function action_faq()
    {
        $success = false;
        $message = null;

        $id = Input::post('id');

        if ($id && $ar = Model_Faq::find($id)) {
            if ($ar->delete()) {
                $success = true;
            }
        }

        return json_encode(array('success' => $success, 'message' => $message));
    }

    public function action_contributor()
    {
        $success = false;
        $message = null;

        $id = Input::post('id');

        if ($id && $ar = Model_Contributor::find($id)) {
            foreach ($ar->assets as $asset) {
                $asset->asset->delete();
                $asset->delete();
            }
            if ($ar->delete()) {
                $success = true;
            }
        }

        return json_encode(array('success' => $success, 'message' => $message));
    }
}

// eof
