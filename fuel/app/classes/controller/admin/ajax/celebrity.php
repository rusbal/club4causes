<?php

class Controller_Admin_Ajax_Celebrity extends Controller_Admin_Base
{

    public function action_select()
    {
        $success = false;
        $message = null;

        $id = Input::post('id');

        if ($id)
        {
            $ar = Model_Celebrity::find($id);
            $ar->selected = '1';
            if ($ar->save())
                $success = true;
        }

        return json_encode(array('success' => $success, 'message' => $message));
    }

    public function action_remove()
    {
        $success = false;
        $message = null;

        $id = Input::post('id');

        if ($id)
        {
            $ar = Model_Celebrity::find($id);
            $ar->selected = '0';
            if ($ar->save())
                $success = true;
        }

        return json_encode(array('success' => $success, 'message' => $message));
    } 
}

// eof
