<?php

class Controller_Admin_Ajax_Select extends Controller_Admin_Base
{

    public function action_celebrity()
    {
        $success = false;
        $message = null;

        $id = Input::post('id');

        if ($id)
        {
            $ar = Model_Celebrity::find($id);
            $ar->selected = '1';
            if ($ar->save())
                $success = true;
        }

        return json_encode(array('success' => $success, 'message' => $message));
    } 

}

// eof
