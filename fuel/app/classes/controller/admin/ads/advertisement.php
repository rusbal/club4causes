<?php

class Controller_Admin_Ads_Advertisement extends Controller_Admin_Base
{
    public function action_index($id = null)
    {
        if (is_null($id)) return $this->action_404();

        if (Input::method() == "POST")
        {
            list($image_filename, $upload_error) = Helper::upload_file('advertisement', false);

            $url_ids = array();
            $url_ids = array($url_ids, Model_Ad_Object::multiple_object_autosave(Input::post('target_url'), $id, $image_filename, Input::post('target_description')));
            
            /*foreach ($url_ids as $url_id)
                $ins = Model_Ad_Instance::forge(array(
                    'object_id' => $url_id,
                    'space_id' => Input::post('id'),
                ));*/
            foreach ($url_ids as $url_id)
                $ins = new Model_Ad_Instance();
                $ins->object_id = $url_id;
                $ins->space_id = Input::post('id');

            if ($ins->save())
            {
                Session::set_flash('okhide', 'advertisement was successfully saved!');
                Response::redirect('admin/ads/owner/view/');
            }
        }

        $this->template->ng_scripts[] = 'ng-multiple-autocomplete.js';

        $this->reddo('admin/ads/advertisement_form.twig' ,array(
            'owner' => Model_Ad_Owner::find($id),
            'spaces' => Model_Ad_Space::find()->get(),
        ));
    }
}
