<?php

class Controller_Admin_Ads_User extends Controller_Admin_Base
{
    public function action_index()
    {   
        if (Input::method() == "POST")
        {
            $user = new Model_User();
            $user->username = Input::post('username');
            $user->password = Auth::instance()->hash_password(Input::post('password'));
            $user->email = Input::post('email');
            $user->group = self::USER_MEMBER;
            $user->first_name = Input::post('firstname');
            $user->last_name = Input::post('lastname');
            $user->alias = Input::post('alias');
            $user->gender = Input::post('gender');

            if ($user->save())
            {
                Session::set_flash('okhide', 'New user was successfully saved');
            }
            else
                $this->template->flash_error = "Failed attempt to save message".self::THIS_SHOULD_NOT_HAPPEN;
        }

        $this->reddo('admin/ads/create_form.twig');
    }

}