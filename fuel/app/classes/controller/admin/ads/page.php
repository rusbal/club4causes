<?php

class Controller_Admin_Ads_Page extends Controller_Admin_Base
{
    public function action_index()
    {
        if (Input::method() == "POST")
        {
            $page_ids = array();

            if (Input::post('page') && Input::post('page_description'))
            {
                $page_ids = array($page_ids, Model_Ad_Page::multiple_page_autosave(Input::post('page'), Input::post('page_description'), 
                    Input::post('location_code'), Input::post('space_description') ));                
            }
        }
        else
        {
            $this->value['page'] =  Input::post('page');
            $this->value['page_description'] = Input::post('page_description');
            $this->value['space_description'] = Input::post('space_description');
        }

        $this->template->ng_scripts[] = 'ng-multiple-autocomplete.js';
        $this->reddo('admin/ads/page_form.twig');
    }
}
