<?php

class Controller_Admin_Ads_Owner extends Controller_Admin_Base
{
    public function action_index()
    {
        $val_owner = Model_Ad_Owner::validate('owner');
        $val_address = Model_Ad_Owner_Address::validate('address');

        if (Input::method() == "POST")
        {
            if ($val_owner->run() and $val_address->run())
            {
                $advertise = array(
                    'user_id' => Session::get('user_id'),
                    'business_name' => Input::post('business_name'),
                );

                $adver = new Model_Ad_Owner($advertise);

                $adver->address = new Model_Ad_Owner_Address(array(
                    'city' => Input::post('city'),
                    'state' => Input::post('state'),
                    'country' => Input::post('country'),
                ));

                if (list($image_filename, $upload_error) = Helper::upload_file('users', false))
                {
                    $own = Session::get('user_id');
                    $owner = Model_User::find($own);
                    $owner->image_filename = $image_filename;

                    $owner->save();
                }

                if ($adver->save())
                {
                    Session::set_flash('okhide', 'Owner was successfully saved!');
                    Response::redirect('/admin/ads/owner');
                }
                else
                    $this->template->flash_error = "Failed attempt to save message".self::THIS_SHOULD_NOT_HAPPEN;
            }
            else
            {
                $this->errmsg['business_name'] = $val_owner->error('business_name');
                $this->errmsg['city'] = $val_address->error('city');
                $this->errmsg['state'] = $val_address->error('state');
                $this->errmsg['country'] = $val_address->error('country');
            }
        }

        $this->reddo('admin/ads/owner_form.twig', array(
            'countries_with_grouping' => Model_Static::get_countries_with_grouping(),
        ));
    }

    public function action_view()
    {
        $this->reddo('admin/ads/owner.twig', array(
            'owners' => Model_Ad_Owner::find()->get(),
        ));
    }

}
