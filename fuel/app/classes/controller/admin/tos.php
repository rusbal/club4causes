<?php

class Controller_Admin_Tos extends Controller_Admin_Base
{
    public function action_index()
    {
        if (Input::method() == "POST")
        {
            if (strlen(Input::post('tos')) > 100)
            {
                if (Helper::save_text2file( Input::post('tos'), "terms_of_service.html" ))
                {
                    $this->template->flash_success = "Terms of Service was successfully saved!";
                }
                else
                {
                    $this->template->flash_error = "Failed attempt to save Terms of Service".self::THIS_SHOULD_NOT_HAPPEN;
                }
            }
            else
            {
                $this->value['tos'] = Input::post('tos');
                $this->errmsg['tos'] = "The field Terms of Service has to contain at least 100 characters.";
            }
        } else {
            if (file_exists(DOCROOT.DS."web_data/static/terms_of_service.html")) {
                $this->value['tos'] = file_get_contents(DOCROOT.DS."web_data/static/terms_of_service.html");
            }
        }

        $this->reddo('admin/terms_of_service.twig');
    }
}
