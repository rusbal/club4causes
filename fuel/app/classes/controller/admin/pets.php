<?php

class Controller_Admin_Pets extends Controller_Admin_Base
{
    public function action_index()
    {
        $this->reddo('admin/pets_list.twig', array(
            'pets' => Model_Pet::find()->get(),
            'selected_id' => Model_Pet::get_selected_id(),
        ));
    }

    public function action_create()
    {
        if (Input::method() == "POST")
        {
            $val_pet = Model_Pet::validate('pets');

            if ($val_pet->run())
            {
                list($image_filename, $str_error) = Helper::upload_file('pets', true);

                $pet = new Model_Pet();
                $pet->name = Input::post('name');
                $pet->merchant_url = Input::post('merchant_url');

                if (is_null($str_error))
                    $pet->image_filename = $image_filename;

                /**
                 * Save assets
                 */
                if (Input::post('asset_video_url'))
                {
                    $asset = new Model_Asset();
                    $asset->type = "video";
                    $asset->location = Input::post('asset_video_url');

                    if ($asset->save())
                        $pet->assets[] = Model_Asset_Interface::forge(array('asset_id' => $asset->id));
                }

                if ($pet->save())
                {
                    Session::set_flash('success', "Pet was successfully saved!");
                    Response::redirect('admin/pets');
                }

                $this->template->flash_error = "Failed attempt to save pet".self::THIS_SHOULD_NOT_HAPPEN;
            }
            else
            {
                /**
                 * Sticky
                 */
                $this->value['name'] = Input::post('name');
                $this->value['merchant_url'] = Input::post('merchant_url');

                /**
                 * Validation error messages
                 */
                $this->errmsg['name'] = $val_pet->error('name');
                $this->errmsg['merchant_url'] = $val_pet->error('merchant_url');
            }
        }

        $this->reddo('admin/pets_create.twig');
    }
}
