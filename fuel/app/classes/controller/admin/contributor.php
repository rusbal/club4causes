<?php

class Controller_Admin_Contributor extends Controller_Admin_Base
{
    public function action_index()
    {
        $this->reddo('admin/contributor_list.twig', array(
            'contributors' => Model_Contributor::find()->get(),
        ));
    }

    public function action_create()
    {
        $val_contributor = Model_Contributor::validate('contributor');

        if (Input::method() == "POST")
        {
            if ($val_contributor->run())
            {
                $contrib = new Model_Contributor();
                $contrib->name = Input::post('name');
                $contrib->organization_name = Input::post('organization_name');
                $contrib->organization_text = Input::post('about');

                $image_filenames = Helper::upload_multiple_files('contributor', false);

                if (is_array($image_filenames))
                {
                    if (isset($image_filenames[0]))
                        $contrib->image_filename = $image_filenames[0]['saved_as'];
                    else
                        $contrib->image_filename = "";

                    if (isset($image_filenames[1]))
                        $contrib->organization_logo = $image_filenames[1]['saved_as'];
                    else
                        $contrib->organization_logo = "";
                }

                if (Input::post('nonprofit'))
                {
                    foreach (Input::post('nonprofit') as $nonprofit)
                        if ($np = Model_Nonprofit::get_model_autosave($nonprofit))
                            $contrib->nonprofits[] = $np;
                }

                /**
                 * Save assets
                 */
                $asset_ids = array();
                if (Input::post('asset_website_url'))
                    $asset_ids = array_merge($asset_ids, Model_Asset::multiple_asset_autosave('link', Input::post('asset_website_url'), Input::post('asset_website_desc')) );
                if (Input::post('asset_video_url'))
                    $asset_ids = array_merge($asset_ids, Model_Asset::multiple_asset_autosave('video', Input::post('asset_video_url'), Input::post('asset_video_desc')) );
                if (Input::post('asset_picture_url'))
                    $asset_ids = array_merge($asset_ids, Model_Asset::multiple_asset_autosave('image', Input::post('asset_picture_url'), Input::post('asset_picture_desc')) );

                foreach ($asset_ids as $asset_id)
                    $contrib->assets[] = Model_Asset_Interface::forge(array('asset_id' => $asset_id));

                if ($contrib->save())
                {
                    Session::set_flash('success', "Contributor was successfully saved!");
                    Response::redirect('admin/contributor');
                }

                $this->template->flash_error = "Failed attempt to save contributor".self::THIS_SHOULD_NOT_HAPPEN;
            }
            else
            {
                /**
                 * Sticky
                 */
                $this->value['name'] = Input::post('name');
                $this->value['about'] = Input::post('about');
                $this->value['organization_name'] = Input::post('organization_name');

                /**
                 * Validation error messages
                 */
                $this->errmsg['name'] = $val_contributor->error('name');
                $this->errmsg['about'] = $val_contributor->error('about');
                $this->errmsg['organization_name'] = $val_contributor->error('organization_name');
            }
        }

        $this->template->ng_scripts[] = 'ng-multiple-autocomplete.js';

        $nonprofit = array();
        foreach (DB::query('SELECT name FROM nonprofits ORDER BY 1')->execute()->as_array() as $np)
            $nonprofit[] = $np['name'];

        $this->reddo('admin/contributor_create.twig', array(
            'nonprofit_arr' => $nonprofit,
        ), false);
    }
}
