<?php

class Controller_Admin_Topgiver extends Controller_Admin_Base
{
    public function action_index()
    {
        if (Input::method() === "POST")
        {
            $id = null;
            $data = array();

            foreach (Input::post() as $key => $value)
            {
                if ($key === "topgiver_id") {
                    $id = $value;
                } else {
                    $data[$key] = $value;
                }
            }
            
            if ($id) {
                $donor = Model_Donor::find($id);

                if ($donor) {
                    foreach ($data as $key => $value) {
                        $donor->info[$key] = $value;
                    }
                    if ($donor->save()) {
                        $this->template->flash_success = "Top giver updates was successful saved!";
                    } else {
                        $this->template->flash_error = "Failed attempt to update top giver".self::THIS_SHOULD_NOT_HAPPEN;
                    }
                } else {
                    $this->template->flash_error = "Top giver no longer exists on database";
                }
            } else {
                $this->template->flash_error = "Failed attempt to update top giver".self::THIS_SHOULD_NOT_HAPPEN;
            }
        }

        $this->reddo('admin/topgiver_list.twig', array(
            'topgivers' => Model_Donor::find()->get(),
        ));
    }

    public function action_create()
    {
        if (Input::method() == "POST")
        {
            if (Input::post('name'))
            {
                $donor = new Model_Donor();
                $donor->name = Input::post('name');

                $image_filenames = Helper::upload_multiple_files('donor', false);

                if (is_array($image_filenames))
                {
                    if (isset($image_filenames[0]))
                        $donor->image_filename = $image_filenames[0]['saved_as'];
                    else
                        $donor->image_filename = "";
                }

                $donor->info = Model_Donor_Info::forge(Input::post());

                if ($donor->save())
                {
                    Session::set_flash('success', "Donor was successfully saved!");
                    Response::redirect('admin/topgiver');
                }

                $this->template->flash_error = "Failed attempt to save donor".self::THIS_SHOULD_NOT_HAPPEN;
            }
            else
            {
                $this->value['name'] = Input::post('name');
                $this->errmsg['name'] = "The field Name is required and must contain a value.";
            }
        }

        $this->reddo('admin/topgiver_create.twig', array(
            'fields' => Model_Donor_Info::get_properties(),
        ));
    }
}
