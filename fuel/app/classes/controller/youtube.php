<?php

class Controller_Youtube extends Controller_Base
{
    public function action_index()
    {
        $videoId = isset($_GET['videoId']) ? $_GET['videoId'] : "0Bmhjf0rKe8";
        $height = 315;
        $width = 423;

        echo '<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Youtube Video</title>
        <script src="http://www.youtube.com/player_api"></script>
        <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
        <style>
        body {
            margin: 0;
            padding: 0;
        }
        #lastscreen {
            background-color: #000000;
            height: 322px;
            line-height: 322px;
            text-align: center;
            font-size: 20px;
            color: #999;
        }
        </style>
    </head>
    <body>
    <div id="player"></div>
<script>
    function toColorBox() {

        // create youtube player
        var player;
        var onYouTubePlayerAPIReady = function() {
            player = new YT.Player("player", {
              height: "' . $height . '",
              width: "' . $width . '",
              videoId: "' . $videoId . '",
              events: {
                "onReady": onPlayerReady,
                "onStateChange": onPlayerStateChange
              },
              rel: 0
            });
        }

        // autoplay video
        var onPlayerReady = function(event) {
            event.target.playVideo();
        }

        // when video ends
        var onPlayerStateChange = function(event) {
            if(event.data === 0) {
                $("body").html("<div id=\'lastscreen\'></div>")
                top.window.location.href="/registration";
            }
        }

        onYouTubePlayerAPIReady();
    }
    setTimeout(function(){
        toColorBox();
    }, 100);
</script>
    </body>
</html>';
        exit;
    }
}
