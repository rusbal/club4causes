<?php
/**
 * helper.php
 */

class Helper
{
    public static function exit_on_honeypot_captcha()
    {
        /**
         * Check honeypot captcha
         */
        if (Input::post('userComment') != "")
        {
            /**
             * This is not human entry!
             */
            echo "Thank you, have a nice day!";
            exit;
        }
    }

    public static function save_text2file($string, $filename, $folder = "static")
    {
        $path = DOCROOT.DS."web_data/$folder/$filename";
        return file_put_contents($path, $string) !== false;
    }

    public static function tidy_html($raw_html = "")
    {
        $tidy = new tidy();
        $tidy->parseString($raw_html, array('show-body-only'=>true), 'utf8');
        $tidy->cleanRepair();
        return $tidy;
    }

    public static function set_get_locale()
    {
        if (isset($_GET['locale'])) {
            Session::set('locale', $_GET['locale']);
            return $_GET['locale'];
        }
        if ($lang = Session::get('locale')) {
            return $lang;
        }
        return 'en_US';
    }

    public static function str_to_array($string, $separator = ',')
    {
        $out_arr = array();
        $arr_from_csv = explode($separator, $string);
        
        foreach ($arr_from_csv as $key => $item) {
            if ($item = trim($item))
                $out_arr[$key] = $item;
        }
        return $out_arr;
    }

    public static function paginate($count, $limit_per_page)
    {
        $pagination = array();
        $i = 1;

        while ($count > $limit_per_page)
        {
            $pagination[] = array(
                'from' => $i,
                'to' => $i + $limit_per_page - 1,
            );
            
            $i += $limit_per_page;
            $count -= $limit_per_page;
        }

        if ($pagination) {
            $pagination[] = array(
                'from' => $i,
                'to' => $i + $count - 1,
            );
        }

        return $pagination;
    }

    public static function random_string($length = 10)
    {
        return substr(md5(rand()), 0, $length);
    }

    public static function remove_non_ascii($input)
    {
        return preg_replace('/[^(\x20-\x7F)]*/','', $input);
    }

    public static function extract_emails($string)
    {
        $pattern = '/[a-z0-9_\-\+]+@[a-z0-9\-]+\.([a-z]{2,3})(?:\.[a-z]{2})?/i';
        preg_match_all($pattern, $string, $matches);
        return $matches[0];
    }

    public static function get_avatar($user_id, $email, $size = 122)
    {
        if ($user_id && $img_filename = Helper::get_image_from_glob("member/" . "webcam_" . $user_id . ".*"))
            $img_filename = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $img_filename;
        else
            $img_filename = "http://www.gravatar.com/avatar/" . md5($email) . "?d=mm&amp;s=$size";

        return $img_filename;
    }

    public static function get_image_from_glob($path_wildcard)
    {
        $image = null;

        $ext_whitelist = array('jpg', 'jpeg', 'gif', 'png');
        $arr = glob( DOCROOT.DS."web_data/$path_wildcard" );

        if (count($arr) > 0)
        {
            foreach ($arr as $path_file)
            {
                $ext = pathinfo($path_file, PATHINFO_EXTENSION);
                if (in_array($ext, $ext_whitelist))
                {
                    $image = substr($path_file, strlen(DOCROOT.DS));
                    break;
                }
            }
        }

        return $image;
    }

    public static function delete_member_imgfile($user_id)
    {
        $mask = DOCROOT.DS."web_data/member/" . $user_id . ".*";
        array_map( "unlink", glob( $mask ) );
    }

    public static function rename_uploaded_file($path, $old_name, $new_name)
    {
        $path = DOCROOT.DS."web_data/$path/";
        return rename($path.$old_name, $path.$new_name);
    }

    public static function upload_multiple_files($path, $require_one = true)
    {
        Upload::process(array(
            'path' => DOCROOT.DS."web_data/$path/",
            'auto_rename' => true,
            'change_case' => 'lower',
            'normalize' => true,
            'ext_whitelist' => array('img', 'jpg', 'jpeg', 'gif', 'png'),
        ));

        if ( ! Upload::is_valid())
            return false;

        Upload::save();
        
        return Upload::get_files();
    }

    public static function upload_file($path, $require_one = true)
    {
        Upload::process(array(
            'path' => DOCROOT.DS."web_data/$path/",
            'auto_rename' => true,
            'change_case' => 'lower',
            'normalize' => true,
            'ext_whitelist' => array('img', 'jpg', 'jpeg', 'gif', 'png'),
        ));

        $filename = null;
        $str_error = null;

        if (Upload::is_valid())
        {
            Upload::save();
            $image = current(Upload::get_files());
            $filename = $image['saved_as'];
        }

        $error_array = current(Upload::get_errors());

        if ($error_array['errors'][0]['error'] == Upload::UPLOAD_ERR_NO_FILE)
        {
            if ($require_one)
                $str_error = $error_array['errors'][0]['message'];
        }
        else
        {
            $str_error = $error_array['errors'][0]['message'];
        }

        return array($filename, $str_error);
    }

    public static function extract_integer($str)
    {
        preg_match_all('!\d+!', $str, $matches);
        return (int) implode('', $matches[0]);
    }

    public static function format_phone($phone)
    {
        if ( ! is_numeric($phone))
            return $phone;

        $phone = str_pad($phone, 10, "0", STR_PAD_LEFT);
        return '(' . substr($phone, 0, 3) . ')- ' . substr($phone, 3, 3) . '-' . substr($phone, 6, 4);
    }

    public static function send_invitation_emails($inviter_name, $arr_emails)
    {
        $recipient_emails = array();

        $message = "<p>Hello!</p>
        <p>Join Club4Causes to support your favorite non-profit organization.</p>
        <p>See you,</p>
        <p>$inviter_name</p>
        ";

        foreach ($arr_emails as $email)
            if (self::send_email("$inviter_name invites you to join Club4Causes!", $message, $email, "Friend"))
                $recipient_emails[] = $email;

        return $recipient_emails;
    }

    public static function send_register_confirm_email($subject, $message, $email, $name)
    {
        return self::send_email($subject, $message, $email, $name);
    }

    public static function send_email($subject, $message, $email, $name)
    {
        $html_message = '<html>
        <head><title>' . $subject . '</title></head>
        <body>' . nl2br(stripslashes($message)) . '</body>
        </html>';

        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        // Additional headers
        $headers .= 'To: ' . $name . ' <' . $email . '>' . "\r\n";
        $headers .= 'From: ' . Controller_Base::APP_NAME . ' <' . Controller_Base::APP_EMAIL . '>' . "\r\n";
        $headers .= 'Reply-To: ' . Controller_Base::APP_EMAIL . "\r\n";
        $headers .= 'Bcc: ' . Controller_Base::PROGRAMMERS_EMAIL . "\r\n";

        // Mail it
        return mail($email, $subject, $html_message, $headers);
    }

    public static function render_flag($country)
    {
        return '<div class="flag flag-' . Model_Static::get_country_code($country) . ' country_flag"></div>';
    }

    public static function time_difference($unix_date)
    {
        $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
        $lengths = array("60","60","24","7","4.35","12","10");

        $now = time();

        if (empty($unix_date)) {
            return "Invalid date";
        }

        //Check to see if it is past date or future date
        if($now > $unix_date) {
            $difference = $now - $unix_date;
            $tense = "ago";

        } else {
            $difference = $unix_date - $now;
            $tense = "from now";
        }

        for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
            $difference /= $lengths[$j];
        }

        $difference = round($difference);

        if($difference != 1) {
            $periods[$j].= "s";
        }

        return "$difference $periods[$j] {$tense}";
    }
}

// eof
