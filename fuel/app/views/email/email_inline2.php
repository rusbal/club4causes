<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;">
<head><title>Promotional Email</title></head>
<body style="font-variant: normal; font-weight: normal; font-size: 12px; margin: 0; background-color: #1d79a2; line-height: 1.5em; font-family: 'arial'; border: 0; color: #292929; padding: 0; vertical-align: baseline; font-style: normal;" bgcolor="#1d79a2">
<style type="text/css">
blockquote:before { content: none !important; }
blockquote:after { content: none !important; }
q:before { content: none !important; }
q:after { content: none !important; }
#menu-bot li#quizzes a:hover span { background-position: 0px -145px !important; }
#menu-bot li#survey a:hover span { background-position: 0px -144px !important; }
#menu-bot li#polls a:hover span { background-position: 0px -144px !important; }
#menu-bot li#games a:hover span { background-position: 0px -144px !important; }
#menu-bot li#contest a:hover span { background-position: 0px -154px !important; }
></style>

	<div style="font-variant: normal; font-weight: normal; font-size: normal; margin: 9px auto; background-color: #ffffff; line-height: normal; border: 0; padding: 0; box-shadow: rgba(0,0,0,.5) 0px 0px 15px; vertical-align: baseline; font-style: normal; width: 680px;">
		<div style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; background-color: #393939; padding: 10px; border: 0; line-height: normal; color: #dcdcdc; vertical-align: baseline; font-style: normal;">
			<?php echo date('F d, Y', time());?>
		</div>
		<div style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border-width: 0 0 1px; padding: 0; border-bottom-style: dashed; border-bottom-color: #444; vertical-align: baseline; font-style: normal;">

			<h1 style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;">
				<img src="<?php echo Uri::base();?>assets/img/site_logo.png" alt="site logo" style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; padding: 22px; vertical-align: baseline; font-style: normal; width: 113px;">
</h1>



		</div>


		<div style="font-variant: normal; font-weight: normal; font-size: normal; margin: 10px 0; padding: 0; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;">
			<div style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 10px 30px; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;">
				<h2 style="font-variant: normal; font-weight: bold; font-size: 1.8em; margin: 0 0 5px; padding: 0; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;">
<span style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; color: #868686; padding: 0; vertical-align: baseline; font-style: normal;">Welcome</span> AVJ School,</h2>

				<p style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; color: #565656; padding: 0; vertical-align: baseline; font-style: normal;">Let's put our heads together and make some more money</p>
			</div>


		</div>

		<div style="font-variant: normal; font-weight: normal; font-size: normal; margin: 10px 0; border-top-color: #f6f6f6; background-attachment: scroll; line-height: normal; border-bottom-style: solid; padding: 10px; border-top-style: solid; border-bottom-color: #f6f6f6; border-width: 1px 0; vertical-align: baseline; text-align: center; background-image: url('<?php echo Uri::base();?>assets/img/stripe.png'); font-style: normal; background-position: 10px 10px;" align="center">
			<p style="font-variant: normal; font-weight: normal; font-size: normal; margin: 10px 0; padding: 0; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;">
				<span style="font-variant: normal; font-weight: bold; font-size: 1.4em; margin: 0; line-height: normal; border: 0; color: #868686; padding: 0; vertical-align: baseline; font-style: normal;">
					GET MORE POINTS MEANS
				</span>
			</p>
			<p style="font-variant: normal; font-weight: normal; font-size: normal; margin: 10px 0; padding: 0; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;">
				<span style="font-variant: normal; font-weight: normal; font-size: 3em; margin: 0; line-height: normal; border: 0; color: #000000; padding: 0; vertical-align: baseline; font-style: normal;">
					GET 
					<span style="font-variant: normal; font-weight: bold; font-size: normal; margin: 0; padding: 0; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;">
						BIGGER
					</span>
					<span style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; color: #d80401; padding: 0; vertical-align: baseline; font-style: normal;">
							DONATIONS
					</span>
				</span>
			</p>
		</div>

		<div style="font-variant: normal; font-weight: normal; font-size: normal; margin: 10px 0; padding: 0; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;">
			<div style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 10px 30px; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;">
				<h2 style="font-variant: normal; font-weight: normal; font-size: 1.8em; margin: 0 0 5px; padding: 0; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;">HERE IS A FEW IDEAS</h2>
				<ul style="font-variant: normal; position: relative; font-weight: bold; font-size: 14px; margin: 0; list-style-type: none; border: 0; padding: 0; line-height: normal; vertical-align: baseline; font-style: italic;">
<li style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0 0 0 25px; line-height: 25px; border: 0; color: black; padding: 0; list-style-type: decimal; vertical-align: baseline; font-style: normal;">
						<span style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; color: #565656; padding: 0; vertical-align: baseline; font-style: normal;">
							Add this badge <img src="<?php echo Uri::base();?>assets/img/promotional_page_logo.gif" style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 0; border: 0; line-height: normal; vertical-align: middle; font-style: normal;"> to you emails, website, newsletters and press releases
						</span>
					</li>
					<li style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0 0 0 25px; line-height: 25px; border: 0; color: black; padding: 0; list-style-type: decimal; vertical-align: baseline; font-style: normal;">
						<span style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; color: #565656; padding: 0; vertical-align: baseline; font-style: normal;">
							Blog about us and mention us in your newsletters and press releases.
						</span>
					</li>
					<li style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0 0 0 25px; line-height: 25px; border: 0; color: black; padding: 0; list-style-type: decimal; vertical-align: baseline; font-style: normal;">
						<span style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; color: #565656; padding: 0; vertical-align: baseline; font-style: normal;">
							Encourage your employees and supporters to become members.
						</span>
					</li>
				</ul>
</div>
			
		</div>

		<div style="font-variant: normal; font-weight: normal; font-size: normal; margin: 10px 0; background-color: #ecf4f7; padding: 5px 10px; border: 0; line-height: normal; text-align: center; vertical-align: baseline; font-style: normal;" align="center">
			<p style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; color: black; padding: 0; vertical-align: baseline; font-style: normal;">
				Need some help, or brainstorm? "We are your Partner"
			</p>
			<p style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;">
				<a href="mailto:club4causes.com?Subject=We%20are%20your%20Partner" style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; text-decoration: none; color: 146495; padding: 0; vertical-align: baseline; font-style: normal;">Partners@club4causes.com</a>
			</p>
			

		</div>

		<div style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 10px; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;">
			<div style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;">

				<div style="font-variant: normal; font-weight: normal; font-size: normal; margin: -5px 0 15px; line-height: 1.3; border: 0; padding: 0; vertical-align: baseline; text-align: center; font-style: normal;" align="center">

					<p style="font-variant: normal; font-weight: normal; font-size: 2em; margin: 0; padding: 0; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;">TODAYS CHALLENGES</p>
					<p style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; color: #898989; padding: 0; vertical-align: baseline; font-style: normal;">TEST YOURSELF and GET POINTS FOR AVJ SCHOOL</p>

				</div>

				<ul style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; padding: 0; list-style-type: none; vertical-align: baseline; font-style: normal;">
<li style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; padding: 9px; display: inline-block; vertical-align: baseline; font-style: normal; float: left;"><a href="" style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; text-decoration: none; padding: 0; vertical-align: baseline; font-style: normal;"><span style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; padding: 0; display: inline-block; vertical-align: baseline; height: 145px; background-image: url(<?php echo Uri::base();?>assets/img/quizzes_btn.gif); font-style: normal; width: 113px;"></span></a></li>
					<li style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; padding: 9px; display: inline-block; vertical-align: baseline; font-style: normal; float: left;"><a href="" style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; text-decoration: none; padding: 0; vertical-align: baseline; font-style: normal;"><span style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; padding: 0; display: inline-block; vertical-align: baseline; height: 145px; background-image: url(<?php echo Uri::base();?>assets/img/survey_btn.gif); font-style: normal; width: 113px;"></span></a></li>
					<li style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; padding: 9px; display: inline-block; vertical-align: baseline; font-style: normal; float: left;"><a href="" style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; text-decoration: none; padding: 0; vertical-align: baseline; font-style: normal;"><span style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; padding: 0; display: inline-block; vertical-align: baseline; height: 145px; background-image: url(<?php echo Uri::base();?>assets/img/polls.gif); font-style: normal; width: 113px;"></span></a></li>
					<li style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; padding: 9px; display: inline-block; vertical-align: baseline; font-style: normal; float: left;"><a href="" style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; text-decoration: none; padding: 0; vertical-align: baseline; font-style: normal;"><span style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; padding: 0; display: inline-block; vertical-align: baseline; height: 145px; background-image: url(<?php echo Uri::base();?>assets/img/games.gif); font-style: normal; width: 113px;"></span></a></li>
					<li style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; padding: 9px; display: inline-block; vertical-align: baseline; font-style: normal; float: left;"><a href="" style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; text-decoration: none; padding: 0; vertical-align: baseline; font-style: normal;"><span style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; padding: 0; display: inline-block; vertical-align: baseline; height: 145px; background-image: url(<?php echo Uri::base();?>assets/img/contest.gif); font-style: normal; width: 113px;"></span></a></li>
				</ul>
</div>
			<div style="font-variant: normal; font-size: normal; font-weight: normal; clear: both; margin: 0; padding: 0; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;"></div>

			<div style="font-variant: normal; font-weight: normal; font-size: normal; margin: 10px 0; padding: 0; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;">

				<ul style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; padding: 0; list-style-type: none; vertical-align: baseline; font-style: normal;">
<li style="font-variant: normal; font-weight: normal; font-size: 18px; margin: 10px; background-color: #FCCCFC; line-height: 18px; border: 1px solid #dddddd; padding: 16px; box-shadow: -3px 10px 27px #062D46; display: inline-block; text-align: center; height: 110px; vertical-align: baseline; font-style: normal; border-radius: 10px 10px 10px 10px; width: 110px; float: left;">aaaaaa</li>
					<li style="font-variant: normal; font-weight: normal; font-size: 18px; margin: 10px; background-color: #FCCCFC; line-height: 18px; border: 1px solid #dddddd; padding: 16px; box-shadow: -3px 10px 27px #062D46; display: inline-block; text-align: center; height: 110px; vertical-align: baseline; font-style: normal; border-radius: 10px 10px 10px 10px; width: 110px; float: left;">aaaa</li>
					<li style="font-variant: normal; font-weight: normal; font-size: 18px; margin: 10px; background-color: #FCCCFC; line-height: 18px; border: 1px solid #dddddd; padding: 16px; box-shadow: -3px 10px 27px #062D46; display: inline-block; text-align: center; height: 110px; vertical-align: baseline; font-style: normal; border-radius: 10px 10px 10px 10px; width: 110px; float: left;">aaaaaa</li>
					<li style="font-variant: normal; font-weight: normal; font-size: 18px; margin: 10px; background-color: #FCCCFC; line-height: 18px; border: 1px solid #dddddd; padding: 16px; box-shadow: -3px 10px 27px #062D46; display: inline-block; text-align: center; height: 110px; vertical-align: baseline; font-style: normal; border-radius: 10px 10px 10px 10px; width: 110px; float: left;">aaaaaaaaaa</li>
					<li style="font-variant: normal; font-weight: normal; font-size: 18px; margin: 10px; background-color: #FCCCFC; line-height: 18px; border: 1px solid #dddddd; padding: 16px; box-shadow: -3px 10px 27px #062D46; display: inline-block; text-align: center; height: 110px; vertical-align: baseline; font-style: normal; border-radius: 10px 10px 10px 10px; width: 110px; float: left;">aaaaaa</li>
					<li style="font-variant: normal; font-weight: normal; font-size: 18px; margin: 10px; background-color: #FCCCFC; line-height: 18px; border: 1px solid #dddddd; padding: 16px; box-shadow: -3px 10px 27px #062D46; display: inline-block; text-align: center; height: 110px; vertical-align: baseline; font-style: normal; border-radius: 10px 10px 10px 10px; width: 110px; float: left;">aaaa</li>
					<li style="font-variant: normal; font-weight: normal; font-size: 18px; margin: 10px; background-color: #FCCCFC; line-height: 18px; border: 1px solid #dddddd; padding: 16px; box-shadow: -3px 10px 27px #062D46; display: inline-block; text-align: center; height: 110px; vertical-align: baseline; font-style: normal; border-radius: 10px 10px 10px 10px; width: 110px; float: left;">aaaaaa</li>
					<li style="font-variant: normal; font-weight: normal; font-size: 18px; margin: 10px; background-color: #FCCCFC; line-height: 18px; border: 1px solid #dddddd; padding: 16px; box-shadow: -3px 10px 27px #062D46; display: inline-block; text-align: center; height: 110px; vertical-align: baseline; font-style: normal; border-radius: 10px 10px 10px 10px; width: 110px; float: left;">aaaaaaaaaa</li>

				</ul>
<div style="font-variant: normal; font-size: normal; font-weight: normal; clear: both; margin: 0; padding: 0; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;"></div>
			</div>

			<div style="font-variant: normal; font-weight: normal; font-size: normal; margin: 10px 0; padding: 0; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;">
				<table style="font-variant: normal; font-size: normal; font-weight: normal; border-collapse: separate; margin: 0; line-height: normal; border: 0; padding: 0; color: #f2f2f2; box-shadow: inset rgba(0,0,0,.5) -2px 13px 13px; border-spacing: 0; text-align: center; vertical-align: baseline; font-style: normal; width: 100%;">
<thead style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; text-transform: uppercase; padding: 0; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;"><tr style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;">
<td style="font-variant: normal; font-weight: bold; font-size: 15px; margin: 0; background-color: #fab907; line-height: normal; border-bottom-style: solid; padding: 5px; border-bottom-color: #f8a605; border-width: 0 0 1px; vertical-align: middle; height: 35px; font-style: normal; width: 25%;" bgcolor="#fab907" valign="middle">news</td>


							<td style="font-variant: normal; font-weight: bold; font-size: 15px; margin: 0; background-color: #25ac33; line-height: normal; border-bottom-style: solid; padding: 5px; border-bottom-color: #1a9924; border-width: 0 0 1px; vertical-align: middle; height: 35px; font-style: normal; width: 25%;" bgcolor="#25ac33" valign="middle">entertainment</td>


							<td style="font-variant: normal; font-weight: bold; font-size: 15px; margin: 0; background-color: #17b0ab; line-height: normal; border-bottom-style: solid; padding: 5px; border-bottom-color: #0f9d97; border-width: 0 0 1px; vertical-align: middle; height: 35px; font-style: normal; width: 25%;" bgcolor="#17b0ab" valign="middle">sports</td>


							<td style="font-variant: normal; font-weight: bold; font-size: 15px; margin: 0; background-color: #da2b28; line-height: normal; border-bottom-style: solid; padding: 5px; border-bottom-color: #d01e1c; border-width: 0 0 1px; vertical-align: middle; height: 35px; font-style: normal; width: 25%;" bgcolor="#da2b28" valign="middle">other</td>
						</tr></thead>
<tbody style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;"><tr style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;">
<td style="font-variant: normal; font-size: normal; font-weight: normal; border-top-color: #faca44; margin: 0; background-color: #fab907; border-width: 1px 0 0; word-wrap: break-word; padding: 10px 32px; line-height: normal; border-top-style: solid; vertical-align: baseline; font-style: normal;" bgcolor="#fab907" valign="baseline">
								Business News, Headlines, Politics
							</td>
							<td style="font-variant: normal; font-size: normal; font-weight: normal; border-top-color: #5dc266; margin: 0; background-color: #25ac33; border-width: 1px 0 0; word-wrap: break-word; padding: 10px 32px; line-height: normal; border-top-style: solid; vertical-align: baseline; font-style: normal;" bgcolor="#25ac33" valign="baseline">
								Best Seller Books (Fiction), Celebrities, Current Music, Fashion, Best Seller Books (Non-Fiction), Current Movies, Current T.V.
							</td>
							<td style="font-variant: normal; font-size: normal; font-weight: normal; border-top-color: #51c4bf; margin: 0; background-color: #17b0ab; border-width: 1px 0 0; word-wrap: break-word; padding: 10px 32px; line-height: normal; border-top-style: solid; vertical-align: baseline; font-style: normal;" bgcolor="#17b0ab" valign="baseline">
								Baseball, Boxing, Fishing, Hockey, outdoors, Soccer, Tennis, Basketball, Fantasy Sports, Golf, Nascar, Scuba Diving, UFC
							</td>
							<td style="font-variant: normal; font-size: normal; font-weight: normal; border-top-color: #e4605e; margin: 0; background-color: #da2b28; border-width: 1px 0 0; word-wrap: break-word; padding: 10px 32px; line-height: normal; border-top-style: solid; vertical-align: baseline; font-style: normal;" bgcolor="#da2b28" valign="baseline">
								Antiques, Cars, History Recipes &amp; Cooking, Trivia, Art, Geography, pets, Travel
							</td>

						</tr></tbody>
</table>
</div>


			<div style="font-variant: normal; font-weight: normal; font-size: 1.5em; margin: 10px 0; background-color: #5b3eaa; line-height: 1.1; border: 0; color: #ffffff; padding: 14px; vertical-align: baseline; text-align: center; font-style: normal;" align="center">
				<span style="font-variant: normal; font-weight: bold; font-size: normal; margin: 0; padding: 0; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;">
					Daily Challenges:

				</span>
				Bingo, Chess, Bridge, Crossword, Poker, Puzzles, Slots, Solitaire, Sudoku, Games, Scrabble, Black Jack, Fantasy Sports.

			</div>

			<div style="font-variant: normal; font-weight: normal; font-size: normal; margin: 10px 0; padding: 0; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;">

				<span style="font-variant: normal; font-weight: bold; font-size: 1.3em; margin: 0; line-height: normal; border: 0; padding: 10px; color: #797979; display: block; text-align: center; vertical-align: baseline; font-style: normal; width: 100%;">
					Point Allocations
				</span>
				<div style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;">
					<table style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0 0 10px; border-collapse: collapse; line-height: normal; border: 1px solid #ffbc09; padding: 0; border-spacing: 0; vertical-align: baseline; font-style: normal; float: left; width: 49.5%;">
<thead style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;"><tr style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;">
<td colspan="2" style="font-variant: normal; font-weight: normal; font-size: 13px; margin: 0; background-color: #ffbc09; line-height: normal; border: 0; color: #000000; padding: 3px 12px; vertical-align: baseline; text-align: center; font-style: normal;" bgcolor="#ffbc09" valign="baseline" align="center">Easy Points</td>
							</tr></thead>
<tbody style="font-variant: normal; font-weight: normal; font-size: 11px; margin: 0; line-height: normal; border: 0; color: #828282; padding: 0; vertical-align: baseline; font-style: normal;">
<tr style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;">
<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;" valign="baseline">Refering a new number</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;" valign="baseline">500 each.</td>
							</tr>
<tr style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;">
<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;" valign="baseline">Each login</td>

								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;" valign="baseline">200 (200 per daymaximum)</td>
							</tr>
<tr style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;">
<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;" valign="baseline">Participating in a chat room</td>

								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;" valign="baseline">200 (200 per daymaximum)</td>
							</tr>
<tr style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;">
<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;" valign="baseline">Each shopping purchase</td>

								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;" valign="baseline">2000 (each check out)</td>
							</tr>
</tbody>
</table>
<table style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0 0 10px; border-collapse: collapse; line-height: normal; border: 1px solid #ffbc09; padding: 0; border-spacing: 0; vertical-align: baseline; font-style: normal; float: right; width: 49.5%;">
<thead style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;"><tr style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;">
<td colspan="2" style="font-variant: normal; font-weight: normal; font-size: 13px; margin: 0; background-color: #ffbc09; line-height: normal; border: 0; color: #000000; padding: 3px 12px; vertical-align: baseline; text-align: center; font-style: normal;" bgcolor="#ffbc09" valign="baseline" align="center">Sponsorship</td>
							</tr></thead>
<tbody style="font-variant: normal; font-weight: normal; font-size: 11px; margin: 0; line-height: normal; border: 0; color: #828282; padding: 0; vertical-align: baseline; font-style: normal;">
<tr style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;">
<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;" valign="baseline">Member's page</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;" valign="baseline">5,000 points</td>
							</tr>
<tr style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;">
<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;" valign="baseline">Non Profit's page</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;" valign="baseline">10,000 points</td>
							</tr>
<tr style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;">
<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;" valign="baseline">Other</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;" valign="baseline">10,000 points</td>
							</tr>
<tr style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;">
<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;" valign="baseline">&nbsp;</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;" valign="baseline">&nbsp;</td>
							</tr>
</tbody>
</table>
<div style="font-variant: normal; font-size: normal; font-weight: normal; clear: both; margin: 0; padding: 0; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;"></div>
				</div>

				<div style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;">
					<table style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0 0 10px; border-collapse: collapse; line-height: normal; border: 1px solid #ffbc09; padding: 0; border-spacing: 0; vertical-align: baseline; font-style: normal; width: 100%;">
<thead style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;"><tr style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;">
<td colspan="6" style="font-variant: normal; font-weight: normal; font-size: 13px; margin: 0; background-color: #ffbc09; line-height: normal; border: 0; color: #000000; padding: 3px 12px; vertical-align: baseline; text-align: center; font-style: normal;" bgcolor="#ffbc09" valign="baseline" align="center">Challenges</td>
							</tr></thead>
<tbody style="font-variant: normal; font-weight: normal; font-size: 11px; margin: 0; line-height: normal; border: 0; color: #828282; padding: 0; vertical-align: baseline; font-style: normal;">
<tr style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;">
<td style="font-variant: normal; font-weight: bold; font-size: 12px; margin: 0; padding: 10px; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;" valign="baseline">Challenges</td>
								<td style="font-variant: normal; font-weight: bold; font-size: 12px; margin: 0; padding: 10px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">Participate</td>
								<td style="font-variant: normal; font-weight: bold; font-size: 12px; margin: 0; padding: 10px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">Plus</td>
								<td style="font-variant: normal; font-weight: bold; font-size: 12px; margin: 0; padding: 10px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">Win</td>
								<td style="font-variant: normal; font-weight: bold; font-size: 12px; margin: 0; padding: 10px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">Second</td>
								<td style="font-variant: normal; font-weight: bold; font-size: 12px; margin: 0; padding: 10px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">Third</td>
							</tr>
<tr style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;">
<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;" valign="baseline">Contests</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">500</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">---</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">3,000</td>	
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">2,000</td>	
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">1,000</td>				
							</tr>
<tr style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;">
<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;" valign="baseline">Games</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">500</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">---</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">---</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">---</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">---</td>	
							</tr>
<tr style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;">
<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;" valign="baseline">Quizzes</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">500</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">---</td>	
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">1,000</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">---</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">---</td>
							</tr>
<tr style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;">
<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;" valign="baseline">Survey and Polls</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">500</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">---</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">---</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">---</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">---</td>	
							</tr>
<tr style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;">
<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;" valign="baseline">Fantasy Games</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">1000</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">---</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">5,000</td>	
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">3,000</td>	
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">2,000</td>	
							</tr>
<tr style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; font-style: normal; border: 0; padding: 0; line-height: normal; vertical-align: baseline;">
<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;" valign="baseline">Puzzles</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">500</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">---</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">1,000</td>		
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">---</td>
								<td style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; padding: 3px 12px; border: 0; line-height: normal; vertical-align: baseline; font-style: italic;" valign="baseline">---</td>
							</tr>
</tbody>
</table>
</div>



			</div>

			<div style="font-variant: normal; font-weight: normal; font-size: normal; margin: 10px 0; background-color: #f9f9f9; padding: 25px; border: 1px solid #dbdbdb; line-height: normal; text-align: center; vertical-align: baseline; font-style: normal;" align="center">

			<span style="font-variant: normal; font-weight: bold; font-size: 3em; text-shadow: 1px 4px 6px #f9f9f9, 0 -1px 0 rgba(0,0,0,.3), 1px 4px 6px #f9f9f9; margin: 0; text-transform: capitalize; padding: 0; border: 0; line-height: normal; color: #dfdfdf; vertical-align: baseline; font-style: normal; letter-spacing: 2;">ADVERTISING</span>

		</div>

	</div>



	<div style="font-variant: normal; font-size: normal; font-weight: normal; border-top-color: #eeeeee; margin: 0; background-color: #f8f8f8; line-height: normal; border-width: 1px 0 0; padding: 10px 20px; color: #9c9a9b; border-top-style: solid; text-align: center; vertical-align: baseline; font-style: normal;" align="center">
		<p style="font-variant: normal; font-weight: normal; font-size: normal; margin: 10px 0 0; padding: 0; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;"> Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>

		<p style="font-variant: normal; font-weight: normal; font-size: normal; margin: 10px 0 0; padding: 0; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;">
			Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie <br> consequat, vel illum dolore eu feugiat nulla facilisis 

		</p>
	</div>

</div>

<div style="font-variant: normal; font-weight: normal; font-size: normal; margin: 10px 0; line-height: normal; border: 0; color: #ffffff; padding: 0; vertical-align: baseline; text-align: center; font-style: normal;" align="center">


	<ul style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; padding: 0; list-style-type: none; vertical-align: baseline; font-style: normal;">
<li style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; padding: 0; display: inline-block; vertical-align: baseline; font-style: normal;">
			Go to Club4Causes.com
			|  
		</li>
		<li style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; padding: 0; display: inline-block; vertical-align: baseline; font-style: normal;">
			<a href="" style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; text-decoration: none; color: #e1df68; padding: 0; vertical-align: baseline; font-style: normal;">About</a>
			| 
		</li>
		<li style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; padding: 0; display: inline-block; vertical-align: baseline; font-style: normal;">
			<a href="" style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; text-decoration: none; color: #e1df68; padding: 0; vertical-align: baseline; font-style: normal;">Disclaimer</a> 
			| 
		</li>
		<li style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; padding: 0; display: inline-block; vertical-align: baseline; font-style: normal;">
			<a href="" style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; text-decoration: none; color: #e1df68; padding: 0; vertical-align: baseline; font-style: normal;">Privacy Policy</a> 
			| 
		</li>
		<li style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; padding: 0; display: inline-block; vertical-align: baseline; font-style: normal;">
			<a href="" style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; text-decoration: none; color: #e1df68; padding: 0; vertical-align: baseline; font-style: normal;">Terms of Usage</a> 
			| 
		</li>
		<li style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; padding: 0; display: inline-block; vertical-align: baseline; font-style: normal;">
			<a href="" style="font-variant: normal; font-weight: normal; font-size: normal; margin: 0; line-height: normal; border: 0; text-decoration: none; color: #e1df68; padding: 0; vertical-align: baseline; font-style: normal;">Contact</a>			
		</li>

	</ul>
<div style="font-variant: normal; font-size: normal; font-weight: normal; clear: both; margin: 0; padding: 0; border: 0; line-height: normal; vertical-align: baseline; font-style: normal;"></div>

</div>


</body>
</html>
