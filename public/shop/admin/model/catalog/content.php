<?php
/*------------------------------------------------------------------------------
  $Id$

  Shopping Cart
  http://dev.club4causes.com/shop

  Club4Causes v1.0

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade Shopping Cart to newer
   versions in the future. If you wish to customize Shopping Cart for your
   needs please refer to http://dev.club4causes.com/shop for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' ) || !IS_ADMIN) {
	header ( 'Location: static_pages/' );
}

class ModelCatalogContent extends Model {
	
	public function getContents() {
		
		$contents = array();
		
		$contentmanager = new AContentManager();
        $results = $contentmanager->getContents();
        $contents[] = array( 'content_id' => '0', 'title' => $this->language->get('text_none') );
        foreach( $results as $r ) {
			$contents[] = array( 'content_id' => $r['content_id'], 'title' => $r['title'] );
		}
		
		return $contents;
		
	}
		
}
?>