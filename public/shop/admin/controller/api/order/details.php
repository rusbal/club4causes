<?php
/*------------------------------------------------------------------------------
  $Id$

  Shopping Cart
  http://dev.club4causes.com/shop

  Club4Causes v1.0

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade Shopping Cart to newer
   versions in the future. If you wish to customize Shopping Cart for your
   needs please refer to http://dev.club4causes.com/shop for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' ) || !IS_ADMIN) {
	header ( 'Location: static_pages/' );
}
class ControllerApiOrderDetails extends AControllerAPI {
  
	public function get() {

		//init controller data
		$this->extensions->hk_InitData($this, __FUNCTION__);

		$this->loadLanguage('sale/order');
		$this->loadModel('sale/order');

		$request = $this->rest->getRequestParams();
		
		if ( !has_value($request['order_id']) ) {
			$this->rest->setResponseData( array('Error' => 'Order ID is missing') );
			$this->rest->sendResponse(200);
			return;
		}		

		$order_details =  $this->model_sale_order->getOrder($request['order_id']);
		if (!count($order_details)) {
			$this->rest->setResponseData( array('Error' => 'Incorrect order ID or missing order data') );
			$this->rest->sendResponse(200);
			return;			
		}
			    
        $this->extensions->hk_UpdateData($this,__FUNCTION__);

		$this->rest->setResponseData( $order_details );
		$this->rest->sendResponse( 200 );
	    
	    }
}