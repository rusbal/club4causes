<?php  
/*------------------------------------------------------------------------------
  $Id$

  Shopping Cart
  http://dev.club4causes.com/shop

  Copyright 2011 Club4Causes

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade Shopping Cart to newer
   versions in the future. If you wish to customize Shopping Cart for your
   needs please refer to http://dev.club4causes.com/shop for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' )) {
	header ( 'Location: static_pages/' );
}
class ControllerApiCommonPreflight extends AControllerAPI {
	
	public function main() {
		// This might require future imptovment. 
		if ( $_SERVER["REQUEST_METHOD"] == 'OPTIONS') {
			$this->registry->get('response')->addHeader("Access-Control-Allow-Origin: " . $_SERVER['HTTP_ORIGIN']);
			$this->registry->get('response')->addHeader("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");	
			$this->registry->get('response')->addHeader("Access-Control-Allow-Credentials: true");
    		$this->registry->get('response')->addHeader("Access-Control-Max-Age: 60");	

			$this->rest->sendResponse( 200 );	
		}
	}	
}



