<?php
/*------------------------------------------------------------------------------
  $Id$

  Shopping Cart
  http://dev.club4causes.com/shop

  Club4Causes v1.0

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade Shopping Cart to newer
   versions in the future. If you wish to customize Shopping Cart for your
   needs please refer to http://dev.club4causes.com/shop for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' ) || !IS_ADMIN) {
	header ( 'Location: static_pages/' );
}
class ControllerResponsesSettingTemplateImage extends AController {
	private $error = array(); 
      
	public function main() {

        //init controller data
        $this->extensions->hk_InitData($this,__FUNCTION__);

		$template = basename($this->request->get['template']);

		$extensions = $this->extensions->getEnabledExtensions();

        $file = $template . '/image/preview.jpg';
		if ( in_array( $template, $extensions ) && is_file( DIR_EXT . $file) ) {
            $img = HTTPS_EXT . $file;
        } else if (is_file( 'storefront/view/' . $template . '/image/preview.jpg')) {
			$img = HTTPS_SERVER . 'storefront/view/' . $template . '/image/preview.jpg';
		} else {
			$img = HTTPS_IMAGE . 'no_image.jpg';
		}

        //update controller data
        $this->extensions->hk_UpdateData($this,__FUNCTION__);
		$this->response->setOutput('<img src="' . $img . '" alt="" title="" />');

	}		
}
?>