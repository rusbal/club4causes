<?php  
/*------------------------------------------------------------------------------
  $Id$

  Shopping Cart
  http://dev.club4causes.com/shop

  Copyright 2011 Club4Causes

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade Shopping Cart to newer
   versions in the future. If you wish to customize Shopping Cart for your
   needs please refer to http://dev.club4causes.com/shop for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' )) {
	header ( 'Location: static_pages/' );
}
class ControllerApiCommonCountry extends AControllerAPI {
	protected $data = array();
	
	public function get() {
        $this->extensions->hk_InitData($this,__FUNCTION__);

		$this->loadModel('localisation/country');
        $this->data = $this->model_localisation_country->getCountries();

        $this->extensions->hk_InitData($this,__FUNCTION__);
        
		$this->rest->setResponseData( $this->data );
		$this->rest->sendResponse( 200 );	
	}

}