<?php
/*------------------------------------------------------------------------------
  $Id$

  Shopping Cart
  http://dev.club4causes.com/shop

  Club4Causes v1.0

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade Shopping Cart to newer
   versions in the future. If you wish to customize Shopping Cart for your
   needs please refer to http://dev.club4causes.com/shop for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' )) {
	header ( 'Location: static_pages/' );
}
class ControllerCommonMaintenance extends AController {
    
    public function main() {

	    //init controller data
        $this->extensions->hk_InitData($this,__FUNCTION__);

        if ($this->config->get('config_maintenance')) {
            
            // Show site only to control panel users if logged in.
			require_once(DIR_CORE . 'lib/user.php');
			$this->registry->set('user', new AUser($this->registry));
            
            if (!$this->user->isLogged()) {
                return $this->dispatch('pages/index/maintenance');
            }
        }
		//init controller data
        $this->extensions->hk_UpdateData($this,__FUNCTION__);
    }
}
?>