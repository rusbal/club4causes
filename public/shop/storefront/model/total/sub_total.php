<?php
/*------------------------------------------------------------------------------
  $Id$

  Shopping Cart
  http://dev.club4causes.com/shop

  Club4Causes v1.0

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade Shopping Cart to newer
   versions in the future. If you wish to customize Shopping Cart for your
   needs please refer to http://dev.club4causes.com/shop for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' )) {
	header ( 'Location: static_pages/' );
}
class ModelTotalSubTotal extends Model {
	public function getTotal(&$total_data, &$total, &$taxes) {
		if ($this->config->get('sub_total_status')) {
			$this->load->language('total/sub_total');
			
			$total_data[] = array( 
        		'id'         => 'subtotal',
        		'title'      => $this->language->get('text_sub_total'),
        		'text'       => $this->currency->format($this->cart->getSubTotal()),
        		'value'      => $this->cart->getSubTotal(),
				'sort_order' => $this->config->get('sub_total_sort_order'),
				'total_type' => $this->config->get('sub_total_total_type')
			);
			
			$total += $this->cart->getSubTotal();
		}
	}
}
?>