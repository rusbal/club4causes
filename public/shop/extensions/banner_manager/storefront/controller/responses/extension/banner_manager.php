<?php   
/*------------------------------------------------------------------------------
  $Id$

  Shopping Cart
  http://dev.club4causes.com/shop

  Club4Causes v1.0

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade Shopping Cart to newer
   versions in the future. If you wish to customize Shopping Cart for your
   needs please refer to http://dev.club4causes.com/shop for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' ) || !IS_ADMIN) {
	header ( 'Location: static_pages/' );
}
class ControllerResponsesExtensionBannerManager extends AController {
	public $data = array();
	public function main() {
        //init controller data
        $this->extensions->hk_InitData($this,__FUNCTION__);

		$banner_id = (int)$this->request->get['banner_id'];
		$type = (int)$this->request->get['type'];

		if($banner_id){
			$this->loadModel('extension/banner_manager');
			$this->model_extension_banner_manager->writeBannerStat($banner_id,$type);
		}

        //update controller data
        $this->extensions->hk_UpdateData($this,__FUNCTION__);
	}
}
?>