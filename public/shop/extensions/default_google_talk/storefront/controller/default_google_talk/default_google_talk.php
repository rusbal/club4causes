<?php
/*------------------------------------------------------------------------------
  $Id$

  Shopping Cart
  http://dev.club4causes.com/shop

  Club4Causes v1.0

  This source file is subject to Open Software License (OSL 3.0)
  Lincence details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade Shopping Cart to newer
   versions in the future. If you wish to customize Shopping Cart for your
   needs please refer to http://dev.club4causes.com/shop for more information.
------------------------------------------------------------------------------*/
if ( !defined ( 'DIR_CORE' )) {
	header ( 'Location: static_pages/' );
}

class ControllerDefaultGoogleTalkDefaultGoogleTalk extends AController {
	public function main() {
		$code = html_entity_decode( $this->config->get('default_google_talk_code') );
		if(!$code){
			return;
		}
		$this->loadLanguage('default_google_talk/default_google_talk');
		$this->view->assign( 'heading_title', $this->language->get('heading_title'));
        $this->view->assign( 'code', $code );
		$this->processTemplate();

	}
}
?>