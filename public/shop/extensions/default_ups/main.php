<?php
/*------------------------------------------------------------------------------
  $Id$

  Shopping Cart
  http://dev.club4causes.com/shop

  Club4Causes v1.0

  This source file is subject to Open Software License (OSL 3.0)
  Lincence details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade Shopping Cart to newer
   versions in the future. If you wish to customize Shopping Cart for your
   needs please refer to http://dev.club4causes.com/shop for more information.
------------------------------------------------------------------------------*/
if ( !defined ( 'DIR_CORE' )) {
	header ( 'Location: static_pages/' );
}

$controllers = array(
    'storefront' => array(),
    'admin' => array(
        'pages/extension/default_ups',
        'responses/extension/default_ups_save',
    ),
);

$models = array(
    'storefront' => array( 'extension/default_ups' ),
    'admin' => array( ),
);

$languages = array(
    'storefront' => array(
	    'default_ups/default_ups'),
    'admin' => array(
        'default_ups/default_ups'));

$templates = array(
    'storefront' => array(),
    'admin' => array( 'pages/extension/default_ups.tpl' ));