<?php
/*------------------------------------------------------------------------------
  $Id$

  Shopping Cart
  http://dev.club4causes.com/shop

  Club4Causes v1.0

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade Shopping Cart to newer
   versions in the future. If you wish to customize Shopping Cart for your
   needs please refer to http://dev.club4causes.com/shop for more information.
------------------------------------------------------------------------------*/
if (!defined('DIR_CORE')) {
	header('Location: static_pages/');
}

final class Registry {
	private $data = array();
	static private $instance = NULL;

	/**
	 * @return Registry
	 */
	static function getInstance() {
		if (self::$instance == NULL) {
			self::$instance = new Registry();
		}
		return self::$instance;
	}

	private function __construct() {}

	private function __clone() {}

	/**
	 * @param $key string
	 * @return ARequest|ALoader|ADocument|ADB|AConfig|AHtml|ExtensionsApi|AExtensionManager|ALanguageManager|ASession|ACache|AMessage|ALog|AResponse|AUser|ARouter| ModelLocalisationLanguageDefinitions|ModelLocalisationCountry | ModelSettingSetting | ModelInstall
	 */
	public function get($key) {
		return (isset($this->data[$key]) ? $this->data[$key] : NULL);
	}

	/**
	 * @param $key string
	 * @param $value mixed
	 */
	public function set($key, $value) {
		$this->data[$key] = $value;
	}

	/**
	 * @param $key string
	 * @return bool
	 */
	public function has($key) {
		return isset($this->data[$key]);
	}
}