<?php
/*------------------------------------------------------------------------------
  $Id$

  Shopping Cart
  http://dev.club4causes.com/shop

  Club4Causes v1.0

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade Shopping Cart to newer
   versions in the future. If you wish to customize Shopping Cart for your
   needs please refer to http://dev.club4causes.com/shop for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' )) {
	header ( 'Location: static_pages/' );
}

final class AAPI {
	protected $registry;
	protected $pre_dispatch = array();
	protected $error;
	private $recursion_limit = 0;
	
	public function __construct($registry) {
		$this->registry = $registry;
	}

	public function __destruct() {
	}

    public function __get($key) {
		return $this->registry->get($key);
	}

	public function __set($key, $value) {
		$this->registry->set($key, $value);
	}
	
	public function addPreDispatch($dispatch_rt) {
		$this->pre_dispatch[] = new ADispatcher($dispatch_rt, array("instance_id" => "0"));
	}
	
  	public function build($dispatch_rt) {
		$dispatch = '';
 		$this->recursion_limit = 0;

		foreach ($this->pre_dispatch as $pre_dispatch) {
			$result = $pre_dispatch->dispatch();					
			if ($result) {
				//Something happened. Need to run different page
				$dispatch_rt = $result;
				break;
			}
		}

		//Process disparcher in while if we have new dispatch back
		while ($dispatch_rt){
			//Process main level controller
			//filter in case we have responses set already
			$dispatch_rt = preg_replace('/^(api)\//', '', $dispatch_rt);			
            $dispatch = new ADispatcher('api/'.$dispatch_rt, array("instance_id" => "0"));
			$dispatch_rt = $dispatch->dispatch();

		}	
			
		unset($dispatch); 
  	}
}
?>