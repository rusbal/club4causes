(function($) {
    $.fn.mySlider = function(b) {
        var c = {
                transition: 'slide',
                automatic: false,
                speed: 1000,
                pause: 2000,
                backgroundImage: "/assets/img/arrows.png",
                prevLeft: '5px',
                prevMarginLeft: '0px',
                prevEnable: true,
                nextRight: '5px',
                nextMarginRight: '0px',
                nextEnable: true,
                alwaysVisible: false,
                marginTop: '-13.5px'
            },
            b = $.extend(c, b),
            btn = true,
            dircls = {
                position: 'absolute',
                overflow: 'hidden',
                width: '12px',
                height: '27px',
                cursor: 'pointer',
                top: '50%',
                marginTop: b.marginTop,
                zIndex: '2',
                display: 'none'
            },
            prevcls = $.extend({}, dircls, { left: b.prevLeft, marginLeft: b.prevMarginLeft, background: 'url("'+b.backgroundImage+'")' }),
            nextcls = $.extend({}, dircls, { right: b.nextRight, marginRight: b.nextMarginRight, background: 'url("'+b.backgroundImage+'") no-repeat right top' }),
            steps;


        if (b.pause <= b.speed)
            b.pause = b.speed + 100;

        return this.each(function() {
            var ptr = 0;
            var a = $(this);
            var pp = a.parent();
            pp.css('width', a.children().width());

            if (a.children().length > 1) {
                if (b.alwaysVisible) {
                    prevcls.display = 'block';
                    nextcls.display = 'block';
                } else {
                    pp.mouseenter(function(){
                        $('.__direction__').show();
                    }).mouseleave(function(){
                        $('.__direction__').hide();
                    });
                }
                if (b.prevEnable) {
                    pp.prepend(
                        $('<div/>').addClass('__direction__').css(prevcls).html('&nbsp;')
                            .click(function(){
                                slide_prev();
                            })
                    );
                }
                if (b.nextEnable) {
                    pp.append(
                        $('<div/>').addClass('__direction__').css(nextcls).html('&nbsp;')
                            .click(function(){
                                slide_next();
                            })
                    );
                }
                build_circles();
            }

            a.wrap($('<div/>').addClass("slider-wrap").css({ float: "left" }));
            a.css({
                'width': '99999px',
                'position': 'relative',
                'padding': 0
            });
            if (b.transition === 'slide') {
                a.children().css({
                    'float': 'left',
                    'list-style': 'none'
                });
                $('.slider-wrap').css({
                    'width': a.children().width(),
                    'overflow': 'hidden'
                })
            }
            if (b.transition === 'fade') {
                a.children().css({
                    'width': a.children().width(),
                    'position': 'absolute',
                    'left': 0
                });
                for(var i = a.children().length, y = 0; i > 0; i--, y++) {
                    a.children().eq(y).css('zIndex', i + 99999)
                }
                fade()
            }

            if (b.automatic)
                if (b.transition === 'slide') slide();

            function slide() {
                setInterval(function() {
                    a.animate({
                        'left': '-' + a.parent().width()
                    }, b.speed, function() {
                        a.css('left', 0).children(':first').appendTo(a)
                    })
                }, b.pause)
            }
            function fade() {
                setInterval(function() {
                    a.children(':first').animate({
                        'opacity': 0
                    }, b.speed, function() {
                        a.children(':first').css('opacity', 1).css('zIndex', a.children(':last').css('zIndex') - 1).appendTo(a)
                    })
                }, b.pause)
            }
            function slide_prev(continuous, nCallSelf) {
                if ( ! btn) return;
                btn = false;
                a.children(':last').css('marginLeft', -1 * a.parent().width()).prependTo(a);

                a.animate({
                    'left': '+' + a.parent().width()
                }, ( continuous ? (400/steps) : b.speed ), function() {
                    a.css('left', 0);
                    a.children().css('marginLeft', '');
                    btn = true;
                    if (nCallSelf > 1) {
                        nCallSelf -= 1;
                        slide_prev(true, nCallSelf);
                    }
                })
                ptr -= 1;
                build_circles();
            }
            function slide_next(continuous, nCallSelf) {
                if ( ! btn) return;
                btn = false;
                a.animate({
                    'left': '-' + a.parent().width()
                }, ( continuous ? (400/steps) : b.speed ), function() {
                    a.css('left', 0).children(':first').appendTo(a)
                    btn = true;
                    if (nCallSelf > 1) {
                        nCallSelf -= 1;
                        slide_next(true, nCallSelf);
                    }
                })
                ptr += 1;
                build_circles();
            }
            slideTo = function(dest) {
                if (dest < ptr) {
                    steps = ptr -dest;
                    slide_prev(true, steps);
                } else {
                    steps =  dest -ptr;
                    slide_next(true, steps);
                }
            }
            function build_circles() {
                var x,
                    y,
                    c = "",
                    circles = $('#circles');
                if (typeof circles === 'undefined') return;
                if (ptr === -1) {
                    ptr = a.children().length -1;
                } else if (ptr === a.children().length) {
                    ptr = 0;
                }
                for (y = 0; y <= a.children().length -1; y += 1) {
                    c += " <div class='circle" + (ptr === y ? " on'" : "' onclick='slideTo(" + y + ")'") + "></div> ";
                }
                circles.html(c);
            }
        })
    }
})(jQuery);