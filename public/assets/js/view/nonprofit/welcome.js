$(function(){

    $('form#password-form').submit(function(){
        
        password = $('#form_password');
        password_reentry = $('#form_password_reentry');

        $('.flderrmsg').hide();

        if (password.val().length < 6)
        {
            password.siblings('.flderrmsg').html('Password should at least 6 characters long.').show();
            password.focus();
            return false;
        }

        if (password.val() != password_reentry.val())
        {
            password_reentry.siblings('.flderrmsg').html('Passwords do not match.').show();
            password_reentry.select();
            return false;
        }
    });
});