$(function(){

    console.log(count_celeb());

    $('.xCircle.floatRight').click(function(){$('#list-celeb').fadeOut()});
    initSlider();
    insertDiselect();
    var tempVal = $('<div>').addClass('celeb glowbox center')
            .append($('<div>').addClass('div-img')
                .append($('<div>').addClass('watermark').css({'background-color': 'rgb(0, 0, 255)'})
                    .html('?')));

    $(document).on('click mouseenter', '.xCircle.remove', function(event){
        if(event.type == 'click'){
            var container = $(this).parents('.featured-celebrities');
            var id = container.parent().prev('input[type=hidden]');
            
            $.post('/ajax/set/choose', { id: id.val(), action: 'delete' }, function(result){
                if (result.success){
                    id.val('');
                    container.html(tempVal.clone())
                    .click(function(){
                        $('#list-celeb').show();
                    });
                    $('#list-celeb').show();
                }
                else{
                    console.log('deleting error');
                }
                    
            }, 'json');  
        }else if(event.type == 'mouseenter'){
            $('div.smallRed').fadeOut(); // Hide other instances
            $(this).siblings('div.smallRed').fadeIn();
        }
    }); 

   
    $('#cause-link').load('/ajax/loader/cause_links', function() {

       bindAlphabet();

       $('#cause-link a').click(function(e){
            e.preventDefault();
            $('#alphabet').html('').load('/ajax/loader/celebrities_links/cause/' + $(this).siblings('span').data('cause-id'), function(){
                loadAlphabet();
            });
        });

    });  

    $('#field-links').load('/ajax/loader/field_links', function(e){

        $('#field-links a').click(function(e){
            e.preventDefault();
            $('#alphabet').html('').load('/ajax/loader/celebrities_links/field/' + $(this).siblings('span').data('field-id'), function(){
                bindAlphabet();
            });
        });
    });

    $('#alphabet').load('/ajax/loader/celebrities_links', function(){
        bindAlphabet();
    });

    var x = count_celeb();

    for (var i = 0; i < 6 - x; i++) {
        $('<div>').addClass('celeb-container')
            .append('<input type=hidden>').val('')
            .append($('<div>').append('<div>').addClass('featured-celebrities')
                .append(tempVal.clone())
                .click(function(){
                    $('#list-celeb').show();
                }))
            .appendTo($('.picked-celebrity'));
    }

});

function insertDiselect(){
    var d, s;

    d = $('<div>')
        .addClass('xCircle remove')
        .attr({'title':'Remove this celebrity and choose another'})
        .html('&times');
    s = $('<div>')
        .addClass('smallRed')
        .attr({'title':'Remove this celebrity and choose another'})
        .html('&larr; Click to deselect');
    $('.celeb-container').each(function(){
        $(this).find('.featured-name').append(d.clone()).append(s.clone());

    });
}

function count_celeb(){
    return $('div.celeb-container > input[type=hidden][value!=""]').length;
}

function initSlider(){
    $('.celeb-members').mySlider({prevEnable: false, nextRight: '-15px', alwaysVisible: true});
}

function bindAlphabet(){
    $('#alphabet a').click(function(e){
        e.preventDefault();
        $.get('/ajax/get/celebrity_id_picture', { full_name: $(this).html().trim() }, function(result){
            if (result.success) {
               celebrity_selected(result.data);
           }
       });
    });
}

function is_selected_celeb(check_id) 
{
    var is_selected = false;
    $('.celeb-container').each(function(){
        if($(this).children('input[type=hidden]').val() == check_id){
            is_selected = true;
            return false;
        }
    });
    return is_selected;
}

function select_node()
{
    var node;
    $('.celeb-container').each(function(){
        if($(this).children('input[type=hidden]').val() == ''){
            node = $(this);
            return false;
        }


        
    });
    return node;
}

function celebrity_selected(data)
{
    var c,
    search,
    n;

    search = $("input#celeb-search");

    var container = select_node();

    if(count_celeb() >= 6){
        alert('You can only pick up to six celebrities!');
        return false;
    }

    if (is_selected_celeb(data.id)) {
       alert('Opps, you have selected this one already!');
       return false;
    }
    else
    {   

         $.post('/ajax/set/choose', { id: data.id, action: 'add' }, function(result){
            if (result.success){
                container.children('div').remove();
                container.children('input[type=hidden]').val(data.id);
                container.append($('<div>')
                    .load('/ajax/get/celebrity_formatted_box/' + data.id, function(){
                        initSlider();
                        $('<div/>')
                            .addClass('xCircle remove')
                            .text('x')
                            .attr('title', 'Remove this celebrity and choose another')
                            .appendTo($(this).find('div.featured-name'));

                        $('div.smallRed').fadeOut(); // Hide other instances
                        
                        $('<div/>')
                            .html("&larr; Click to deselect")
                            .addClass('smallRed')
                            .attr('title', 'Remove this celebrity and choose another')
                            .appendTo($(this).find('div.featured-name'));

                    // Select one celebrity at a time
                    $("#list-celeb").fadeOut(function(){
                        //guide_to_next_celeb();
                        $("#celeb-entry").find('watermark :first')
                        .removeClass('watermark')
                        .html('Click here to select another celebrity');
                    });
                })
                );                
            }
            else{
                log('saving error');
            }
                
        }, 'json');
    }

 
}
