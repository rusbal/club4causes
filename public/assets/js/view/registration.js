$(function(){
    var valid;

    window.us_states_arr = $.map(us_states, function (value, key) { return value; });

    // $('#form_birth_date').datepicker({defaultDate: "1990-01-01"});

    $('#form_confirm_password').change(function(){ if ($(this).val()) $(this).removeClass('redinput')});
    $('#form_password').change(function(){ if ($(this).val()) $(this).removeClass('redinput')});
    $('#form_city').change(function(){ if ($(this).val()) $(this).removeClass('redinput')});
    $('#form_state').change(function(){ if ($(this).val()) $(this).removeClass('redinput')});
    $('#form_country').change(function(){ if ($(this).val()) $(this).removeClass('redinput')});
    $('#form_last_name').change(function(){ if ($(this).val()) $(this).removeClass('redinput')});
    $('#form_first_name').change(function(){ if ($(this).val()) $(this).removeClass('redinput')});
    $('#form_alias').change(function(){ if ($(this).val()) $(this).removeClass('redinput')});
    $('#form_email').change(function(){ if ($(this).val()) $(this).removeClass('redinput')});

    $('#form_country').change(function(){
        call_set_autocomplete($('#form_state'), $(this).val());
    });
    $('#form_nprofit_country_1').change(function(){
        call_set_autocomplete($('#form_nprofit_state_1'), $(this).val());
    });
    $('#form_nprofit_country_2').change(function(){
        call_set_autocomplete($('#form_nprofit_state_2'), $(this).val());
    });

    set_autocomplete($('#form_nprofit_name_1, #form_nprofit_name_2'), nonprofit_arr);

    $('#form_terms_agreed').click(function(){
        if ($('#form_terms_agreed').is(':checked'))
            $('#terms_agreed_message').hide();
        else
            $('#terms_agreed_message').addClass('flderrmsg').html("To proceed, please signify your agreement by checking the check box above.").show();
    });
    
    $('button[class=ragister_btn]').click(function(e)
    {
        valid = true;
        
        // if ("" == $('#form_recaptcha_response_field').val())
        // {
        //     $('#captcha_message').addClass('flderrmsg').html("Please enter the captcha text above.").show();
        //     $('#form_recaptcha_response_field').focus();
        //     valid = false;
        // }
    
        if ($('#form_confirm_password').val() == '') { valid = false; /*$('#form_confirm_password').addClass('redinput').focus();*/ }
        if ($('#form_password').val() == '') { valid = false; /*$('#form_password').addClass('redinput').focus();*/ }
        if ($('#form_city').val() == '') { valid = false; /*$('#form_city').addClass('redinput').focus();*/ }
        if ($('#form_state').val() == '') { valid = false; /*$('#form_state').addClass('redinput').focus();*/ }
        if ($('#form_country').val() == '') { valid = false; /*$('#form_country').addClass('redinput').focus();*/ }
        if ($('#form_birth_date_month').val() == '') { valid = false; /*$('#form_birth_date_month').addClass('redinput').focus();*/ }
        if ($('#form_birth_date_day').val() == '') { valid = false; /*$('#form_birth_date_day').addClass('redinput').focus();*/ }
        if ($('#form_last_name').val() == '') { valid = false; /*$('#form_last_name').addClass('redinput').focus();*/ }
        if ($('#form_first_name').val() == '') { valid = false; /*$('#form_first_name').addClass('redinput').focus();*/ }
        if ($('#form_alias').val() == '' || $('span#msg_taken').length > 0) { valid = false; /*$('#form_alias').addClass('redinput').focus();*/ }
        if ($('#form_email').val() == '') { valid = false; /*$('#form_email').addClass('redinput').focus();*/ }
        
        if (valid)
        {
            if ( ! $('#form_terms_agreed').is(':checked'))
            {
                $('#terms_agreed_message').addClass('flderrmsg').html("To proceed, please signify your agreement by checking the check box above.").show();
                $('#terms_agreed_message').siblings('.flderrmsg').remove();
                $('#form_terms_agreed').focus();
                return false;
            }
        }

        // return valid && captcha_validation();

        /**
         * Return true for the browser to do the "required" validation
         */
        return true;
    });

    $('div#help-find input').click(function(){
        window.open($(this).data('link'));
    });

    // $( "#form_birth_date" ).datepicker( "option", "dateFormat", 'mm/dd' );

    /**
     * Check username availability
     */
    $('#form_alias').blur(function(){

        if ($(this).val() == "")
            return 

        if (c4c.alias === $(this).val())
            return;

        c4c.alias = $(this).val();
        $this = this;

        $.get('/ajax/check_username_availability', { username: $(this).val() }, function(data){
            if (data.is_taken)
            {
                $('<span/>')
                    .attr('id', 'msg_taken')
                    .html('Someone already has that username. Try another?')
                    .css('color', 'red')
                .insertAfter($($this));
            }
            else
            {
                $('#msg_taken').remove();
            }
        }, 'json');
    });

    $('#celeb-link').click(function(){
        var f = $(this).parents('fieldset');
        // f.css('height', '250px');
        f.children('div.celebrities').fadeIn();

        window.celebs = $('div.celebrities > div');
        guide_to_next_celeb();
        activate_div_celeb(0);

        $(this).parent().hide();
        $('#celeb-entry').show();
        $('#celeb-search').focus();
    });

    $('div.celeb').click(function(){
        activate_div_celeb();
    });

    $('#pick-celeb input[type=button], #pick-celeb div.floatRight').click(function(){
        $("#pick-celeb").fadeOut();
    });

    /**
     * Ajax request for pick-celeb content
     */
    $('#cause-links').load('/ajax/loader/cause_links', function() {
        var loadAlphabet = function(){
            $('#alphabet a').click(function(e){
                e.preventDefault();
                $.get('/ajax/get/celebrity_id_picture', { full_name: $(this).html().trim() }, function(result){
                    if (result.success) {
                        celebrity_selected(result.data);
                    }
                });
            });
        }

        $('#cause-links a').click(function(e){
            e.preventDefault();
            $('#alphabet').html('').load('/ajax/loader/celebrities_links/cause/' + $(this).siblings('span').data('cause-id'), function(){
                loadAlphabet();
            });
        });

        $('#field-links').load('/ajax/loader/field_links', function(e){
            $('#field-links a').click(function(e){
                e.preventDefault();
                $('#alphabet').html('').load('/ajax/loader/celebrities_links/field/' + $(this).siblings('span').data('field-id'), function(){
                    loadAlphabet();
                });
            });
        });

        $('#alphabet').load('/ajax/loader/celebrities_links', function(){
            loadAlphabet();
        });
    });
});

function celebrity_selected(data)
{
    var c,
        search,
        n;

    search = $("input#celeb-search");

    if (is_selected_celeb(data.id)) {
        alert('Opps, you have selected this one already!');
        return false;
    }

    c = $(window.celebs[window.active_celeb]);
    c.children('input').val(data.id);
    c.children('div').remove();

    $('<div/>')
        .load('/ajax/get/celebrity_formatted_box/' + data.id, function(){
            $('.celeb-members').mySlider({prevEnable: false, nextRight: '-15px', alwaysVisible: true});
            $('<div/>')
                .addClass('xCircle')
                .html('x')
                .css({ marginLeft: '10px' })
                .attr('title', 'Remove this celebrity and choose another')
                .click(function(){
                    var c = $(this).parent().parent().parent().parent().parent();
                    c.parent().find('input[type=hidden]').val('');
                    window.active_celeb = c.parent().find('input[type=hidden]').data('idx') -1;
                    $(this).parent().parent().parent().parent().remove();
                    c.html('<div class="celeb glowbox center"><div class="div-img"><div class="watermark" style="background-color: rgb(0, 0, 255);">?</div></div></div>')
                        .click(function(){
                            $('#pick-celeb').show();
                        });
                    $('#pick-celeb').show();
                })
                .mouseenter(function(){
                    $('div.smallRed').fadeOut(); // Hide other instances
                    $(this).siblings('div.smallRed').fadeIn();
                })
                .appendTo($(this).find('div.featured-name'));

            $('div.smallRed').fadeOut(); // Hide other instances
            $('<div/>')
                .html("&larr; Click to deselect")
                .addClass('smallRed')
                .attr('title', 'Remove this celebrity and choose another')
                .appendTo($(this).find('div.featured-name'));

            // Select one celebrity at a time
            $("#pick-celeb").fadeOut(function(){
                guide_to_next_celeb();
                $("#celeb-entry").find('watermark :first')
                    .removeClass('watermark')
                    .html('Click here to select another celebrity');
            });
        })
        .appendTo(c);
}

function is_selected_celeb(check_id) 
{
    var selected = false;
    $.each($('#celeb-entry').find('input'), function(nil, obj){
        if (+$(obj).val() === check_id) {
            selected = true;
            return false;
        }
    });
    return selected;
}

function count_selected_celeb() 
{
    var n = 0;
    $.each($('#celeb-entry').find('input'), function(nil, obj){
        if ($(obj).val() !== '') {
            n += 1;
        }
    });
    return n;
}

function guide_to_next_celeb()
{
    var x,
        n = count_selected_celeb();

    for (x = 0; x <= 6; x++) {
        if (x === n) {
            $(window.celebs[x]).find('.watermark').css({ backgroundColor: 'blue', fontSize: '12px', lineHeight: '16px', padding: '40px 10px', color: 'yellow', textDecoration: 'blink' }).html('Click here to select next celebrity');
        } else {
            $(window.celebs[x]).find('.watermark').css('backgroundColor', '#dddddd');
        }
    }
}

function activate_div_celeb(n)
{
    var x,
        n;

    if (typeof n === 'undefined') {
        n = count_selected_celeb();
    }
    window.active_celeb = n;

    for (x = 0; x <= 6; x++) {
        if (x === n) {
            $(window.celebs[x]).find('.watermark').css('textDecoration', '').html('Select your favorite celebrity');
        } else {
            $(window.celebs[x]).find('.watermark').css('backgroundColor', '#dddddd');
        }
    }

    $('#pick-celeb').show();
}

function call_set_autocomplete(obj, val)
{
    switch (val)
    {
        case "United States":
            set_autocomplete(obj, us_states_arr);
            break;

        case "":
        default:
            set_autocomplete(obj, []);
            break;
    }
}
