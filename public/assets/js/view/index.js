$(function(){
    $('#da-slider').cslider({autoplay: false });
    $('#tl_content').easyTabs({defaultContent:1});
    $('#cs_content').easyTabs({defaultContent:1});
    $(".youtube").colorbox({iframe:true, innerWidth:425, innerHeight:315, width: '457px', height: '386px'});

    // Set HarveyUsbal as default user on sign in form
    $('#form_username').val('HarveyUsbal');
    $('#form_password').val('default');
});