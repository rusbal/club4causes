<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<pun_language>" lang="<pun_language>" dir="<pun_content_direction>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="/forum/style/c4c-style.css" />
<script src="/assets/js/jquery.js" type="text/javascript"></script>
<pun_head>
</head>

<body>
<div id="site_header">
    <div id="header_strip">
        <div class="hs_inner">
            <ul id="header_menu">
                <li><a href="/">Home</a></li>
                <li><a href="/signout">Sign out</a></li>
                <li><a href="/forum">Forum</a></li>
                <li><a href="/shopping">Shopping Mall</a></li>
                <li><a href="/celebrities">Top Celebrity Givers</a></li>
                <li><a href="/worlds-top-givers">World's Top Givers</a></li>
                <li><a href="/about">Our Team</a></li>
                <li><a href="/faq">FAQ</a></li>
                <li><a href="/blog">Blog</a></li>
                <li><a href="/contact">Contact Us</a></li>
                <li id="google-search">
                    <div id="google_translate_element" style="margin-top:-1px"></div><script type="text/javascript">
    function googleTranslateElementInit() {
      new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
    }
    </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                </li>
            </ul>
        </div>
    </div>
        <div class="sh_inner">
            <a href="/" class="main_logo"></a>
                <div class="sm_icons">
                    <a href=""><span class="sm_icon_01"></span></a>
                    <a href=""><span class="sm_icon_02"></span></a>
                    <a href=""><span class="sm_icon_03"></span></a>
                    <a href=""><span class="sm_icon_04"></span></a>
                    <a href="http://digg.com/"><span class="sm_icon_05"></span></a>
                    <a href="http://www.youtube.com/"><span class="sm_icon_06"></span></a>
                    <a href="https://www.facebook.com/"><span class="sm_icon_07"></span></a>
                    <a href="https://twitter.com/"><span class="sm_icon_08"></span></a>
                    <a href=""><span class="sm_icon_09"></span></a>
                    <a href="http://www.linkedin.com/"><span class="sm_icon_10"></span></a>
                </div>
        </div>
</div>

<div id="pun<pun_page>" class="pun">
<div class="top-box"><div><!--Top Corners --></div></div>
<div class="punwrap">

<div id="brdheader" class="block">
	<div class="box">
		<div id="brdtitle" class="inbox">
			<pun_title>
			<pun_desc>
		</div>
		<!--<pun_navlinks>-->
		<pun_status>
	</div>
</div>

<pun_announcement>

<div id="brdmain">
<pun_main>
</div>

<pun_footer>

</div>
<div class="end-box"><div><!-- Bottom corners --></div></div>
</div>

</body>
</html>
